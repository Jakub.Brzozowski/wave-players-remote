package com.jakubbrzozowski.waveplayersremote.ui

import android.content.SharedPreferences
import android.content.res.Resources
import com.google.gson.Gson

import com.jakubbrzozowski.player_apis.FileInfo
import com.jakubbrzozowski.player_apis.MediaPlayerApi
import com.jakubbrzozowski.player_apis.mpc.hc.MediaPlayerClassicHomeCinema
import com.jakubbrzozowski.player_apis.vlc.VlcHttpClient
import com.jakubbrzozowski.waveplayersremote.R
import com.jakubbrzozowski.waveplayersremote.ui.filebrowser.FileBrowserContract
import com.jakubbrzozowski.waveplayersremote.ui.main.MainContract
import com.jakubbrzozowski.waveplayersremote.ui.remote.RemoteContract

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CommonModel @Inject
internal constructor(private val mRes: Resources, private val mShPrefs: SharedPreferences) : SharedPreferences.OnSharedPreferenceChangeListener {

    private val mPrefPlayerVlc: String
    private val mPrefPlayerMpc: String
    private val mPortDefaultMpc: String
    private val mPortDefaultVlc: String
    var prefPlayer: String? = null
        private set
    private var mAutoServiceStartupFlag: Boolean = false
    var isKeepScreenOnFlag: Boolean = false
        private set
    var fileInfoList: MutableList<FileInfo>? = null
    private var mMainPresenter: MainContract.Presenter? = null
    private var mFileBrowserPresenter: FileBrowserContract.Presenter? = null
    private var mRemotePresenter: RemoteContract.Presenter? = null
    var mediaPlayerApi: MediaPlayerApi? = null
        private set
    var serverAddress: String = ""
        private set
    var port: Int = 0
        private set
    var password: String = ""
        private set

    val defaultPort: String
        get() = if (isPrefPlayerVlc) {
            mPortDefaultVlc
        } else
            mPortDefaultMpc

    var isPauseOnRinging: Boolean
        get() = mShPrefs.getBoolean(mRes.getString(R.string.pref_pause_on_ringing_key),
                mRes.getBoolean(R.bool.pref_pause_on_ringing_default))
        set(pauseOnRinging) = mShPrefs.edit().putBoolean(mRes.getString(R.string.pref_pause_on_ringing_key),
                pauseOnRinging).apply()

    val isPrefPlayerVlc: Boolean
        get() = prefPlayer == mPrefPlayerVlc

    val isPrefPlayerMpc: Boolean
        get() = prefPlayer == mPrefPlayerMpc

    var isAutoServiceStartupFlag: Boolean
        get() = mAutoServiceStartupFlag
        set(autoServiceStartupFlag) {
            mShPrefs.edit().putBoolean(mRes.getString(R.string.pref_wave_service_auto_startup_key),
                    autoServiceStartupFlag)
                    .apply()
            mAutoServiceStartupFlag = autoServiceStartupFlag
            if (mMainPresenter != null) {
                if (mAutoServiceStartupFlag) {
                    mMainPresenter!!.startWaveService()
                } else {
                    mMainPresenter!!.stopWaveService()
                }
            }
        }

    init {
        initializeSharedPreferences()
        mPrefPlayerVlc = mRes.getString(R.string.pref_player_vlc)
        mPrefPlayerMpc = mRes.getString(R.string.pref_player_mpc_hc)
        mPortDefaultMpc = mRes.getString(R.string.port_default_mpc)
        mPortDefaultVlc = mRes.getString(R.string.port_default_vlc)
    }

    fun setRemotePresenter(remotePresenter: RemoteContract.Presenter?) {
        mRemotePresenter = remotePresenter
    }

    private fun initializeSharedPreferences() {
        mAutoServiceStartupFlag = mShPrefs.getBoolean(mRes.getString(R.string.pref_wave_service_auto_startup_key),
                mRes.getBoolean(R.bool.pref_wave_service_auto_startup_default))
        prefPlayer = mShPrefs.getString(mRes.getString(R.string.pref_player_key),
                mRes.getString(R.string.pref_player_default))
        isKeepScreenOnFlag = mShPrefs.getBoolean(mRes.getString(R.string.pref_keep_screen_on_key),
                mRes.getBoolean(R.bool.pref_keep_screen_on_default))
        if (mMainPresenter != null) {
            mMainPresenter!!.setKeepScreenOn(isKeepScreenOnFlag)
        }

        serverAddress = mShPrefs.getString(mRes.getString(R.string.pref_server_address_key),
            mRes.getString(R.string.pref_server_address_default))!!
        port = Integer.valueOf(mShPrefs.getString(mRes.getString(R.string.pref_server_port_key),
                mRes.getString(R.string.pref_server_port_default)))!!
        password = mShPrefs.getString(mRes.getString(R.string.pref_server_pass_key),
            mRes.getString(R.string.pref_server_pass_default))!!
        mediaPlayerApi = if (prefPlayer == mRes.getString(R.string.pref_player_mpc_hc)) {
            MediaPlayerClassicHomeCinema(serverAddress, port, password)
        } else if (prefPlayer == mRes.getString(R.string.pref_player_vlc)) {
            VlcHttpClient(serverAddress, port, password)
        } else
            MediaPlayerClassicHomeCinema(serverAddress, port, password)

        mShPrefs.registerOnSharedPreferenceChangeListener(this)
    }

    fun setMainPresenter(mainPresenter: MainContract.Presenter?) {
        mMainPresenter = mainPresenter
    }

    fun setFileBrowserPresenter(fileBrowserPresenter: FileBrowserContract.Presenter?) {
        mFileBrowserPresenter = fileBrowserPresenter
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        if (key == mRes.getString(R.string.pref_wave_service_auto_startup_key)) {
            mAutoServiceStartupFlag = sharedPreferences
                    ?.getBoolean(mRes.getString(R.string.pref_wave_service_auto_startup_key),
                            mRes.getBoolean(R.bool.pref_wave_service_auto_startup_default)) == true
            if (mMainPresenter != null) {
                if (mAutoServiceStartupFlag) {
                    mMainPresenter!!.startWaveService()
                } else {
                    mMainPresenter!!.stopWaveService()
                }
            }
            if (mRemotePresenter != null) {
                mRemotePresenter!!.setWaveHandIcon(mAutoServiceStartupFlag)
            }
        }

        if (key == mRes.getString(R.string.pref_player_key)) {
            clearCache()
            prefPlayer = sharedPreferences?.getString(key,
                    mRes.getString(R.string.pref_player_default))
            recreateMediaPlayerApi(sharedPreferences)
            if (mFileBrowserPresenter != null) {
                mFileBrowserPresenter!!.openRoot()
            }
        }

        if (key == mRes.getString(R.string.pref_keep_screen_on_key)) {
            isKeepScreenOnFlag = sharedPreferences?.getBoolean(key,
                    mRes.getBoolean(R.bool.pref_keep_screen_on_default)) == true
            if (mMainPresenter != null) {
                mMainPresenter!!.setKeepScreenOn(isKeepScreenOnFlag)
            }
        }

        if (key == mRes.getString(R.string.pref_server_address_key)) {
            serverAddress = sharedPreferences?.getString(
                    mRes.getString(R.string.pref_server_address_key),
                    mRes.getString(R.string.pref_server_address_default))
                ?: mRes.getString(R.string.pref_server_address_default)
            clearCache()
            recreateMediaPlayerApi(sharedPreferences)
        }

        if (key == mRes.getString(R.string.pref_server_port_key)) {
            port = Integer.valueOf(sharedPreferences?.getString(
                    mRes.getString(R.string.pref_server_port_key),
                    mRes.getString(R.string.pref_server_port_default))
                ?:  mRes.getString(R.string.pref_server_port_default))
            clearCache()
            recreateMediaPlayerApi(sharedPreferences)
        }

        if (key == mRes.getString(R.string.pref_server_pass_key)) {
            password = sharedPreferences?.getString(
                    mRes.getString(R.string.pref_server_pass_key),
                    mRes.getString(R.string.pref_server_pass_default))
                ?:  mRes.getString(R.string.pref_server_pass_default)
            clearCache()
            recreateMediaPlayerApi(sharedPreferences)
        }
    }

    fun clearCache() {
        fileInfoList = null
        setDirectoryCache(null)
    }

    fun setDirectoryCache(fileInfo: FileInfo?) {
        mShPrefs.edit().putString(DIRECTORY_CACHE_KEY, Gson().toJson(fileInfo)).commit()
    }

    fun getDirectoryCache(): FileInfo? {
        return Gson().fromJson(mShPrefs.getString(DIRECTORY_CACHE_KEY, null), FileInfo::class.java)
    }

    private fun recreateMediaPlayerApi(sharedPreferences: SharedPreferences?) {
        val player = sharedPreferences?.getString(mRes.getString(R.string.pref_player_key),
                mRes.getString(R.string.pref_player_mpc_hc))
        if (player == mRes.getString(R.string.pref_player_mpc_hc)) {
            mediaPlayerApi = MediaPlayerClassicHomeCinema(serverAddress, port, password)
        } else if (player == mRes.getString(R.string.pref_player_vlc)) {
            mediaPlayerApi = VlcHttpClient(serverAddress, port, password)
        }
    }

    companion object {

        const val DIRECTORY_CACHE_KEY = "directory_cache"
        val VLC_APP_THEME_NO_ACTION_BAR = R.style.VlcAppTheme_NoActionBar
        val APP_THEME_NO_ACTION_BAR = R.style.AppTheme_NoActionBar
    }
}
