package com.jakubbrzozowski.waveplayersremote.ui.remote


import com.jakubbrzozowski.player_apis.MediaPlayerApi
import com.jakubbrzozowski.player_apis.TimeCode
import com.jakubbrzozowski.player_apis.TimeCodeException
import com.jakubbrzozowski.waveplayersremote.eventbus.*
import com.jakubbrzozowski.waveplayersremote.ui.CommonModel
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.json.JSONException
import timber.log.Timber
import java.io.IOException
import java.util.*
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.ScheduledThreadPoolExecutor
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Named

class RemotePresenter @Inject
internal constructor(private val mCommonModel: CommonModel, remoteModel: RemoteContract.Model,
                     @param:Named("mainThread") private val mMainScheduler: Scheduler) : RemoteContract.Presenter {
    private var mView: RemoteContract.View? = null
    private val mRemoteModel: RemoteContract.Model = remoteModel
    private val mDisposables = CompositeDisposable()
    private val mExecutor = ScheduledThreadPoolExecutor(10)
    private var mScheduledUpdateSnapshot: ScheduledFuture<*>? = null
    private var mScheduledFastForward: ScheduledFuture<*>? = null
    private var mScheduledRewind: ScheduledFuture<*>? = null

    private val infoObservable: Observable<Map<String, String>>
        get() = Observable.fromCallable {
            var map: Map<String, String> = HashMap()
            try {
                map = mCommonModel.mediaPlayerApi!!.variables
            } catch (e: Exception) {
                when (e) {
                    is JSONException,
                    is NullPointerException,
                    is KotlinNullPointerException,
                    is IOException -> Timber.d(e)
                }
            }
            map
        }

    override fun attachView(view: RemoteContract.View) {
        EventBus.getDefault().unregister(this)
        EventBus.getDefault().register(this)
        mView = view
        mCommonModel.setRemotePresenter(this)
    }

    override fun detachView() {
        EventBus.getDefault().unregister(this)
        mView = null
        mCommonModel.setRemotePresenter(null)
        stopUpdatingInfo()
    }

    @Subscribe
    fun onEvent(event: ProgressUpdateEvent) {
        if (mView != null) {
            manualProgressUpdate()
        }
    }

    override fun startJumpingForward() {
        jumpForwardMedium()
        mScheduledFastForward = mExecutor.scheduleAtFixedRate(
                { mMainScheduler.scheduleDirect { this.jumpForwardMedium() } },
                JUMP_REPEAT_START_DELAY_MS.toLong(), JUMP_REPEAT_INTERVAL_MS.toLong(), TimeUnit.MILLISECONDS)
    }

    override fun stopJumpingForward() {
        if (mScheduledFastForward != null && !mScheduledFastForward!!.isCancelled) {
            mScheduledFastForward!!.cancel(true)
        }
    }

    override fun startJumpingBackward() {
        jumpBackwardMedium()
        mScheduledRewind = mExecutor.scheduleAtFixedRate(
                { mMainScheduler.scheduleDirect { this.jumpBackwardMedium() } },
                JUMP_REPEAT_START_DELAY_MS.toLong(), JUMP_REPEAT_INTERVAL_MS.toLong(), TimeUnit.MILLISECONDS)
    }

    override fun stopJumpingBackward() {
        if (mScheduledRewind != null && !mScheduledRewind!!.isCancelled) {
            mScheduledRewind!!.cancel(true)
        }
    }

    override fun stopUpdatingInfo() {
        mDisposables.clear()
    }

    override fun stopSnapshotUpdate() {
        if (mScheduledUpdateSnapshot != null && !mScheduledUpdateSnapshot!!.isCancelled) {
            mScheduledUpdateSnapshot!!.cancel(true)
        }
    }

    override fun dismissSnackBar() {
        if (!mView!!.isSnackBarNull && mView!!.isSnackBarShown) {
            mView!!.dismissSnackBar()
        }
    }

    override fun startSnapshotUpdate() {
        if (mCommonModel.isPrefPlayerVlc) {
            mView!!.setVlcBackground()
        } else {
            mScheduledUpdateSnapshot = mExecutor.scheduleAtFixedRate(
                    { mMainScheduler.scheduleDirect { this.updateSnapshot() } },
                    UPDATE_SNAPSHOT_START_DELAY_MS.toLong(), SNAPSHOT_UPDATE_INTERVAL_MS.toLong(), TimeUnit.MILLISECONDS)
        }
    }

    override fun setWaveHandIcon(waveHandOn: Boolean) {
        if (waveHandOn) {
            mView!!.setWaveHandIconOn()
        } else {
            mView!!.setWaveHandIconOff()
        }
    }

    override fun initWaveHandIcon() {
        setWaveHandIcon(mCommonModel.isAutoServiceStartupFlag)
    }

    override fun startInfoUpdate() {
        if (mView != null) {
            mDisposables.add(subscribeInfoObservable(infoObservable.compose(applyInfoLooping())))
        }
    }

    override fun togglePlay() {
        mRemoteModel.togglePlay()
        manualProgressUpdate()
    }

    override fun nextAudioTrack() {
        mRemoteModel.nextAudioTrack()
    }

    override fun nextSubtitles() {
        mRemoteModel.nextSubtitles()
    }

    override fun toggleFullscreen() {
        mRemoteModel.toggleFullscreen()
    }

    override fun toggleRepeatMode() {
        mRemoteModel.toggleRepeatMode()
    }

    override fun nextFile() {
        mRemoteModel.nextFile()
        manualProgressUpdate()
    }

    override fun previousFile() {
        mRemoteModel.previousFile()
        manualProgressUpdate()
    }

    override fun waveHandToggle() {
        mCommonModel.isAutoServiceStartupFlag = !mCommonModel.isAutoServiceStartupFlag
        setWaveHandIcon(mCommonModel.isAutoServiceStartupFlag)
    }

    override fun snackBarChangeServerAction(fragmentClass: Class<*>, layoutId: Int) {
        EventBus.getDefault().post(LoadFragmentEvent(fragmentClass, layoutId))
    }

    override fun initSnackBar() {
        if (mView!!.isSnackBarNull || !mView!!.isSnackBarShown) {
            mView!!.initSnackBar()
        }
    }

    override fun unlockRightDrawer() {
        EventBus.getDefault().post(UnlockRightDrawerEvent())
    }

    override fun invalidateOptionsMenu() {
        EventBus.getDefault().post(InvalidateOptionsMenuEvent())
    }

    private fun jumpBackwardMedium() {
        mRemoteModel.jumpBackwardMedium()
        manualProgressUpdate()
    }

    private fun jumpForwardMedium() {
        mRemoteModel.jumpForwardMedium()
        manualProgressUpdate()
    }

    override fun onVolumeSeekBarChange(progress: Int, fromUser: Boolean) {
        if (fromUser) {
            val desiredVolumeLevel: Double
            desiredVolumeLevel = progress.toDouble() * 100.0 / mRemoteModel.volumeSeekBarMaxProgress
            mRemoteModel.setVolume(desiredVolumeLevel.toInt())
        }
    }

    override fun onProgressSeekBarChange(progress: Int, fromUser: Boolean) {
        if (fromUser) {
            var tempTimeCode: TimeCode? = null
            val progressInSeconds: Double
            progressInSeconds = progress.toDouble() / mRemoteModel.progressSeekBarMaxProgress * mRemoteModel.totalSeconds
            try {
                tempTimeCode = TimeCode(progressInSeconds.toInt())
                mRemoteModel.seek(tempTimeCode)
            } catch (e: TimeCodeException) {
                e.printStackTrace()
            }

            mView!!.setCurrentPositionText(setFormattedTimeCodeText(tempTimeCode))
        }
    }

    private fun setFormattedTimeCodeText(timeCode: TimeCode?): String {
        if (timeCode != null) {
            val hours = timeCode.hours
            val minutes = timeCode.minutes
            val seconds = timeCode.seconds
            return String.format("%02d:%02d:%02d", hours, minutes, seconds)
        } else {
            return "00:00:00"
        }

    }

    override fun updateSnapshot() {
        Timber.d("Snapshot update started")
        if (mCommonModel.isPrefPlayerVlc) {
            mView?.setVlcBackground()
        } else if (mView?.backgroundImageViewWith ?: 0 > 0) {
            mView?.loadMpcBackground(mCommonModel.serverAddress, mCommonModel.port.toString())
        }
    }

    private fun manualProgressUpdate() {
        subscribeInfoObservable(infoObservable)
    }

    private fun subscribeInfoObservable(observable: Observable<Map<String, String>>):
            DisposableObserver<Map<String, String>> {
        // Run on a background thread
        return observable.subscribeOn(Schedulers.io())
                // Be notified on the main thread
                .observeOn(mMainScheduler)
                .subscribeWith(object : DisposableObserver<Map<String, String>>() {
                    override fun onNext(stringStringMap: Map<String, String>) {
                        Timber.d("Update info onNext called")
                        try {
                            updateProgress(stringStringMap)
                        } catch (e: NullPointerException) {
                            Timber.d(e)
                        }
                    }

                    override fun onError(e: Throwable) {
                        Timber.d(e)
                    }

                    override fun onComplete() {
                        Timber.d("Update info onComplete called")
                    }
                })
    }

    private fun <T> applyInfoLooping(): ObservableTransformer<T, T> {
        return ObservableTransformer { observable ->
            observable
                    .repeatWhen({ objectObservable ->
                        objectObservable.delay(INFO_UPDATE_INTERVAL_MS.toLong(),
                                TimeUnit.MILLISECONDS)
                    })
                    .retryWhen({ throwables -> throwables.delay(INFO_UPDATE_INTERVAL_MS.toLong(), TimeUnit.MILLISECONDS) })
        }
    }

    private fun setToolbarTitle(title: String) {
        EventBus.getDefault().post(MainToolbarTitleChangeEvent(title, this.javaClass))
    }

    override fun setDefaultToolbarTitle() {
        EventBus.getDefault().post(MainToolbarTitleChangeEvent(
                mRemoteModel.remoteFragmentTitle, this.javaClass))
    }

    private fun updateProgress(stringMap: Map<String, String>) {
        if (stringMap.isEmpty()) {
            setDefaultToolbarTitle()
            try {
                if (!mView!!.isSnackBarNull) {
                    if (!mView!!.isSnackBarShown) {
                        mView!!.showSnackBar()
                    }
                } else {
                    mView!!.initSnackBar()
                    if (!mView!!.isSnackBarNull) {
                        mView!!.showSnackBar()
                    }
                }
                return
            } catch (e: NullPointerException) {
                Timber.d(e)
            }

        }
        if (!mView!!.isSnackBarNull && mView!!.isSnackBarShown) {
            mView!!.dismissSnackBar()
        }

        if (stringMap[MediaPlayerApi.VARIABLES_STATE] == "2") {
            mView!!.displayPauseIcon()
        } else
            mView!!.displayPlayIcon()

        val currentTotalSeconds = stringMap[MediaPlayerApi.VARIABLES_POSITION]?.toIntOrNull() ?: 0
        stringMap[MediaPlayerApi.VARIABLES_POSITION_STRING]?.let { mView?.setCurrentPositionText(it) }

        mRemoteModel.totalSeconds = stringMap[MediaPlayerApi.VARIABLES_DURATION]?.toIntOrNull() ?: 0
        val s = " / " + stringMap[MediaPlayerApi.VARIABLES_DURATION_STRING]
        mView!!.setEndPositionText(s)

        var progress = currentTotalSeconds.toDouble() / mRemoteModel.totalSeconds.toDouble() * mRemoteModel.progressSeekBarMaxProgress.toDouble()
        mView!!.setProgressSeekBarProgress(progress.toInt())

        val volumeLevel = stringMap[MediaPlayerApi.VARIABLES_VOLUME_LEVEL]?.toIntOrNull() ?: 0
        progress = volumeLevel.toDouble() * mRemoteModel.volumeSeekBarMaxProgress / 100
        mView!!.setVolumesSeekBarProgress(progress.toInt())

        val fileName = stringMap[MediaPlayerApi.VARIABLES_FILE_NAME]
        fileName?.let { setToolbarTitle(it) }
    }

    companion object {

        private val INFO_UPDATE_INTERVAL_MS = 500
        private val SNAPSHOT_UPDATE_INTERVAL_MS = 5000
        private val JUMP_REPEAT_INTERVAL_MS = 200
        private val JUMP_REPEAT_START_DELAY_MS = 300
        private val UPDATE_SNAPSHOT_START_DELAY_MS = 100
    }
}
