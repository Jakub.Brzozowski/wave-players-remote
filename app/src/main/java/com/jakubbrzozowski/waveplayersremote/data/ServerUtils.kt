package com.jakubbrzozowski.waveplayersremote.data

import io.realm.Realm
import java.util.regex.Matcher
import java.util.regex.Pattern

object ServerUtils {

    fun isPortValid(port: String): Boolean {
        val portInt: Int
        try {
            portInt = Integer.parseInt(port)
            if (portInt > 65535 || portInt <= 0) {
                return false
            }
        } catch (e: NumberFormatException) {
            return false
        }

        return true
    }

    fun isIpAddress(ip: String): Boolean {
        val pat: Pattern
        val match: Matcher
        val ip4 = "IPv4"
        val ip6 = "IPv6"
        val non = "Neither"
        val ipv4 = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$"
        val ipv6 = "^[0-9a-f]{1,4}:" + "[0-9a-f]{1,4}:" +
                "[0-9a-f]{1,4}:" + "[0-9a-f]{1,4}:" +
                "[0-9a-f]{1,4}:" + "[0-9a-f]{1,4}:" +
                "[0-9a-f]{1,4}:" + "[0-9a-f]{1,4}$"

        if (ip.indexOf(".") > 0) {
            pat = Pattern.compile(ipv4)
            match = pat.matcher(ip)
            if (match.matches()) {
                return true
            }
        } else if (ip.indexOf(":") > 0) {
            pat = Pattern.compile(ipv6)
            match = pat.matcher(ip)
            if (match.matches()) {
                return true
            }
        } else {
            return false
        }
        return false
    }

    fun isServerDuplicate(newAddress: String, newPort: String,
                          currentServer: Server?): Boolean {
        val realm = Realm.getDefaultInstance()
        try {
            val server = realm.where(Server::class.java)
                    .equalTo(Server.ADDRESS, newAddress)
                    .equalTo(Server.PORT, newPort).findFirst()
            if (server != null) {
                if (server != currentServer) {
                    return true
                }
            }
        } finally {
            realm.close()
        }
        return false
    }

    fun isServerDuplicate(server: Server): Boolean {
        val realm = Realm.getDefaultInstance()
        val isDuplicate = realm.where(Server::class.java).equalTo(Server.ADDRESS, server.address)
                .equalTo(Server.PORT, server.port).findFirst() != null
        realm.close()
        return isDuplicate
    }
}