package com.jakubbrzozowski.waveplayersremote

import android.app.*
import android.app.PendingIntent.FLAG_IMMUTABLE
import android.app.PendingIntent.FLAG_UPDATE_CURRENT
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.ServiceInfo
import android.graphics.BitmapFactory
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.AsyncTask
import android.os.Build
import android.os.IBinder
import android.os.PowerManager
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import com.jakubbrzozowski.player_apis.MediaPlayerApi
import com.jakubbrzozowski.player_apis.mpc.hc.MediaPlayerClassicHomeCinema
import com.jakubbrzozowski.player_apis.vlc.VlcHttpClient
import com.jakubbrzozowski.waveplayersremote.ui.main.MainActivity
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class WaveControlService : Service(), SensorEventListener, SharedPreferences.OnSharedPreferenceChangeListener {
    internal var mStartMode: Int = 0       // indicates how to behave if the service is killed
    internal var mBinder: IBinder? = null      // interface for clients that bind
    internal var mAllowRebind: Boolean = false // indicates whether onRebind should be used
    internal lateinit var mPowerManager: PowerManager
    internal var mWakeLock: PowerManager.WakeLock? = null
    internal var mServerAddress: String? = null
    @Inject
    lateinit internal var mShPrefs: SharedPreferences
    private var mWaitTime: Int? = null
    private var mContext: Context? = null
    private var mSensorManager: SensorManager? = null
    private var mProximity: Sensor? = null
    private var mMediaPlayerApi: MediaPlayerApi? = null
    private var mProximityStateChecker: ProximityTogglePlayAsyncTask? = null
    private var mPort: Int = 0
    private var mPassword: String? = null
    private val mProximityStateCheckerSubject = PublishSubject.create<SensorEvent>()
    private val mDisposable = CompositeDisposable()

    override fun onCreate() {
        mStartMode = Service.START_STICKY
        (applicationContext as WavePlayersRemoteApplication).component.inject(this)
        mContext = applicationContext
        initializeSharedPreferences()
        mSensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        mProximity = mSensorManager!!.getDefaultSensor(Sensor.TYPE_PROXIMITY)
        mSensorManager!!.registerListener(this, mProximity, SensorManager.SENSOR_DELAY_FASTEST)
    }

    private fun initializeSharedPreferences() {
        mServerAddress = mShPrefs.getString(getString(R.string.pref_server_address_key),
                getString(R.string.pref_server_address_default))
        mPort = Integer.valueOf(mShPrefs.getString(getString(R.string.pref_server_port_key),
                getString(R.string.pref_server_port_default)))!!
        mPassword = mShPrefs.getString(getString(R.string.pref_server_pass_key), getString(R
                .string.pref_server_pass_default))
        val player = mShPrefs.getString(getString(R.string.pref_player_key),
                getString(R.string.pref_player_default))
        if (player == getString(R.string.pref_player_mpc_hc)) {
            mMediaPlayerApi = MediaPlayerClassicHomeCinema(mServerAddress, mPort, mPassword)
        } else if (player == getString(R.string.pref_player_vlc)) {
            mMediaPlayerApi = VlcHttpClient(mServerAddress, mPort, mPassword)
        } else
            mMediaPlayerApi = MediaPlayerClassicHomeCinema(mServerAddress, mPort, mPassword)

        val stringDelay = mShPrefs.getString(getString(R.string.pref_filter_delay_key),
                getString(R.string.pref_filter_delay_default))
        mWaitTime = Integer.parseInt(stringDelay)

        resetSubjectSubscription()

        mShPrefs.registerOnSharedPreferenceChangeListener(this)
    }

    private fun resetSubjectSubscription() {
        mDisposable.clear()
        mDisposable.add(
            mProximityStateCheckerSubject
                .debounce(mWaitTime!!.toLong(), TimeUnit.MILLISECONDS)
                .subscribe { event ->
                    val `val` = event.values[0]
                    if (`val` < mProximity!!.maximumRange) {
                        val vibrateOnWave = mShPrefs.getBoolean(
                            getString(R.string.pref_vibrate_on_wave_key),
                            resources.getBoolean(R.bool.pref_vibrate_on_wave_default)
                        )
                        val vibrationStrength = Integer.parseInt(
                            mShPrefs.getString(
                                getString(R.string.pref_vibration_strength_key),
                                getString(R.string.pref_vibration_strength_default)
                            )
                        )
                        mProximityStateChecker = ProximityTogglePlayAsyncTask(
                            mContext,
                            mMediaPlayerApi!!, mWaitTime
                        )
                        mProximityStateChecker!!.executeOnExecutor(
                            AsyncTask.THREAD_POOL_EXECUTOR,
                            vibrateOnWave, vibrationStrength
                        )
                    } else {
                        if (mProximityStateChecker != null) {
                            mProximityStateChecker!!.cancel(true)
                            mProximityStateChecker = null
                        }
                    }
                }
        )
    }

    fun initChannels(context: Context?) {
        if (Build.VERSION.SDK_INT < 26) {
            return
        }
        val notificationManager = context!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val channel = NotificationChannel(ID_PERSISTENT_NOTIFICATION_CHANNEL,
                getString(R.string.notification_persistent_channel_name),
                NotificationManager.IMPORTANCE_DEFAULT)
        channel.description = getString(R.string.notification_persistent_channel_description)
        notificationManager.createNotificationChannel(channel)
    }

    fun setupCustomNotification(): Notification {
        val openMainActivityIntent = Intent(mContext, MainActivity::class.java)
        openMainActivityIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
        val openMainActivityPendingIntent = PendingIntent.getActivity(
            mContext,
            1,
            openMainActivityIntent,
            FLAG_UPDATE_CURRENT or FLAG_IMMUTABLE
        )

        val closeMainActivityIntent = Intent("action_stop_service")
        val closeMainActivityPendingIntent = PendingIntent.getBroadcast(
            mContext,
            1,
            closeMainActivityIntent,
            FLAG_UPDATE_CURRENT or FLAG_IMMUTABLE
        )

        val contentView = RemoteViews(packageName, R.layout.main_notification)
        contentView.setImageViewResource(R.id.image, R.mipmap.ic_launcher)
        contentView.setTextViewText(R.id.main_notification_text_view, getString(R.string
                .app_name) + " " + getString(R.string.main_notification_textView_text))
        contentView.setOnClickPendingIntent(R.id.main_notification_button, closeMainActivityPendingIntent)
        contentView.setOnClickPendingIntent(R.id.main_notification_text_view, openMainActivityPendingIntent)

        initChannels(mContext)
        val largeIcon = BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher)
        val mBuilder = NotificationCompat.Builder(this,
                ID_PERSISTENT_NOTIFICATION_CHANNEL)
                .setCustomContentView(contentView)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBuilder.setSmallIcon(R.drawable.ic_wave_hand)
                    .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
        } else {
            mBuilder.setSmallIcon(R.mipmap.ic_launcher)
                    .setLargeIcon(largeIcon)
        }

        val notification = mBuilder.build()
        notification.contentIntent = openMainActivityPendingIntent
        return notification
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        mPowerManager = getSystemService(Context.POWER_SERVICE) as PowerManager

        if (mWakeLock == null) {
            mWakeLock = mPowerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    "wpr:ProximityListeningWakelock")
        }
        if (!mWakeLock!!.isHeld) {
            mWakeLock!!.acquire()
        }

        startForeground(1, setupCustomNotification() )

        mStartMode = Service.START_STICKY
        return mStartMode
    }

    override fun onBind(intent: Intent): IBinder? {
        // A client is binding to the service with bindService()
        return mBinder
    }

    override fun onUnbind(intent: Intent): Boolean {
        // All clients have unbound with unbindService()
        return mAllowRebind
    }

    override fun onRebind(intent: Intent) {
        // A client is binding to the service with bindService(),
        // after onUnbind() has already been called
    }

    override fun onDestroy() {
        mSensorManager!!.unregisterListener(this, mProximity)
        stopSelf()
        mShPrefs.unregisterOnSharedPreferenceChangeListener(this)
        // The service is no longer used and is being destroyed
    }

    override fun onSensorChanged(event: SensorEvent) {
        mProximityStateCheckerSubject.onNext(event)
    }

    override fun onTaskRemoved(rootIntent: Intent) {
        stopSelf()
        super.onTaskRemoved(rootIntent)
    }

    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {

    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        if (key == getString(R.string.pref_filter_delay_key)) {
            val filterDelay = sharedPreferences?.getString(
                    getString(R.string.pref_filter_delay_key),
                    getString(R.string.pref_filter_delay_default))
                ?: getString(R.string.pref_filter_delay_default)
            mWaitTime = Integer.parseInt(filterDelay)
        }

        if (key == getString(R.string.pref_server_address_key)) {
            mServerAddress = sharedPreferences?.getString(
                    getString(R.string.pref_server_address_key),
                    getString(R.string.pref_server_address_default))
            recreateMediaPlayerApi(sharedPreferences)
        }

        if (key == getString(R.string.pref_server_port_key)) {
            mPort = Integer.valueOf(sharedPreferences?.getString(
                    getString(R.string.pref_server_port_key),
                    getString(R.string.pref_server_port_default)))
            recreateMediaPlayerApi(sharedPreferences)
        }

        if (key == getString(R.string.pref_server_pass_key)) {
            mPassword = sharedPreferences?.getString(
                    getString(R.string.pref_server_pass_key),
                    getString(R.string.pref_server_pass_default))
            recreateMediaPlayerApi(sharedPreferences)
        }
        if (key == getString(R.string.pref_player_key)) {
            recreateMediaPlayerApi(sharedPreferences)
        }

        resetSubjectSubscription()
    }

    private fun recreateMediaPlayerApi(sharedPreferences: SharedPreferences?) {
        val player = sharedPreferences?.getString(getString(R.string.pref_player_key),
                getString(R.string.pref_player_key))
        if (player == getString(R.string.pref_player_mpc_hc)) {
            mMediaPlayerApi = MediaPlayerClassicHomeCinema(mServerAddress, mPort, mPassword)
        } else if (player == getString(R.string.pref_player_vlc)) {
            mMediaPlayerApi = VlcHttpClient(mServerAddress, mPort, mPassword)
        }
    }

    companion object {
        private val ID_PERSISTENT_NOTIFICATION_CHANNEL = "default"
    }

}
