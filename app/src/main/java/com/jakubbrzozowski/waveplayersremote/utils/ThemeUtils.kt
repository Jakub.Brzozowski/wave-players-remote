package com.jakubbrzozowski.waveplayersremote.utils


import android.content.SharedPreferences
import android.content.res.Resources
import android.preference.PreferenceManager
import androidx.appcompat.app.AppCompatActivity

import com.jakubbrzozowski.waveplayersremote.R
import com.jakubbrzozowski.waveplayersremote.ui.settings.SettingsActivity

object ThemeUtils {

    /**
     * Set the theme of the activity, according to the configuration.
     */
    fun onActivityCreateSetTheme(activity: AppCompatActivity) {
        val shPrefs = PreferenceManager.getDefaultSharedPreferences(activity)
        val res = activity.resources
        val prefPlayerKey = res.getString(R.string.pref_player_key)
        val prefPlayer = shPrefs.getString(prefPlayerKey, res.getString(R.string.pref_player_default))
        if (prefPlayer == res.getString(R.string.pref_player_mpc_hc)) {
            if (activity is SettingsActivity) {
                activity.setTheme(R.style.MpcPreferenceFixTheme)
            } else {
                activity.setTheme(R.style.AppTheme_NoActionBar)
            }
        } else if (prefPlayer == res.getString(R.string.pref_player_vlc)) {
            if (activity is SettingsActivity) {
                activity.setTheme(R.style.VlcPreferenceFixTheme)
            } else {
                activity.setTheme(R.style.VlcAppTheme_NoActionBar)
            }
        }
    }
}