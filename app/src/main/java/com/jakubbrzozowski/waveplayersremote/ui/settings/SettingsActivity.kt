package com.jakubbrzozowski.waveplayersremote.ui.settings

import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import com.jakubbrzozowski.waveplayersremote.R
import com.jakubbrzozowski.waveplayersremote.WavePlayersRemoteApplication
import com.jakubbrzozowski.waveplayersremote.ui.main.MainActivity.Companion.PERMISSIONS_REQUEST_READ_PHONE_STATE
import com.jakubbrzozowski.waveplayersremote.utils.ThemeUtils
import javax.inject.Inject

class SettingsActivity : AppCompatActivity() {

    @Inject
    lateinit internal var mShPrefs: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as WavePlayersRemoteApplication).component.inject(this)
        ThemeUtils.onActivityCreateSetTheme(this)
        setContentView(R.layout.activity_settings)

        supportFragmentManager.beginTransaction()
                .replace(android.R.id.content, SettingsFragment())
                .commitNow()

        if (intent != null && intent.action != null
                && intent.action == ACTION_CLOSE) {
            finish()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSIONS_REQUEST_READ_PHONE_STATE -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    mShPrefs.edit().putBoolean(getString(R.string.pref_pause_on_ringing_key),
                            false).apply()
                }
                return
            }
        }
    }

    companion object {

        val ACTION_CLOSE = "action_close"
    }

}
