package com.jakubbrzozowski.waveplayersremote.ui.remote

import com.jakubbrzozowski.player_apis.TimeCode

interface RemoteContract {

    interface Model {

        val progressSeekBarMaxProgress: Int

        val volumeSeekBarMaxProgress: Int

        var totalSeconds: Int

        val remoteFragmentTitle: String
        fun seek(timeCode: TimeCode)

        fun setVolume(desiredVolume: Int)

        fun togglePlay()

        fun nextAudioTrack()

        fun nextSubtitles()

        fun toggleFullscreen()

        fun toggleRepeatMode()

        fun nextFile()

        fun previousFile()

        fun jumpBackwardMedium()

        fun jumpForwardMedium()
    }

    interface View {

        val isSnackBarNull: Boolean

        val isSnackBarShown: Boolean

        val backgroundImageViewWith: Int
        fun setWaveHandIconOff()

        fun setWaveHandIconOn()

        fun showSnackBar()

        fun dismissSnackBar()

        fun initSnackBar()

        fun setProgressSeekBarProgress(progress: Int)

        fun setVolumesSeekBarProgress(progress: Int)

        fun displayPauseIcon()

        fun displayPlayIcon()

        fun setVlcBackground()

        fun loadMpcBackground(address: String, port: String)

        fun setCurrentPositionText(text: String)

        fun setEndPositionText(text: String)
    }

    interface Presenter {
        fun attachView(view: View)

        fun detachView()

        fun setWaveHandIcon(waveHandOn: Boolean)

        fun startInfoUpdate()

        fun togglePlay()

        fun nextAudioTrack()

        fun nextSubtitles()

        fun toggleFullscreen()

        fun toggleRepeatMode()

        fun nextFile()

        fun previousFile()

        fun waveHandToggle()

        fun snackBarChangeServerAction(fragmentClass: Class<*>, layoutId: Int)

        fun initSnackBar()

        fun initWaveHandIcon()

        fun invalidateOptionsMenu()

        fun unlockRightDrawer()

        fun dismissSnackBar()

        fun startSnapshotUpdate()

        fun startJumpingForward()

        fun stopJumpingForward()

        fun startJumpingBackward()

        fun stopJumpingBackward()

        fun stopUpdatingInfo()

        fun stopSnapshotUpdate()

        fun onVolumeSeekBarChange(progress: Int, fromUser: Boolean)

        fun onProgressSeekBarChange(progress: Int, fromUser: Boolean)

        fun updateSnapshot()

        fun setDefaultToolbarTitle()
    }
}
