package com.jakubbrzozowski.waveplayersremote.ui.remote


import android.content.res.Resources
import android.os.AsyncTask

import com.jakubbrzozowski.player_apis.TimeCode
import com.jakubbrzozowski.player_apis.WMCommand
import com.jakubbrzozowski.waveplayersremote.R
import com.jakubbrzozowski.waveplayersremote.ui.CommonModel
import com.jakubbrzozowski.waveplayersremote.utils.PlayerAsyncTask

import javax.inject.Inject

class RemoteModel @Inject
internal constructor(private val mRes: Resources, private val mCommonModel: CommonModel) : RemoteContract.Model {

    override val progressSeekBarMaxProgress: Int
    override val volumeSeekBarMaxProgress: Int
    override var totalSeconds: Int = 0

    override val remoteFragmentTitle: String
        get() = mRes.getString(R.string.remote_title)

    init {
        progressSeekBarMaxProgress = mRes.getInteger(R.integer.progress_seekbar_max_value)
        volumeSeekBarMaxProgress = mRes.getInteger(R.integer.volume_seekbar_max_value)
    }

    override fun seek(timeCode: TimeCode) {
        mCommonModel.mediaPlayerApi?.let {
            PlayerAsyncTask(it, timeCode).executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR, WMCommand.SEEK)
        }
    }

    override fun setVolume(desiredVolume: Int) {
        mCommonModel.mediaPlayerApi?.let {
            PlayerAsyncTask(it, desiredVolume).executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR, WMCommand.SET_VOLUME)
        }
    }

    override fun togglePlay() {
        mCommonModel.mediaPlayerApi?.let {
            PlayerAsyncTask(it).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                    WMCommand.PLAY_PAUSE)
        }
    }

    override fun nextAudioTrack() {
        mCommonModel.mediaPlayerApi?.let {
            PlayerAsyncTask(it).executeOnExecutor(AsyncTask
                    .THREAD_POOL_EXECUTOR,
                    WMCommand.NEXT_AUDIO)
        }
    }

    override fun nextSubtitles() {
        mCommonModel.mediaPlayerApi?.let {
            PlayerAsyncTask(it).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                    WMCommand.NEXT_SUBTITLE)
        }
    }

    override fun toggleFullscreen() {
        mCommonModel.mediaPlayerApi?.let {
            PlayerAsyncTask(it).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                    WMCommand.FULLSCREEN)
        }
    }

    override fun toggleRepeatMode() {
        mCommonModel.mediaPlayerApi?.let {
            PlayerAsyncTask(it).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                    WMCommand.TOGGLE_REPEAT)
        }
    }

    override fun nextFile() {
        mCommonModel.mediaPlayerApi?.let {
            PlayerAsyncTask(it).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                    WMCommand.NEXT_FILE)
        }
    }

    override fun previousFile() {
        mCommonModel.mediaPlayerApi?.let {
            PlayerAsyncTask(it).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                    WMCommand.PREVIOUS_FILE)
        }
    }

    override fun jumpBackwardMedium() {
        mCommonModel.mediaPlayerApi?.let {
            PlayerAsyncTask(it).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                    WMCommand.JUMP_BACKWARD_MEDIUM)
        }
    }

    override fun jumpForwardMedium() {
        mCommonModel.mediaPlayerApi?.let {
            PlayerAsyncTask(it).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                    WMCommand.JUMP_FORWARD_MEDIUM)

        }
    }
}
