package com.jakubbrzozowski.waveplayersremote.ui.filebrowser

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakubbrzozowski.player_apis.FileInfo
import com.jakubbrzozowski.waveplayersremote.R
import com.jakubbrzozowski.waveplayersremote.WavePlayersRemoteApplication
import com.jakubbrzozowski.waveplayersremote.ui.SimpleDividerItemDecoration
import java.util.*
import javax.inject.Inject

/**
 * Mandatory empty constructor for the fragment manager to instantiate the
 * fragment (e.g. upon screen orientation changes).
 */
class FileBrowserFragment : Fragment(), FileBrowserRecyclerViewAdapter.OnBrowserItemClickedListener, FileBrowserContract.View {
    @Inject
    lateinit internal var mPresenter: FileBrowserContract.Presenter
    internal var mRecyclerView: RecyclerView? = null
    private var mColumnCount = 1
    private var mAdapter: FileBrowserRecyclerViewAdapter? = null

    override val isFragmentAdded: Boolean
        get() = isAdded

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (context!!.applicationContext as WavePlayersRemoteApplication).component.inject(this)

        if (arguments != null) {
            mColumnCount = arguments!!.getInt(ARG_COLUMN_COUNT)
        }

        retainInstance = true
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (activity != null && (activity as AppCompatActivity).supportActionBar != null) {
            (activity as AppCompatActivity).supportActionBar!!.title = getString(R.string
                    .file_browser_title)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_filebrowser_list, container, false)
        mPresenter.attachView(this)
        // Set the adapter
        if (view is RecyclerView) {
            val context = view.getContext()
            mRecyclerView = view
            if (mColumnCount <= 1) {
                mRecyclerView!!.layoutManager = LinearLayoutManager(context)
            } else {
                mRecyclerView!!.layoutManager = GridLayoutManager(context, mColumnCount)
            }
            mRecyclerView!!.addItemDecoration(SimpleDividerItemDecoration(
                    getContext()))
            if (mAdapter == null) {
                mAdapter = FileBrowserRecyclerViewAdapter(ArrayList(), this)
            }

            mRecyclerView!!.adapter = mAdapter
        }
        return view
    }

    override fun onStart() {
        super.onStart()
        mPresenter.initFileInfoList()
    }

    override fun onDestroyView() {
        mPresenter.detachView()
        super.onDestroyView()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onBrowserItemClicked(info: FileInfo?) {
        mPresenter.openFileInfo(info)
    }

    override fun displayFileInfoList(fileInfo: List<FileInfo>) {
        mAdapter = FileBrowserRecyclerViewAdapter(fileInfo, this)
        if (mRecyclerView != null) {
            mRecyclerView!!.adapter = mAdapter
            mRecyclerView!!.invalidate()
        }
    }

    companion object {

        private val ARG_COLUMN_COUNT = "column-count"
    }
}
