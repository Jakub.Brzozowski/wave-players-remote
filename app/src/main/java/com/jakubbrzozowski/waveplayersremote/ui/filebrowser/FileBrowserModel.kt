package com.jakubbrzozowski.waveplayersremote.ui.filebrowser


import android.content.SharedPreferences
import android.content.res.Resources
import android.os.AsyncTask
import com.jakubbrzozowski.player_apis.FileInfo
import com.jakubbrzozowski.waveplayersremote.R
import com.jakubbrzozowski.waveplayersremote.ui.CommonModel
import com.jakubbrzozowski.waveplayersremote.utils.PlayerAsyncTask
import javax.inject.Inject

class FileBrowserModel @Inject
internal constructor(private val mRes: Resources,
                     private val mPrefs: SharedPreferences) : FileBrowserContract.Model {

    override val browserTitle: String
        get() = mRes.getString(R.string.file_browser_title)

    override fun openFile(mCommonModel: CommonModel, fileInfo: FileInfo?) {
        mCommonModel.mediaPlayerApi?.let {
            PlayerAsyncTask(it, fileInfo).executeOnExecutor(AsyncTask
                .THREAD_POOL_EXECUTOR, PlayerAsyncTask.OPEN_FILE_COMMAND)
        }
    }
}
