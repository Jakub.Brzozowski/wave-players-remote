package com.jakubbrzozowski.waveplayersremote.utils

import android.os.AsyncTask
import com.jakubbrzozowski.player_apis.*
import org.json.JSONException
import timber.log.Timber
import java.io.IOException
import java.lang.Exception

class PlayerAsyncTask : AsyncTask<Any, Void, Any> {
    private var mMediaPlayerApi: MediaPlayerApi
    private var mInputTimeCode: TimeCode? = null
    private var mInputVolume: Int? = null
    private var mFileInfo: FileInfo? = null

    constructor(mediaPlayerApi: MediaPlayerApi, fileInfo: FileInfo?) : super() {
        mMediaPlayerApi = mediaPlayerApi
        mFileInfo = fileInfo
    }

    constructor(mediaPlayerApi: MediaPlayerApi, inputVolume: Int) : super() {
        mMediaPlayerApi = mediaPlayerApi
        mInputVolume = inputVolume
    }

    constructor(mediaPlayerApi: MediaPlayerApi, inputTimeCode: TimeCode) : super() {
        mMediaPlayerApi = mediaPlayerApi
        mInputTimeCode = inputTimeCode
    }

    constructor(mediaPlayerApi: MediaPlayerApi) : super() {
        mMediaPlayerApi = mediaPlayerApi
    }

    override fun doInBackground(vararg command: Any): Any {
        try {
            if (command[0] is WMCommand) {
                val wmCommand = command[0] as WMCommand
                when (wmCommand) {
                    WMCommand.PLAY_PAUSE -> {
                        mMediaPlayerApi.togglePlayPause()
                        return RESULT_OK
                    }
                    WMCommand.JUMP_FORWARD_MEDIUM -> {
                        mMediaPlayerApi.jump(JUMP_FORWARD_MEDIUM_SECONDS)
                        return RESULT_OK
                    }
                    WMCommand.JUMP_BACKWARD_MEDIUM -> {
                        mMediaPlayerApi.jump(JUMP_BACKWARD_MEDIUM_SECONDS)
                        return RESULT_OK
                    }
                    WMCommand.SEEK -> {
                        mMediaPlayerApi.seek(mInputTimeCode)
                        return RESULT_OK
                    }
                    WMCommand.SET_VOLUME -> {
                        mMediaPlayerApi.volume = (mInputVolume ?: 0)
                        return RESULT_OK
                    }
                    WMCommand.NEXT_AUDIO -> {
                        mMediaPlayerApi.nextAudioTrack()
                        return RESULT_OK
                    }
                    WMCommand.NEXT_SUBTITLE -> {
                        mMediaPlayerApi.nextSubtitles()
                        return RESULT_OK
                    }
                    WMCommand.NEXT_FILE -> {
                        mMediaPlayerApi.nextFile()
                        return RESULT_OK
                    }
                    WMCommand.PREVIOUS_FILE -> {
                        mMediaPlayerApi.previousFile()
                        return RESULT_OK
                    }
                    WMCommand.PAUSE -> {
                        mMediaPlayerApi.pause()
                        return RESULT_OK
                    }
                    WMCommand.PLAY -> {
                        mMediaPlayerApi.play()
                        return RESULT_OK
                    }
                    WMCommand.FULLSCREEN -> {
                        mMediaPlayerApi.toggleFullscreen()
                        return RESULT_OK
                    }
                    WMCommand.TOGGLE_REPEAT -> {
                        mMediaPlayerApi.toggleRepeatMode()
                        return RESULT_OK
                    }
                    else -> return RESULT_UNKNOWN_COMMAND
                }
            }

            if (command[0] is String) {
                val commandString = command[0] as String
                when (commandString) {
                    OPEN_FILE_COMMAND -> {
                        mMediaPlayerApi.openFile(mFileInfo)
                        return RESULT_OK
                    }
                    else -> return RESULT_UNKNOWN_COMMAND
                }
            }
        } catch (e: IOException) {
            Timber.d(e)
            return RESULT_NOT_OK
        } catch (e: TimeCodeException) {
            Timber.d(e)
            return RESULT_NOT_OK
        } catch (e: JSONException) {
            Timber.d(e)
            return RESULT_NOT_OK
        } catch (e: Exception) {
            Timber.d(e)
            return RESULT_NOT_OK
        }

        return RESULT_UNKNOWN_COMMAND
    }

    companion object {

        const val RESULT_OK = "RESULT_OK"
        const val RESULT_NOT_OK = "RESULT_NOT_OK"
        const val RESULT_UNKNOWN_COMMAND = "RESULT_UNKNOWN_COMMAND"
        const val OPEN_FILE_COMMAND = "OPEN_FILE_COMMAND"
        private const val JUMP_FORWARD_MEDIUM_SECONDS = 5
        private const val JUMP_BACKWARD_MEDIUM_SECONDS = -JUMP_FORWARD_MEDIUM_SECONDS
    }
}
