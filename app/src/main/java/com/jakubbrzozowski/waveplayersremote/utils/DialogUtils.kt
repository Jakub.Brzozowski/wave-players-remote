package com.jakubbrzozowski.waveplayersremote.utils


import android.Manifest
import android.app.Activity
import androidx.core.app.ActivityCompat

import com.afollestad.materialdialogs.MaterialDialog
import com.jakubbrzozowski.waveplayersremote.R
import com.jakubbrzozowski.waveplayersremote.ui.main.MainActivity

object DialogUtils {

    fun getLegalNoticeDialog(activity: Activity): MaterialDialog {
        return MaterialDialog.Builder(activity)
                .title(R.string.legal_notice_title)
                .content(R.string.legal_notice_content)
                .positiveText("OK")
                .build()
    }

    fun getRingingPermissionDialog(activity: Activity): MaterialDialog {

        return MaterialDialog.Builder(activity)
                .title(R.string.permission_ringing_dialog_title)
                .content(R.string.permission_ringing_dialog_content)
                .positiveText(R.string.permission_ringing_dialog_positive_text)
                .cancelListener { _ -> requestPermission(activity) }
                .onAny { _, _ -> requestPermission(activity) }
                .build()
    }

    private fun requestPermission(activity: Activity) {
        ActivityCompat.requestPermissions(activity,
                arrayOf(Manifest.permission.READ_PHONE_STATE),
                MainActivity.PERMISSIONS_REQUEST_READ_PHONE_STATE)
    }
}
