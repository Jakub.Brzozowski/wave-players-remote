package com.jakubbrzozowski.waveplayersremote.ui.servermanager

import android.content.Context
import com.google.android.material.snackbar.Snackbar
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.afollestad.materialdialogs.MaterialDialog
import com.jakubbrzozowski.waveplayersremote.R
import com.jakubbrzozowski.waveplayersremote.WavePlayersRemoteApplication
import com.jakubbrzozowski.waveplayersremote.data.Server
import github.nisrulz.recyclerviewhelper.RVHAdapter
import github.nisrulz.recyclerviewhelper.RVHViewHolder
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter

/**
 * [RecyclerView.Adapter] that can display a [Server]
 */
class ServerManagerRecyclerViewAdapter internal constructor(data: OrderedRealmCollection<Server>?,
                                                            autoUpdate: Boolean,
                                                            private val mContext: Context,
                                                            private val mRecyclerView: RecyclerView,
                                                            private val mPresenter: ServerManagerContract.Presenter) : RealmRecyclerViewAdapter<Server, ServerManagerRecyclerViewAdapter.ServerViewHolder>(data, autoUpdate), RVHAdapter, ServerManagerContract.Adapter {
    private var mMaterialDialog: MaterialDialog? = null
    private var mFocusedItem = 0
    private var mSnackbar: Snackbar? = null

    init {
        (mContext.applicationContext as WavePlayersRemoteApplication).component.inject(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServerViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.server_list_item, parent, false)
        return ServerViewHolder(view)
    }


    override fun setServerDuplicationError() {
        if (mMaterialDialog != null) {
            val serverAddressEdiText = mMaterialDialog!!
                    .findViewById(R.id.dialog_server_address_editText) as EditText
            val serverPortEdiText = mMaterialDialog!!
                    .findViewById(R.id.dialog_server_port_editText) as EditText
            serverAddressEdiText.error = mContext.getString(R.string.err_msg_server_duplication)
            serverPortEdiText.error = mContext.getString(R.string.err_msg_server_duplication)
        }
    }

    override fun setAddressInvalidError() {
        if (mMaterialDialog != null) {
            val serverAddressEdiText = mMaterialDialog!!
                    .findViewById(R.id.dialog_server_address_editText) as EditText
            serverAddressEdiText.error = mContext.getString(R.string.err_msg_invalid_address)
        }
    }

    override fun setPortInvalidError() {
        if (mMaterialDialog != null) {
            val serverPortEdiText = mMaterialDialog!!
                    .findViewById(R.id.dialog_server_port_editText) as EditText
            serverPortEdiText.error = mContext.getString(R.string.err_msg_invalid_port)
        }
    }

    override fun onBindViewHolder(holder: ServerViewHolder, position: Int) {
        val server = getItem(position)
        holder.mItem = server
        holder.mNameTextView.text = server!!.name
        holder.mAddressView.text = server.address
        holder.mPortView.text = server.port

        val textColor: Int
        if (server.isResponding) {
            textColor = mContext.resources.getColor(R.color.textColorPrimary)
            holder.mNameTextView.setTextColor(textColor)
            holder.mAddressView.setTextColor(textColor)
            holder.mPortView.setTextColor(textColor)
            holder.mIcon.setImageResource(R.drawable.ic_computer_white_48px)
            holder.mStatus.setImageResource(R.drawable.ic_server_responding)
        } else {
            textColor = mContext.resources.getColor(R.color.textColorGreyedOut)
            holder.mNameTextView.setTextColor(textColor)
            holder.mAddressView.setTextColor(textColor)
            holder.mPortView.setTextColor(textColor)
            holder.mIcon.setImageResource(R.drawable.ic_computer_greyed_out_48px)
            holder.mStatus.setImageResource(R.drawable.ic_server_not_responding)
        }
        holder.mView.setOnLongClickListener(OnServerLongClickListener(holder))
        val res = mContext.resources
        // Check if holder holds server from preferences, if yes then mark it as selected
        holder.itemView.isSelected = mPresenter.isPreferredServer(position)
        holder.itemView.setOnClickListener { v ->
            if (server.isResponding) {
                // Redraw the old selection and the new
                notifyItemChanged(mFocusedItem)
                mFocusedItem = holder.layoutPosition
                notifyItemChanged(mFocusedItem)
                val newAddress = holder.mAddressView.text.toString().trim { it <= ' ' }
                val newPort = holder.mPortView.text.toString().trim { it <= ' ' }
                val newPassword = holder.mItem!!.password.trim { it <= ' ' }
                mPresenter.setPreferredServer(newAddress, newPort, newPassword)
                notifyDataSetChanged()
                mPresenter.goToRemote()
            }
        }
    }

    override fun getItemCount(): Int {
        return if (data != null && data!!.isValid) {
            data!!.size
        } else {
            0
        }
    }

    override fun onItemMove(fromPosition: Int, toPosition: Int): Boolean {
        return false
    }

    override fun onItemDismiss(position: Int, direction: Int) {
        onItemRemove(position)
    }

    private fun onItemRemove(position: Int) {
        if (mMaterialDialog != null && mMaterialDialog!!.isShowing) {
            notifyDataSetChanged()
            return
        }
        mSnackbar = Snackbar
                .make(mRecyclerView, "", Snackbar.LENGTH_LONG)
                .setAction(R.string.server_remove_snackbar_action_undo) { view ->
                    mPresenter.undoDeleteServer()
                    mRecyclerView.scrollToPosition(position)
                }
        mPresenter.onItemSwiped(position)
    }

    override fun setSnackbarText(text: String) {
        mSnackbar!!.setText(text)
    }

    override fun showSnackbar() {
        mSnackbar!!.show()
    }

    override fun dismissDialog() {
        if (mMaterialDialog != null) {
            mMaterialDialog!!.dismiss()
        }
    }

    inner class ServerViewHolder internal constructor(internal val mView: View) : RecyclerView
    .ViewHolder(mView), RVHViewHolder {
        internal val mNameTextView: TextView
        internal val mAddressView: TextView
        internal val mPortView: TextView
        internal val mStatus: ImageView
        internal val mIcon: ImageView
        internal var mItem: Server? = null

        init {
            mNameTextView = mView.findViewById(R.id.server_name_textView)
            mAddressView = mView.findViewById(R.id.server_address_textView)
            mPortView = mView.findViewById(R.id.server_port_textView)
            mStatus = mView.findViewById(R.id.server_status_imageView)
            mIcon = mView.findViewById(R.id.server_icon_imageView)
        }

        override fun onItemSelected(actionstate: Int) {}

        override fun onItemClear() {}

        override fun toString(): String {
            return super.toString() + " '" + mAddressView.text + "'"
        }
    }

    inner class OnServerLongClickListener internal constructor(private val mHolder: ServerViewHolder) : View.OnLongClickListener {

        override fun onLongClick(v: View): Boolean {
            val currentAddress = getItem(mHolder.adapterPosition)!!.address
            val currentName = getItem(mHolder.adapterPosition)!!.name
            val currentPort = getItem(mHolder.adapterPosition)!!.port
            val currentPassword = getItem(mHolder.adapterPosition)!!.password

            mMaterialDialog = MaterialDialog.Builder(mContext)
                    .title(mContext.getString(R.string.dialog_edit_server_title))
                    .customView(R.layout.server_edit_dialog, true)
                    .positiveText(mContext.getString(R.string.dialog_server_save_label))
                    .negativeText(mContext.getString(R.string.dialog_server_cancel_label))
                    .autoDismiss(false)
                    .onNegative { dialog, which -> dialog.dismiss() }
                    .onPositive { dialog, which ->
                        val rootView = dialog.customView
                        val serverNameEdiText = rootView!!.findViewById<EditText>(R.id.dialog_server_name_editText)
                        val serverAddressEdiText = rootView.findViewById<EditText>(R.id.dialog_server_address_editText)
                        val serverPortEdiText = rootView.findViewById<EditText>(R.id.dialog_server_port_editText)
                        val serverPasswordEdiText = rootView.findViewById<EditText>(R.id.dialog_server_password_editText)
                        val newName = serverNameEdiText.text.toString().trim { it <= ' ' }
                        val newAddress = serverAddressEdiText.text.toString().trim { it <= ' ' }
                        val newPort = serverPortEdiText.text.toString().trim { it <= ' ' }
                        val newPassword = serverPasswordEdiText.text.toString().trim { it <= ' ' }
                        var currentAddress1: String = ""
                        var currentName1: String = ""
                        var currentPort1: String = ""
                        var currentPassword1 = ""
                        try {
                            currentAddress1 = getItem(mHolder.adapterPosition)?.address.toString()
                            currentName1 = getItem(mHolder.adapterPosition)?.name.toString()
                            currentPort1 = getItem(mHolder.adapterPosition)?.port.toString()
                            currentPassword1 = getItem(mHolder.adapterPosition)?.password.toString()
                        } catch (e: ArrayIndexOutOfBoundsException) {
                            // Do nothing
                        }

                        if (mPresenter.editServer(currentName1, currentAddress1, currentPort1,
                                currentPassword1, newName, newAddress, newPort, newPassword)) {
                            dialog.dismiss()
                        }
                    }
                    .build()
            // Initialize EditTexts with values from server being edited
            val serverNameEdiText = mMaterialDialog!!.customView!!.findViewById<EditText>(R.id.dialog_server_name_editText)
            val serverAddressEdiText = mMaterialDialog!!.customView!!.findViewById<EditText>(R.id.dialog_server_address_editText)
            val serverPortEdiText = mMaterialDialog!!.customView!!.findViewById<EditText>(R.id.dialog_server_port_editText)
            val serverPasswordEdiText = mMaterialDialog!!.customView!!.findViewById<EditText>(R.id.dialog_server_password_editText)
            serverNameEdiText.setText(currentName)
            serverNameEdiText.setSelection(serverNameEdiText.text.length)
            serverAddressEdiText.setText(currentAddress)
            serverPortEdiText.setText(currentPort)
            serverPasswordEdiText.setText(currentPassword)

            mMaterialDialog!!.show()

            return true
        }

    }
}