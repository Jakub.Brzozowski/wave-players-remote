package com.jakubbrzozowski.waveplayersremote.ui.tutorial


interface TutorialFragmentInteractionListener {
    fun onPlayerPrefChanged(newPrefPlayer: String)
}
