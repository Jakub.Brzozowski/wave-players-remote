package com.jakubbrzozowski.waveplayersremote.dagger

import android.content.SharedPreferences
import android.content.res.Resources
import android.preference.PreferenceManager
import com.jakubbrzozowski.waveplayersremote.WavePlayersRemoteApplication
import com.jakubbrzozowski.waveplayersremote.ui.filebrowser.FileBrowserContract
import com.jakubbrzozowski.waveplayersremote.ui.filebrowser.FileBrowserPresenter
import com.jakubbrzozowski.waveplayersremote.ui.main.MainContract
import com.jakubbrzozowski.waveplayersremote.ui.main.MainModel
import com.jakubbrzozowski.waveplayersremote.ui.main.MainPresenter
import com.jakubbrzozowski.waveplayersremote.ui.remote.RemoteContract
import com.jakubbrzozowski.waveplayersremote.ui.remote.RemoteModel
import com.jakubbrzozowski.waveplayersremote.ui.remote.RemotePresenter
import com.jakubbrzozowski.waveplayersremote.ui.servermanager.ServerManagerContract
import com.jakubbrzozowski.waveplayersremote.ui.servermanager.ServerManagerModel
import com.jakubbrzozowski.waveplayersremote.ui.servermanager.ServerManagerPresenter
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Named
import javax.inject.Singleton

@Module
class StorageModule(private val mApplication: WavePlayersRemoteApplication) {

    @Provides
    @Singleton
    fun providesApplication(): WavePlayersRemoteApplication {
        return mApplication
    }

    @Provides
    @Singleton
     fun provideSharedPreferences(): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(mApplication)
    }

    @Provides
    @Singleton
     fun provideResources(): Resources {
        return mApplication.resources
    }

    @Provides
    @Singleton
     fun providesMainPresenter(presenter: MainPresenter): MainContract.Presenter {
        return presenter
    }

    @Provides
    @Singleton
     fun providesFileBrowserPresenter(presenter: FileBrowserPresenter): FileBrowserContract.Presenter {
        return presenter
    }

    @Provides
    @Singleton
    @Named("mainThread")
     fun provideMainScheduler(): Scheduler {
        return AndroidSchedulers.mainThread()
    }

    @Provides
    @Singleton
     fun providesRemotePresenter(presenter: RemotePresenter): RemoteContract.Presenter {
        return presenter
    }

    @Provides
    @Singleton
     fun providesServerManagerPresenter(
            presenter: ServerManagerPresenter): ServerManagerContract.Presenter {
        return presenter
    }

    @Provides
    @Singleton
    fun providesMainModel(
            model: MainModel): MainContract.Model {
        return model
    }

    @Provides
    @Singleton
    fun providesRemoteModel(
            model: RemoteModel): RemoteContract.Model {
        return model
    }

    @Provides
    @Singleton
    fun providesServerManagerModel(
            model: ServerManagerModel): ServerManagerContract.Model {
        return model
    }
}
