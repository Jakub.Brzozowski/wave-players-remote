package com.jakubbrzozowski.waveplayersremote.ui.servermanager


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.ItemTouchHelper
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.afollestad.materialdialogs.MaterialDialog
import com.jakubbrzozowski.waveplayersremote.R
import com.jakubbrzozowski.waveplayersremote.WavePlayersRemoteApplication
import com.jakubbrzozowski.waveplayersremote.ui.SimpleDividerItemDecoration
import github.nisrulz.recyclerviewhelper.RVHItemTouchHelperCallback
import timber.log.Timber
import javax.inject.Inject

/**
 * Mandatory empty constructor for the fragment manager to instantiate the
 * fragment (e.g. upon screen orientation changes).
 */
class ServerManagerFragment : Fragment(), ServerManagerContract.View {

    internal var mHelper: ItemTouchHelper? = null
    internal var mAdapter: ServerManagerRecyclerViewAdapter? = null
    internal var mRefreshButton: ImageView? = null
    @BindView(R.id.empty_server_list_message_textView)
    internal lateinit var mServerListMsgTextView: TextView
    @BindView(R.id.server_swiperefresh)
    internal lateinit var mRefreshLayout: SwipeRefreshLayout
    @Inject
    lateinit internal var mPresenter: ServerManagerContract.Presenter
    private var mRecyclerView: RecyclerView? = null
    private var mRotation: Animation? = null
    private var mMaterialDialog: MaterialDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d("onCreate")
        (requireContext().applicationContext as WavePlayersRemoteApplication).component.inject(this)
        retainInstance = true
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_server_manager, menu)
        if (mRefreshButton != null) {
            mRefreshButton!!.clearAnimation()
        }

        mRefreshButton = menu.findItem(R.id.action_refresh_servers).actionView as ImageView
        if (mRefreshButton != null) {
            mRefreshButton!!.setImageResource(R.drawable.ic_refresh_white_48px)
            mRefreshButton!!.setOnClickListener { view ->
                mRotation = AnimationUtils.loadAnimation(context, R.anim.clockwise_refresh)
                mRotation!!.fillAfter = false
                mRotation!!.reset()
                view.startAnimation(mRotation)
                mPresenter.scanForServers()
            }
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun clearRefreshAnimation() {
        if (mRefreshButton != null) {
            mRefreshButton!!.clearAnimation()
            mRefreshButton!!.animation = null
        }
        if (mRefreshLayout != null) {
            mRefreshLayout.isRefreshing = false
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Timber.d("onActivityCreated")
    }

    override fun setRefreshingMessage() {
        mServerListMsgTextView.setText(R.string.refreshing_server_list)
    }

    override fun setEmptyListMessage() {
        mServerListMsgTextView.setText(R.string.empty_server_list)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_server_list, container, false)
        Timber.d("onCreateView")
        ButterKnife.bind(this, view)

        mRefreshLayout.setOnRefreshListener { mPresenter.scanForServers() }

        mRecyclerView = view.findViewById(R.id.server_list_recyclerView)
        // Set the adapter
        if (mRecyclerView != null) {
            mRecyclerView!!.layoutManager = LinearLayoutManager(context)
            mAdapter = context?.let { ServerManagerRecyclerViewAdapter(mPresenter.data, true, it, mRecyclerView!!, mPresenter) }
            mRecyclerView!!.adapter = mAdapter
            val callback = RVHItemTouchHelperCallback(mAdapter, false, true,
                    true)
            mHelper = ItemTouchHelper(callback)
            mRecyclerView!!.addItemDecoration(SimpleDividerItemDecoration(context))
            mHelper?.attachToRecyclerView(mRecyclerView)
        }
        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Timber.d("onDestroyView")
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.d("onDestroy")
    }

    override fun onStart() {
        super.onStart()
        mPresenter.attachView(this, mAdapter)
        mPresenter.initServerList()
        mPresenter.checkServers()
        mPresenter.invalidateOptionsMenu()
        mPresenter.lockRightDrawer()
        mPresenter.setToolbarTitle()
        mAdapter?.notifyDataSetChanged()
        Timber.d("onStart")
    }

    override fun onStop() {
        mPresenter.detachView()
        if (mRefreshButton != null) {
            mRefreshButton!!.clearAnimation()
        }
        Timber.d("onStop")
        super.onStop()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Timber.d("onAttach")
    }

    @OnClick(R.id.add_server_fab)
    fun addServer() {
        val context = context
        mMaterialDialog = MaterialDialog.Builder(context!!)
                .title(context.getString(R.string.dialog_add_server_title))
                .customView(R.layout.server_edit_dialog, true)
                .positiveText(context.getString(R.string.dialog_add_server_save_label))
                .negativeText(context.getString(R.string.dialog_server_cancel_label))
                .autoDismiss(false)
                .onNegative { dialog, which -> dialog.dismiss() }
                .onPositive { dialog, which ->
                    val rootView = dialog.customView
                    val serverNameEdiText = rootView!!.findViewById<EditText>(R.id.dialog_server_name_editText)
                    val serverAddressEdiText = rootView.findViewById<EditText>(R.id.dialog_server_address_editText)
                    val serverPortEdiText = rootView.findViewById<EditText>(R.id.dialog_server_port_editText)
                    val serverPasswordEdiText = rootView.findViewById<EditText>(R.id.dialog_server_password_editText)
                    val newName = serverNameEdiText.text.toString().trim { it <= ' ' }
                    val newAddress = serverAddressEdiText.text.toString().trim { it <= ' ' }
                    val newPort = serverPortEdiText.text.toString().trim { it <= ' ' }
                    val newPassword = serverPasswordEdiText.text.toString().trim { it <= ' ' }

                    if (mPresenter.createServer(newAddress, newPort, newName, newPassword)) {
                        dialog.dismiss()
                    }
                }
                .build()

        val serverNameEdiText = mMaterialDialog!!.customView!!
                .findViewById<EditText>(R.id.dialog_server_name_editText)
        val serverAddressEdiText = mMaterialDialog!!.customView!!
                .findViewById<EditText>(R.id.dialog_server_address_editText)
        val serverPasswordEdiText = mMaterialDialog!!.customView!!
                .findViewById<EditText>(R.id.dialog_server_password_editText)
        serverNameEdiText.setText(R.string.prefill_add_server_name)
        serverAddressEdiText.setText(R.string.prefill_add_server_address)
        mPresenter.prefillPort()
        serverPasswordEdiText.setText(getString(R.string.pref_server_pass_default))

        serverNameEdiText.setSelection(serverNameEdiText.text.length)

        mMaterialDialog!!.show()
    }

    override fun prefillPortVlc() {
        if (mMaterialDialog != null) {
            val serverPortEdiText = mMaterialDialog!!
                    .findViewById(R.id.dialog_server_port_editText) as EditText
            serverPortEdiText.setText(R.string.port_default_vlc)
        }
    }

    override fun prefillPortMpc() {
        if (mMaterialDialog != null) {
            val serverPortEdiText = mMaterialDialog!!
                    .findViewById(R.id.dialog_server_port_editText) as EditText
            serverPortEdiText.setText(R.string.port_default_mpc)
        }
    }

    override fun setServerDuplicationError() {
        if (mMaterialDialog != null) {
            val serverAddressEdiText = mMaterialDialog!!
                    .findViewById(R.id.dialog_server_address_editText) as EditText
            val serverPortEdiText = mMaterialDialog!!
                    .findViewById(R.id.dialog_server_port_editText) as EditText
            serverAddressEdiText.error = getString(R.string.err_msg_server_duplication)
            serverPortEdiText.error = getString(R.string.err_msg_server_duplication)
        }
    }

    override fun setAddressInvalidError() {
        if (mMaterialDialog != null) {
            val serverAddressEdiText = mMaterialDialog!!
                    .findViewById(R.id.dialog_server_address_editText) as EditText
            serverAddressEdiText.error = getString(R.string.err_msg_invalid_address)
        }
    }

    override fun setPortInvalidError() {
        if (mMaterialDialog != null) {
            val serverPortEdiText = mMaterialDialog!!
                    .findViewById(R.id.dialog_server_port_editText) as EditText
            serverPortEdiText.error = getString(R.string.err_msg_invalid_port)
        }
    }

    override fun setServerListEmptyLayout() {
        mRecyclerView!!.visibility = View.GONE
        mServerListMsgTextView.visibility = View.VISIBLE
    }

    override fun setServerListNotEmptyLayout() {
        mRefreshLayout.isRefreshing = false
        mRecyclerView!!.visibility = View.VISIBLE
        mServerListMsgTextView.visibility = View.GONE
    }
}
