package com.jakubbrzozowski.player_apis.mpc.hc;


import com.jakubbrzozowski.player_apis.FileInfo;
import com.jakubbrzozowski.player_apis.MediaPlayerApi;
import com.jakubbrzozowski.player_apis.TimeCode;
import com.jakubbrzozowski.player_apis.TimeCodeException;
import com.jakubbrzozowski.player_apis.WMCommand;

import org.json.JSONException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Client for the web interface exposed by MPC-HC.
 */
public class MediaPlayerClassicHomeCinema extends MediaPlayerApi {
    private static final String COMMAND_PARAM_NAME = "wm_command";
    private static final Pattern VERSION_PATTERN =
            Pattern.compile("« (MPC-HC v\\d+.\\d+.\\d+.\\d+).*");
    private boolean toggleSubtitleFlag = false;
    private int toggleRepeatSwitch = 0;

    /**
     * Construct a new instance of the media player.
     *
     * @param hostname The hostname or IP address of the machine that MPC-HC is running on
     * @param port     The port that MPC-HC is listening on
     */
    public MediaPlayerClassicHomeCinema(String hostname, int port, String pass) {
        super(hostname, port, pass);
        commandEndPoint = baseURI + "/command.html";
        infoEndPoint = baseURI + "/info.html";
        variableEndPoint = baseURI + "/variables.html";
        okHttpClient = new OkHttpClient.Builder()
                .readTimeout(READ_TIMEOUT, TimeUnit.MILLISECONDS)
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.MILLISECONDS).build();
        mBrowserRootUriPath = "/browser.html?path=";
    }

    /**
     * Get a list of {@link FileInfo} objects for the given path.
     *
     * @param file The FileInfo object from the FileTable
     * @return A list of {@link FileInfo} objects for the given path
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public List<FileInfo> browse(FileInfo file) throws IOException {
        if (!file.isDirectory()) {
            throw new IllegalArgumentException("Argument must be a directory");
        }
        return browse(file.getHref());
    }

    /**
     * Get a list of {@link FileInfo} objects for the given path.
     *
     * @param href The relative path to the file, as returned by
     *             {@link FileInfo#getHref()}
     * @return A list of {@link FileInfo} objects for the given path
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    protected List<FileInfo> browse(String href) throws IOException {
        Response response = get(baseURI + href);
        Document document = Jsoup.parse(response.body().string());

        Elements tables = document.select("table");
        Element table = tables.get(tables.size() - 1);
        return FileInfo.fromHTMLTableElement(table);
    }

    /**
     * Open the file pointed to by the href with MPC-HC.
     *
     * @param file The {@link FileInfo} to open
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public void openFile(FileInfo file) throws IOException {
        if (file.isDirectory()) {
            throw new IllegalArgumentException("Argument must be a file");
        }
        get(baseURI + file.getHref());
    }

    /**
     * Send the command to MPC-HC.
     *
     * @param command The command to execute
     * @param args    Any additional key value pairs to add to the form
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    @Override
    public void execute(final WMCommand command,
                        KeyValuePair... args) throws IOException {
        FormBody.Builder formBuilder = new FormBody.Builder()
                .add(COMMAND_PARAM_NAME, Integer.toString(command.getValue()));
        for (KeyValuePair arg : args) {
            formBuilder.add(arg.getKey(), arg.getValue());
        }

        Request request = new Request.Builder()
                .url(commandEndPoint)
                .post(formBuilder.build())
                .build();
        Response response = null;
        try {
            response = okHttpClient.newCall(request).execute();
            if (!response.isSuccessful()) {
                throw new IOException("Unexpected code: " + response);
            }
        } finally {
            if (response != null) {
                response.body().close();
            }
        }
    }

    /**
     * Get the info string from MPC-HC.
     *
     * @return The info string
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public String getInfo() throws IOException {
        Response response = get(infoEndPoint);
        Document document = Jsoup.parse(response.body().string());
        Element element = document.getElementById("mpchc_np");
        return element.text();
    }

    /**
     * Get a map of all web interface exposed MPC-HC variables.
     *
     * @return The variables
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public Map<String, String> getVariables() throws IOException {
        Response response = get(variableEndPoint);

        final String html = response.body().string();
        Document document = Jsoup.parse(html);
        Map<String, String> variables = new HashMap<String, String>();
        for (String variableID : VARIABLE_IDS) {
            Element element = document.getElementById(variableID);
            String value;
            if (element == null && variableID == VARIABLES_FILE_NAME) {
                value = "";
            } else {
                value = element.text();
            }
            if (variableID.equals(VARIABLES_POSITION) | variableID.equals(VARIABLES_DURATION)) {
                value = String.valueOf(Integer.parseInt(value) / 1000);
            }
            variables.put(variableID, value);
        }

        if (variables.get(VARIABLES_FILE_NAME) == null || variables.get(VARIABLES_FILE_NAME).isEmpty()) {
            String fileName = variables.get(VARIABLES_FILE_PATH);
            fileName = fileName.substring(fileName.lastIndexOf('\\') + 1);
            variables.put(VARIABLES_FILE_NAME, fileName);
        }

        return variables;
    }

    //
    // Convenience methods
    //

    /**
     * Returns true if the player is playing.
     *
     * @return True if the player is playing
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public boolean isPlaying() throws IOException, JSONException {
        return getVariables().get(VARIABLES_STATE_STRING).equals("Playing");
    }

    /**
     * Returns true if the player is paused.
     *
     * @return True if the player is paused
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public boolean isPaused() throws IOException, JSONException {
        return getVariables().get(VARIABLES_STATE_STRING).equals("Paused");
    }

    /**
     * Returns true if the player is stopped.
     *
     * @return True if the player is stopped
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public boolean isStopped() throws IOException, JSONException {
        return getVariables().get(VARIABLES_STATE_STRING).equals("Stopped");
    }

    /**
     * Returns true if no file is open.
     *
     * @return True if no file is open
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public boolean isClosed() throws IOException, JSONException {
        return getVariables().get(VARIABLES_STATE).equals("-1");
    }

    /**
     * Set the volume (0 to 100). Values outside of this range are clamped.
     *
     * @param volume The volume to set
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public void setVolume(int volume) throws IOException {
        if (volume < 0) {
            volume = 0;
        } else if (volume > 100) {
            volume = 100;
        }

        execute(WMCommand.SET_VOLUME,
                new KeyValuePair("volume", Integer.toString(volume)));
    }

    /**
     * Seek to the given time code. If the give time is larger than the duration
     * of the video, seek to the end.
     *
     * @param where Where to seek
     * @throws IOException       If the HTTP call receives an unexpected response code
     * @throws TimeCodeException This should never happen
     */
    public void seek(TimeCode where) throws IOException, TimeCodeException, JSONException {
        if (where.getTotalSeconds() > getDuration().getTotalSeconds()) {
            where = getDuration();
        }
        execute(WMCommand.SEEK, new KeyValuePair("position", where.toString()));
    }

    @Override
    public void toggleRepeatMode() throws IOException {
        switch (toggleRepeatSwitch) {
            case 0:
            case 2:
                execute(WMCommand.REPEAT_PLAY_FOREVER);
                toggleRepeatSwitch++;
                break;
            case 1:
                execute(WMCommand.REPEAT_PLAY_FOREVER);
                execute(WMCommand.REPEAT_PLAY_ONE_FILE);
                toggleRepeatSwitch++;
                break;
            case 3:
                execute(WMCommand.REPEAT_PLAY_FOREVER);
                execute(WMCommand.REPEAT_PLAY_WHOLE_PLAYLIST);
                toggleRepeatSwitch++;
                break;
        }
        if (toggleRepeatSwitch > 3) {
            toggleRepeatSwitch = 0;
        }
    }

    public void nextSubtitles() throws IOException {
        if (toggleSubtitleFlag) {
            execute(WMCommand.ON_OFF_SUBTITLE);
            toggleSubtitleFlag = !toggleSubtitleFlag;
        } else {
            execute(WMCommand.ON_OFF_SUBTITLE);
            execute(WMCommand.NEXT_SUBTITLE);
            toggleSubtitleFlag = !toggleSubtitleFlag;
        }
    }

    /**
     * Get the version string (MPC-HC vX.X.X.X).
     *
     * @return The version string
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public String getVersion() throws IOException {
        Matcher matcher = VERSION_PATTERN.matcher(getInfo());
        return matcher.group(1);
    }
}
