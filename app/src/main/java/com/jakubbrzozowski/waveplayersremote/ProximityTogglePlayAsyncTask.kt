package com.jakubbrzozowski.waveplayersremote

import android.annotation.SuppressLint
import android.content.Context
import android.os.AsyncTask
import android.os.Vibrator
import com.jakubbrzozowski.player_apis.MediaPlayerApi
import timber.log.Timber
import java.io.IOException

class ProximityTogglePlayAsyncTask(@field:SuppressLint("StaticFieldLeak")
                                   var mContext: Context?, private val mMediaPlayerApi: MediaPlayerApi,
                                   private val mWaitTime: Int?) : AsyncTask<Any, Void, String>() {

    override fun doInBackground(params: Array<Any>): String {
        try {
            val vibrateOnWave = params[0] as Boolean
            val vibrationStrength = params[1] as Int
            val vibTime = 25 * vibrationStrength
            val vibTimeLonger = 2 * vibTime
            Thread.sleep(mWaitTime!!.toLong())
            try {
                val vibrator = mContext!!.getSystemService(Context.VIBRATOR_SERVICE) as? Vibrator
                if (vibrateOnWave && vibrator != null) {
                    vibrator.vibrate(vibTime.toLong())
                }
                mMediaPlayerApi.togglePlayPause()
                Thread.sleep(vibTimeLonger.toLong())
                if (vibrateOnWave && vibrator != null) {
                    vibrator.vibrate(vibTimeLonger.toLong())
                }
                Thread.sleep(vibTimeLonger.toLong())
            } catch (e: IOException) {
                Timber.d(e)
            }

        } catch (e: InterruptedException) {
            Timber.d(e)
        } finally {
            mContext = null
        }
        return "some message"
    }
}
