package com.jakubbrzozowski.waveplayersremote.eventbus


class LoadFragmentEvent(val fragmentClass: Class<*>, val layoutId: Int)
