package com.jakubbrzozowski.waveplayersremote.ui.servermanager

import com.jakubbrzozowski.waveplayersremote.data.Server

import io.realm.OrderedRealmCollection
import io.realm.RealmResults
import io.realm.internal.util.Pair

interface ServerManagerContract {

    interface Model {

        val deleteServerSnackbarText: String

        val defaultToolbarTitle: String

        val servers: RealmResults<Server>
        fun editServer(currentAddress: String, currentPort: String, newServer: Server)

        fun remove(server: Server)

        fun setPreferredServer(newAddress: String, newPort: String, newPassword: String)

        fun undoDeleteServer()

        fun saveNewServer(newServer: Server)

        fun checkAndUpdatePreferences(currentAddress: String, currentPort: String, currentPassword: String, newAddress: String, newPort: String, newPassword: String)

        fun buildCheckServerUrlString(server: Server, address: String?, port: String?, scanType: Int): Pair<Server, String>

        fun checkServer(server: Server, url: String, scanType: Int): Pair<Server, Boolean>

        fun setResponding(server: Server, isResponding: Boolean)

        fun setServersNotResponding(servers: RealmResults<Server>)

        fun scanServer(ipBytes: ByteArray?, ipFourthByte: Int, port: String, scanType: Int): Int

        fun openDatabaseConnection()

        fun closeDatabaseConnection()
    }

    interface View {

        fun clearRefreshAnimation()

        fun setRefreshingMessage()

        fun setEmptyListMessage()

        fun prefillPortVlc()

        fun prefillPortMpc()

        fun setServerDuplicationError()

        fun setAddressInvalidError()

        fun setPortInvalidError()

        fun setServerListEmptyLayout()

        fun setServerListNotEmptyLayout()
    }

    interface Adapter {
        fun notifyItemRemoved(position: Int)

        fun notifyDataSetChanged()

        fun setSnackbarText(text: String)

        fun showSnackbar()

        fun setPortInvalidError()

        fun setAddressInvalidError()

        fun setServerDuplicationError()

        fun dismissDialog()
    }

    interface Presenter {

        val data: OrderedRealmCollection<Server>
        fun attachView(view: View, adapter: Adapter?)

        fun detachView()

        fun lockRightDrawer()

        fun invalidateOptionsMenu()

        fun initServerList()

        fun checkServers()

        fun setToolbarTitle()

        fun createServer(newAddress: String, newPort: String, newName: String, newPassword: String): Boolean

        fun onItemSwiped(position: Int)

        fun undoDeleteServer()

        fun isPreferredServer(position: Int): Boolean

        fun setPreferredServer(newAddress: String, newPort: String, newPassword: String)

        fun editServer(currentName: String, currentAddress: String, currentPort: String,
                       currentPassword: String, newName: String, newAddress: String, newPort: String,
                       newPassword: String): Boolean

        fun prefillPort()

        fun goToRemote()

        fun scanForServers()
    }
}
