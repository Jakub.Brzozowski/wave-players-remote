package com.jakubbrzozowski.player_apis.vlc;


import com.jakubbrzozowski.player_apis.FileInfo;
import com.jakubbrzozowski.player_apis.MediaPlayerApi;
import com.jakubbrzozowski.player_apis.TimeCode;
import com.jakubbrzozowski.player_apis.TimeCodeException;
import com.jakubbrzozowski.player_apis.WMCommand;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Authenticator;
import okhttp3.Credentials;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

/**
 * Client for the web interface exposed by MPC-HC.
 */
public class VlcHttpClient extends MediaPlayerApi {
    private static final String COMMAND_PARAM_NAME = "command";


    /**
     * Construct a new instance of the media player.
     *
     * @param hostname The hostname or IP address of the machine that MPC-HC is running on
     * @param port     The port that MPC-HC is listening on
     */
    public VlcHttpClient(String hostname, int port, String pass) {
        super(hostname, port, pass);
        pathSegments = "requests/status.json";
        commandEndPoint = baseURI + "/" + pathSegments;
        infoEndPoint = commandEndPoint;
        okHttpClient = new OkHttpClient.Builder()
                .readTimeout(READ_TIMEOUT, TimeUnit.MILLISECONDS)
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.MILLISECONDS)
                .authenticator(new Authenticator() {
                    @Override
                    public Request authenticate(Route route, Response response) throws IOException {
                        System.out.println("Authenticating for response: " + response);
                        System.out.println("Challenges: " + response.challenges());
                        String credential = Credentials.basic("", password);
                        return response.request().newBuilder()
                                .header("Authorization", credential)
                                .build();
                    }
                }).build();
        mBrowserRootUriPath = "/requests/browse.json?dir=";
    }

    public static OkHttpClient.Builder getVlcOkHttpClient(final String pass) {
        return new OkHttpClient.Builder()
                .authenticator(new Authenticator() {
                    @Override
                    public Request authenticate(Route route, Response response) throws IOException {
                        System.out.println("Authenticating for response: " + response);
                        System.out.println("Challenges: " + response.challenges());
                        String credential = Credentials.basic("", pass);
                        return response.request().newBuilder()
                                .header("Authorization", credential)
                                .build();
                    }
                });
    }

    /**
     * Get a list of {@link FileInfo} objects for the given path.
     *
     * @param file The FileInfo object from the FileTable
     * @return A list of {@link FileInfo} objects for the given path
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public List<FileInfo> browse(FileInfo file) throws IOException, JSONException {
        if (!file.isDirectory()) {
            throw new IllegalArgumentException("Argument must be a directory");
        }

        List<FileInfo> fileList = browse(mBrowserRootUriPath + file.getHref());
        boolean hasUpNav = false;

        for (int i = 0; i < fileList.size(); i++) {
            FileInfo fileInfo = fileList.get(i);
            if (fileInfo.getFileName().equals("..") || fileInfo.getHref().endsWith("/..")) {
                hasUpNav = true;
                break;
            }
        }

        if (!hasUpNav && !file.getHref().equals("")) {
            String href = file.getHref();
            String upNavPath = "";
            int slashCount = href.length() - href.replaceAll("[/]", "").length();
            // If href length is longer than 3 the href points to something other than drive
            if (href.length() > 3) {
                // If more than one occurrence, then it's safe to remove one path segment
                if (slashCount > 1) {
                    upNavPath = href.substring(0, href.lastIndexOf('/'));
                    upNavPath = upNavPath.substring(0, upNavPath.lastIndexOf('/'));
                    if (upNavPath.length() < 3) {
                        upNavPath = "";
                    }
                } else if (slashCount == 1) {
                    upNavPath = href.substring(0, href.lastIndexOf('/'));
                }
            }

            fileList.add(0, new FileInfo(
                    "..",
                    upNavPath,
                    "dir",
                    "0",
                    "0",
                    true));
        }

        return fileList;
    }

    /**
     * Get a list of {@link FileInfo} objects for the given path.
     *
     * @param href The relative path to the file, as returned by
     *             {@link FileInfo#getHref()}
     * @return A list of {@link FileInfo} objects for the given path
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    protected List<FileInfo> browse(String href) throws IOException, JSONException {
        Response response = get(baseURI + href);
        return FileInfo.fromJson(response.body().string());
    }

    /**
     * Open the file pointed to by the href.
     *
     * @param file The {@link FileInfo} to open
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    @Override
    public void openFile(FileInfo file) throws IOException {
        if (file.isDirectory()) {
            throw new IllegalArgumentException("Argument must be a file");
        }
        if (file.isPlaylist()) {
            execute(WMCommand.EMPTY_PLAYLIST);
        }
        execute(WMCommand.OPEN_FILE, new KeyValuePair("input", "file:///" + file.getHref()));
    }

    /**
     * Send the command to MPC-HC.
     *
     * @param command The command to execute
     * @param args    Any additional key value pairs to add to the form
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    @Override
    public void execute(final WMCommand command,
                        KeyValuePair... args) throws IOException {
        VlcCommand vlcCommand = VlcCommand.wmCommandToVlcCommand.get(command);
        HttpUrl.Builder urlBuilder = new HttpUrl.Builder()
                .scheme(SCHEME)
                .host(hostAddress)
                .port(hostPort)
                .addPathSegments(pathSegments)
                .addQueryParameter(COMMAND_PARAM_NAME, vlcCommand.getValue());

        for (KeyValuePair arg : args) {
            urlBuilder.addQueryParameter(arg.getKey(), arg.getValue());
        }

        HttpUrl url = urlBuilder.build();
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();

        Response response = null;
        try {
            response = okHttpClient.newCall(request).execute();
            if (!response.isSuccessful()) {
                throw new IOException("Unexpected code: " + response);
            }
        } finally {
            if (response != null) {
                response.body().close();
            }
        }
    }

    /**
     * Get the info string from MPC-HC.
     *
     * @return The info string
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public String getInfo() throws IOException {
        throw new UnsupportedOperationException();
    }

    /**
     * Get a map of all web interface exposed MPC-HC variables.
     *
     * @return The variables
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public Map<String, String> getVariables() throws IOException, JSONException {
        Response response = null;
        try {
            response = get(infoEndPoint);
            final String responseString = response.body().string();
            JSONObject rootJson = new JSONObject(responseString);
            JSONObject information = rootJson.getJSONObject("information");
            JSONObject category = information.getJSONObject("category");
            JSONObject meta = category.getJSONObject("meta");
            Map<String, String> variables = new HashMap<String, String>();

            // File name
            String fileName = meta.getString("filename");
            fileName = fileName.substring(fileName.lastIndexOf('/') + 1);
            variables.put(VARIABLES_FILE_NAME, fileName);

            // State
            String stateString = rootJson.getString("state");
            String stateInt;
            switch (stateString) {
                case "stopped":
                    stateInt = "0";
                    break;
                case "paused":
                    stateInt = "1";
                    break;
                default:
                    stateInt = "2";
                    break;
            }
            variables.put(VARIABLES_STATE, stateInt);

            // Position
            int positionSeconds = rootJson.getInt("time");
            variables.put(VARIABLES_POSITION, String.valueOf(positionSeconds));

            // Position string
            int hours = positionSeconds / 3600;
            positionSeconds = positionSeconds % 3600;
            int minutes = positionSeconds / 60;
            positionSeconds = positionSeconds % 60;
            String positionString = String.format("%02d:%02d:%02d", hours, minutes, positionSeconds);
            variables.put(VARIABLES_POSITION_STRING, positionString);

            // Duration
            int durationSeconds = rootJson.getInt("length");
            variables.put(VARIABLES_DURATION, String.valueOf(durationSeconds));

            //Duration string
            hours = durationSeconds / 3600;
            durationSeconds = durationSeconds % 3600;
            minutes = durationSeconds / 60;
            durationSeconds = durationSeconds % 60;
            String durationString = String.format("%02d:%02d:%02d", hours, minutes,
                    durationSeconds);
            variables.put(VARIABLES_DURATION_STRING, durationString);

            // Volume level
            int volume = (int) Math.round(rootJson.getInt("volume") / 3.2);
            variables.put(VARIABLES_VOLUME_LEVEL, String.valueOf(volume));

            return variables;
        } finally {
            if (response != null) {
                response.body().close();
            }
        }
    }

    //
    // Convenience methods
    //

    @Override
    public boolean isPlaying() throws IOException, JSONException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isPaused() throws IOException, JSONException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isStopped() throws IOException, JSONException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isClosed() throws IOException, JSONException {
        throw new UnsupportedOperationException();
    }

    /**
     * Set the volume (0 to 100). Values outside of this range are clamped.
     *
     * @param volume The volume to set
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public void setVolume(int volume) throws IOException {
        if (volume < 0) {
            volume = 0;
        } else if (volume > 100) {
            volume = 100;
        }

        execute(WMCommand.SET_VOLUME,
                new KeyValuePair("val", Double.toString(volume * 3.2)));
    }

    /**
     * Seek to the given time code. If the give time is larger than the duration
     * of the video, seek to the end.
     *
     * @param where Where to seek
     * @throws IOException       If the HTTP call receives an unexpected response code
     * @throws TimeCodeException This should never happen
     */
    public void seek(TimeCode where) throws IOException, TimeCodeException, JSONException {
        if (where.getTotalSeconds() > getDuration().getTotalSeconds()) {
            where = getDuration();
        }
        execute(WMCommand.SEEK,
                new KeyValuePair("val", String.valueOf(where.getTotalSeconds())));
    }

    @Override
    public void toggleRepeatMode() throws IOException {
        execute(WMCommand.TOGGLE_REPEAT, new KeyValuePair("val", "loop"));
    }


    public void nextSubtitles() throws IOException {
        execute(WMCommand.NEXT_SUBTITLE, new KeyValuePair("val", "subtitle-track"));

    }

    /**
     * Get the version string (MPC-HC vX.X.X.X).
     *
     * @return The version string
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public String getVersion() throws IOException {
        throw new UnsupportedOperationException();
    }
}
