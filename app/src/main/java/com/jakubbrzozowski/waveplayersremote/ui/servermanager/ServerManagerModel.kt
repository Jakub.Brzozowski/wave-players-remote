package com.jakubbrzozowski.waveplayersremote.ui.servermanager


import android.content.SharedPreferences
import android.content.res.Resources
import com.jakubbrzozowski.player_apis.vlc.VlcHttpClient
import com.jakubbrzozowski.waveplayersremote.R
import com.jakubbrzozowski.waveplayersremote.data.Server
import com.jakubbrzozowski.waveplayersremote.data.ServerUtils
import com.jakubbrzozowski.waveplayersremote.ui.CommonModel
import com.jakubbrzozowski.waveplayersremote.ui.servermanager.ServerManagerPresenter.Companion.MPC_SCAN
import com.jakubbrzozowski.waveplayersremote.ui.servermanager.ServerManagerPresenter.Companion.VLC_SCAN
import io.realm.Realm
import io.realm.RealmResults
import io.realm.internal.util.Pair
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import timber.log.Timber
import java.io.IOException
import java.net.InetAddress
import java.net.UnknownHostException
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ServerManagerModel @Inject
internal constructor(private val mRes: Resources, private val mCommonModel: CommonModel, private val mShPrefs: SharedPreferences) : ServerManagerContract.Model {
    private val mOkHttpClient = OkHttpClient.Builder()
            .readTimeout(READ_TIMEOUT.toLong(), TimeUnit.MILLISECONDS)
            .connectTimeout(CONNECT_TIMEOUT.toLong(), TimeUnit.MILLISECONDS).build()
    private val mQuickTimeoutClient: OkHttpClient
    private val mVlcClient: OkHttpClient
    private val mQuickTimeoutVlcClient: OkHttpClient
    private var mServers: RealmResults<Server>
    override val defaultToolbarTitle: String
    private var mDeletedServerBackup: Server? = null
    private var mRealm: Realm = Realm.getDefaultInstance()
        get() {
            return if (field.isClosed) {
                Realm.getDefaultInstance()
            } else {
                field
            }
        }

    override val deleteServerSnackbarText: String
        get() = mRes.getString(R.string.server_remove_snackbar_message_beginning) + " " +
                mDeletedServerBackup!!.address + ":" + mDeletedServerBackup!!.port + " " +
                mRes.getString(R.string.server_remove_snackbar_message_ending)

    override val servers: RealmResults<Server>
        get() {
            if (!mServers.isValid || mServers.isEmpty()) {
                mServers = mRealm.where(Server::class.java).findAll()
            }
            return mServers
        }

    init {
        val password = mRes.getString(R.string.pref_server_pass_default)
        mVlcClient = VlcHttpClient.getVlcOkHttpClient(password).build()
        mQuickTimeoutClient = OkHttpClient.Builder()
                .connectTimeout(SHORT_TIMEOUT.toLong(), TimeUnit.MILLISECONDS)
                .readTimeout(SHORT_TIMEOUT.toLong(), TimeUnit.MILLISECONDS).build()
        mQuickTimeoutVlcClient = VlcHttpClient.getVlcOkHttpClient(password)
                .connectTimeout(SHORT_TIMEOUT.toLong(), TimeUnit.MILLISECONDS)
                .readTimeout(SHORT_TIMEOUT.toLong(), TimeUnit.MILLISECONDS).build()
        defaultToolbarTitle = mRes.getString(R.string.server_manager_title)
        openDatabaseConnection()
        mServers = mRealm.where(Server::class.java).findAll()
        mRealm = Realm.getDefaultInstance()
    }

    override fun openDatabaseConnection() {
        if (!mRealm.isClosed) {
            return
        }
        mRealm = Realm.getDefaultInstance()
    }

    override fun closeDatabaseConnection() {
        mRealm.close()
    }

    override fun editServer(currentAddress: String, currentPort: String, newServer: Server) {
        // Delete existing server and create new one
        mRealm.beginTransaction()
        val srvToDelete = mRealm.where(Server::class.java)
                .equalTo(Server.ADDRESS, currentAddress)
                .equalTo(Server.PORT, currentPort)
                .findFirst()
        srvToDelete?.deleteFromRealm()
        mRealm.insert(newServer)
        mRealm.commitTransaction()
    }

    override fun remove(server: Server) {
        mDeletedServerBackup = Server(server.address, server.port,
                server.name, server.password)
        mRealm.beginTransaction()
        server.deleteFromRealm()
        mRealm.commitTransaction()
    }

    override fun setPreferredServer(newAddress: String, newPort: String, newPassword: String) {
        mShPrefs.edit().putString(mRes.getString(R.string.pref_server_address_key), newAddress)
                .putString(mRes.getString(R.string.pref_server_port_key), newPort)
                .putString(mRes.getString(R.string.pref_server_pass_key), newPassword)
                .apply()
    }

    override fun undoDeleteServer() {
        mRealm.beginTransaction()
        mRealm.insert(mDeletedServerBackup!!)
        mRealm.commitTransaction()
    }

    override fun saveNewServer(newServer: Server) {
        mRealm.beginTransaction()
        mRealm.copyToRealm(newServer)
        mRealm.commitTransaction()
    }

    override fun checkAndUpdatePreferences(currentAddress: String, currentPort: String, currentPassword: String, newAddress: String, newPort: String, newPassword: String) {
        val addressKey = mRes.getString(R.string.pref_server_address_key)
        val addmRessDefault = mRes.getString(R.string.pref_server_address_default)
        val portKey = mRes.getString(R.string.pref_server_port_key)
        val portDefault = mRes.getString(R.string.pref_server_port_default)
        val passwordKey = mRes.getString(R.string.pref_server_pass_key)
        val passwordDefault = mRes.getString(R.string.pref_server_pass_default)
        if (currentAddress == mShPrefs.getString(addressKey, addmRessDefault)
                && currentPort == mShPrefs.getString(portKey, portDefault) &&
                currentPassword == mShPrefs.getString(passwordKey, passwordDefault)) {
            mShPrefs.edit().putString(addressKey, newAddress).putString(portKey, newPort)
                    .putString(passwordKey, newPassword).apply()
        }
    }


    override fun buildCheckServerUrlString(server: Server, address: String?, port: String?, scanType: Int): Pair<Server, String> {
        val adrStr: String
        if (scanType == MPC_SCAN) {
            adrStr = "http://$address:$port/info.html"
        } else if (scanType == VLC_SCAN) {
            adrStr = "http://$address:$port/requests/status.json"
        } else {
            throw IllegalArgumentException("Unknown scan type in checkServer")
        }
        return Pair(server, adrStr)
    }

    override fun checkServer(server: Server, url: String, scanType: Int): Pair<Server, Boolean> {
        val request = Request.Builder()
                .url(url)
                .build()
        val response: Response
        try {
            if (scanType == VLC_SCAN) {
                response = mQuickTimeoutVlcClient.newCall(request).execute()
            } else {
                response = mQuickTimeoutClient.newCall(request).execute()
            }
        } catch (e: IOException) {
            Timber.d("checkServer returning " + url + false)
            return Pair(server, false)
        }

        val isResponding = checkResponse(scanType, response, request)
        Timber.d("checkServer returning " + url + isResponding)
        return Pair(server, isResponding)
    }

    override fun setResponding(server: Server, isResponding: Boolean) {
        Timber.d("setResponding " + isResponding)
        mRealm.beginTransaction()
        if (server.isValid && server.isResponding != isResponding) {
            server.isResponding = isResponding
        }
        mRealm.commitTransaction()
    }

    override fun setServersNotResponding(servers: RealmResults<Server>) {
        mRealm.beginTransaction()
        for (next in servers) {
            next.isResponding = false
        }
        mRealm.commitTransaction()
    }

    override fun scanServer(ipBytes: ByteArray?, ipFourthByte: Int, port: String, scanType: Int): Int {
        var address: InetAddress? = null
        ipBytes?.set(3, ipFourthByte.toByte())
        try {
            address = InetAddress.getByAddress(ipBytes)
        } catch (e: UnknownHostException) {
            Timber.d(e)
        }

        var addressStr = ""
        if (scanType == MPC_SCAN) {
            addressStr = "http://$address:$port/info.html"
        } else if (scanType == VLC_SCAN) {
            addressStr = "http://$address:$port/requests/status.json"
        }
        val request = Request.Builder()
                .url(addressStr)
                .build()
        var response: Response? = null
        try {
            if (scanType == MPC_SCAN) {
                response = getMpcResponse(request)
            } else if (scanType == VLC_SCAN) {
                response = getVlcResponse(request)
            }
        } catch (e: IOException) {
            return 0
        }

        if (checkResponse(scanType, response, request)) {
            val server = getServerFromUrl(request.url())
            saveServerIfNotDuplicate(server)
        }
        return 0
    }

    @Throws(IOException::class)
    private fun getMpcResponse(request: Request): Response {
        return mOkHttpClient.newCall(request).execute()
    }

    @Throws(IOException::class)
    private fun getVlcResponse(request: Request): Response {
        return mVlcClient.newCall(request).execute()
    }

    private fun getServerFromUrl(url: HttpUrl): Server {
        val password = mRes.getString(R.string.pref_server_pass_default)
        return Server(url.host(), url.port().toString(), url.host(), password)
    }

    private fun saveServerIfNotDuplicate(server: Server) {
        val realm = Realm.getDefaultInstance()
        try {
            // Check if server already exists
            if (!ServerUtils.isServerDuplicate(server)) {
                realm.beginTransaction()
                server.isResponding = true
                realm.copyToRealm(server)
                realm.commitTransaction()
            }
        } finally {
            realm.close()
        }
    }

    private fun checkResponse(scanType: Int, response: Response?, request: Request): Boolean {
        // Check if response is successful
        if (response != null && response.isSuccessful) {
            try {
                val bodyString = response.body()!!.string()
                // Check if response body is correct
                if (scanType == MPC_SCAN && bodyString.contains("MPC") || (scanType == VLC_SCAN && bodyString.contains("apiversion")
                        && bodyString.contains("fullscreen"))) {
                    Timber.d("Response successful" + request.url())
                    return true
                } else
                    return false
            } catch (e: IOException) {
                Timber.d(e)
            }

        }
        return false
    }

    companion object {

        private val READ_TIMEOUT = 3000
        private val CONNECT_TIMEOUT = 3000
        private val SHORT_TIMEOUT = 500
    }
}
