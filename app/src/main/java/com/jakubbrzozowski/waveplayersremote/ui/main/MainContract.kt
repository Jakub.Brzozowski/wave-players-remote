package com.jakubbrzozowski.waveplayersremote.ui.main

interface MainContract {

    interface Model {

        val isFirstRun: Boolean
        fun stopWaveService()

        fun startWaveService()
    }

    interface View {

        val isFileBrowserDrawerOpen: Boolean
        fun lockRightDrawer()

        fun unlockRightDrawer()

        fun setKeepScreenOn()

        fun setDontKeepScreenOn()

        fun startFirstRunTutorial()

        fun setToolbarTitle(title: String)

        fun loadFragment(tFragment: Class<*>, layoutId: Int)

        fun invalidateOptionsMenu()

        fun recreate()

        fun goToRemote()
    }

    interface Presenter {

        val isPauseOnRingingPref: Boolean
        fun attachView(view: View)

        fun detachView()

        fun updateProgress()

        fun stopWaveService()

        fun startWaveService()

        fun initializeService()

        fun initFileBrowser()

        fun setPauseOnRinging(pauseOnRinging: Boolean)

        fun checkTheme(themeResId: Int)

        fun setKeepScreenOn(keepScreenOn: Boolean)

        fun firstRunCheck()

        fun initKeepScreenOn()
    }
}
