package com.jakubbrzozowski.waveplayersremote.ui.main


import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Resources

import com.jakubbrzozowski.waveplayersremote.R
import com.jakubbrzozowski.waveplayersremote.WaveControlService
import com.jakubbrzozowski.waveplayersremote.WavePlayersRemoteApplication

import javax.inject.Inject

class MainModel @Inject
internal constructor(private val mApplication: WavePlayersRemoteApplication, internal var mRes: Resources, internal var mShPrefs: SharedPreferences) : MainContract.Model {
    private val mMpcServiceIntent: Intent

    override val isFirstRun: Boolean
        get() {
            val firstRunKey = mRes.getString(R.string.pref_first_run_key)
            return mShPrefs.getBoolean(firstRunKey, mRes.getBoolean(R.bool.pref_first_run_default))
        }

    init {
        mMpcServiceIntent = Intent(mApplication, WaveControlService::class.java)
    }

    override fun stopWaveService() {
        mApplication.stopService(mMpcServiceIntent)
    }

    override fun startWaveService() {
        mApplication.startService(mMpcServiceIntent)
    }
}
