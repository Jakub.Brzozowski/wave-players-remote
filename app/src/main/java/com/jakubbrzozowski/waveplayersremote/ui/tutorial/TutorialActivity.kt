package com.jakubbrzozowski.waveplayersremote.ui.tutorial


import android.app.Fragment
import android.app.FragmentManager
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.legacy.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.widget.*
import butterknife.BindView
import butterknife.ButterKnife
import com.jakubbrzozowski.waveplayersremote.R
import com.jakubbrzozowski.waveplayersremote.WavePlayersRemoteApplication
import com.jakubbrzozowski.waveplayersremote.ui.main.MainActivity
import com.jakubbrzozowski.waveplayersremote.utils.ThemeUtils
import javax.inject.Inject

class TutorialActivity : AppCompatActivity(), TutorialFragmentInteractionListener, ViewPager.OnPageChangeListener {

    @Inject
    lateinit internal var mShPrefs: SharedPreferences

    @BindView(R.id.pager_left_arrow_imageView)
    internal lateinit var mLeftArrow: ImageView

    @BindView(R.id.pager_right_arrow_imageView)
    internal lateinit var mRightArrow: ImageView

    @BindView(R.id.tutorial_toolbar)
    internal lateinit var mToolbar: Toolbar

    @BindView(R.id.tutorial_tab_layout)
    internal lateinit var mTabLayout: TabLayout

    /**
     * The [ViewPager] that will host the section contents.
     */
    @BindView(R.id.tutorial_viewPager)
    internal lateinit var mViewPager: ViewPager

    /**
     * The [android.support.v4.view.PagerAdapter] that will provide
     * fragments for each of the sections. We use a
     * [FragmentPagerAdapter] derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * [android.support.v13.app.FragmentStatePagerAdapter].
     */
    private var mTutorialPagerAdapter: TutorialPagerAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ThemeUtils.onActivityCreateSetTheme(this)

        (application as WavePlayersRemoteApplication).component.inject(this)
        setContentView(R.layout.activity_tutorial)
        ButterKnife.bind(this)

        mLeftArrow.setImageResource(R.drawable.ic_keyboard_arrow_left_greyed_out_48px)
        mLeftArrow.setOnClickListener { view -> mViewPager.currentItem = mViewPager.currentItem - 1 }
        mRightArrow.setOnClickListener { view -> mViewPager.currentItem = mViewPager.currentItem + 1 }
        mToolbar.title = getString(R.string.app_name) + " " +
                getString(R.string.tutorial_toolbar_title_end)
        setSupportActionBar(mToolbar)

        // Set up the ViewPager with the sections adapter.
        mViewPager.addOnPageChangeListener(this)
        mTabLayout.setupWithViewPager(mViewPager)
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mTutorialPagerAdapter = TutorialPagerAdapter(
                application as WavePlayersRemoteApplication,
                fragmentManager
        )
        mViewPager.adapter = mTutorialPagerAdapter
    }

    override fun onDestroy() {
        super.onDestroy()
        mViewPager.removeOnPageChangeListener(this)
    }

    override fun onBackPressed() {
        val firstRunKey = getString(R.string.pref_first_run_key)
        if (mShPrefs.getBoolean(firstRunKey, resources.getBoolean(R.bool.pref_first_run_default))) {
            mShPrefs.edit().putBoolean(firstRunKey, false).apply()
        }
        super.onBackPressed()
    }

    override fun onPlayerPrefChanged(newPrefPlayer: String) {
        if (newPrefPlayer == getString(R.string.pref_player_mpc_hc)) {
            mShPrefs.edit().putString(getString(R.string.pref_player_key), getString(R.string.pref_player_mpc_hc)).apply()
            recreate()
        } else if (newPrefPlayer == getString(R.string.pref_player_vlc)) {
            mShPrefs.edit().putString(getString(R.string.pref_player_key), getString(R.string.pref_player_vlc)).apply()
            recreate()
        }
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

    override fun onPageSelected(position: Int) {
        if (mViewPager.currentItem <= 0) {
            mLeftArrow.setImageResource(R.drawable.ic_keyboard_arrow_left_greyed_out_48px)
            mRightArrow.setImageResource(R.drawable.ic_keyboard_arrow_right_white_48px)
        } else if (mViewPager.currentItem >= mTutorialPagerAdapter!!.count - 1) {
            mLeftArrow.setImageResource(R.drawable.ic_keyboard_arrow_left_white_48px)
            mRightArrow.setImageResource(R.drawable.ic_keyboard_arrow_right_greyed_out_48px)
        } else {
            mLeftArrow.setImageResource(R.drawable.ic_keyboard_arrow_left_white_48px)
            mRightArrow.setImageResource(R.drawable.ic_keyboard_arrow_right_white_48px)
        }
    }

    override fun onPageScrollStateChanged(state: Int) {}

    /**
     * A placeholder fragment containing a simple view.
     */
    class TutorialFragment : Fragment() {
        @BindView(R.id.tutorial_RadioGroup)
        internal lateinit var mRadioGroup: RadioGroup

        @BindView(R.id.tutorial_screen_imageView)
        internal lateinit var mImageView: ImageView

        @BindView(R.id.tutorial_description_textView)
        internal lateinit var mDescriptionTextView: TextView

        @BindView(R.id.tutorial_title_textView)
        internal lateinit var mTitleTextView: TextView

        @BindView(R.id.tutorial_button)
        internal lateinit var mButton: Button

        @Inject
        lateinit internal var mShPrefs: SharedPreferences

        private fun initButtonFindServer() {
            mButton.visibility = View.VISIBLE
            mButton.setText(R.string.tutorial_button_find_server)
            mButton.setOnClickListener { view ->
                val intent = Intent(activity, MainActivity::class.java)
                intent.action = MainActivity.ACTION_SERVER_MANAGER
                startActivity(intent)
                val firstRunKey = getString(R.string.pref_first_run_key)
                mShPrefs.edit().putBoolean(firstRunKey, false).apply()
            }
        }

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                                  savedInstanceState: Bundle?): View? {
            (activity.application as WavePlayersRemoteApplication).component.inject(this)
            val rootView = inflater.inflate(R.layout.fragment_tutorial, container, false)
            ButterKnife.bind(this, rootView)

            val isPrefPlayerMpc = mShPrefs.getString(getString(R.string.pref_player_key),
                    getString(R.string.pref_player_default)) == getString(R.string.pref_player_mpc_hc)
            val isPrefPlayerVlc = mShPrefs.getString(getString(R.string.pref_player_key),
                    getString(R.string.pref_player_default)) == getString(R.string.pref_player_vlc)

            val sectionNumber = arguments.getInt(ARG_SECTION_NUMBER)
            if (sectionNumber == 0) {
                // Check if it's first run
                val firstRunKey = getString(R.string.pref_first_run_key)
                if (mShPrefs.getBoolean(firstRunKey, resources.getBoolean(R.bool.pref_first_run_default))) {
                    mTitleTextView.visibility = View.VISIBLE
                    mTitleTextView.setText(R.string.tutorial_welcome_title)
                } else {
                    mTitleTextView.visibility = GONE
                }
                mButton.setText(R.string.tutorial_button_skip_tutorial)
                mButton.setOnClickListener { view ->
                    val firstRunKey12 = getString(R.string.pref_first_run_key)
                    mShPrefs.edit().putBoolean(firstRunKey12, false).apply()
                    this@TutorialFragment.activity.finish()
                }
                mDescriptionTextView.textAlignment = TEXT_ALIGNMENT_CENTER
                mDescriptionTextView.text = getString(R.string.tutorial_description_0)
                mImageView.visibility = GONE
                mRadioGroup.visibility = View.VISIBLE
                val radioMpc = mRadioGroup.findViewById<RadioButton>(R.id.radio_mpc)
                radioMpc.setOnClickListener { view ->
                    val firstRunKey1 = getString(R.string.pref_first_run_key)
                    if (mShPrefs.getBoolean(firstRunKey1,
                                    resources.getBoolean(R.bool.pref_first_run_default))) {
                        activity.finish()
                    }
                    (activity as TutorialFragmentInteractionListener)
                            .onPlayerPrefChanged(getString(R.string.pref_player_mpc_hc))
                }
                val radioVlc = mRadioGroup.findViewById<RadioButton>(R.id.radio_vlc)
                radioVlc.setOnClickListener { view ->
                    val firstRunKey13 = getString(R.string.pref_first_run_key)
                    if (mShPrefs.getBoolean(firstRunKey13,
                                    resources.getBoolean(R.bool.pref_first_run_default))) {
                        activity.finish()
                    }
                    (activity as TutorialFragmentInteractionListener)
                            .onPlayerPrefChanged(getString(R.string.pref_player_vlc))
                }
                if (isPrefPlayerMpc) {
                    radioMpc.isChecked = true
                    radioVlc.isChecked = false
                } else if (isPrefPlayerVlc) {
                    radioMpc.isChecked = false
                    radioVlc.isChecked = true
                }
            } else if (isPrefPlayerMpc) {
                mRadioGroup.visibility = GONE
                mDescriptionTextView.textAlignment = TEXT_ALIGNMENT_TEXT_START
                mButton.visibility = GONE
                mTitleTextView.visibility = View.VISIBLE
                when (sectionNumber) {
                    1 -> {
                        mImageView.setImageResource(R.drawable.mpc_tutorial_1)
                        mDescriptionTextView.text = getString(R.string.tutorial_desc_mpc1)
                        mTitleTextView.setText(R.string.tutorial_middle_screens_title)
                    }
                    2 -> {
                        mImageView.setImageResource(R.drawable.mpc_tutorial_2)
                        mDescriptionTextView.text = getString(R.string.tutorial_desc_mpc2)
                        mTitleTextView.setText(R.string.tutorial_middle_screens_title)
                    }
                    3 -> {
                        mImageView.setImageResource(R.drawable.tutorial_second_last_screen)
                        mDescriptionTextView.text = getString(R.string.tutorial_desc_mpc3)
                        mButton.visibility = View.VISIBLE
                        mButton.setText(R.string.tutorial_wave_exercise_button_text)
                        mButton.setOnClickListener { view -> startActivity(Intent(activity, WaveTutorialActivity::class.java)) }
                        mTitleTextView.setText(R.string.tutorial_second_last_screen_title)
                    }
                    4 -> {
                        mImageView.visibility = GONE
                        mDescriptionTextView.text = getString(R.string.tutorial_desc_mpc4)
                        mTitleTextView.setText(R.string.tutorial_end_screen_title)
                        initButtonFindServer()
                    }
                }
            } else if (isPrefPlayerVlc) {
                mRadioGroup.visibility = GONE
                mButton.visibility = GONE
                mDescriptionTextView.textAlignment = TEXT_ALIGNMENT_TEXT_START
                mTitleTextView.visibility = View.VISIBLE
                when (sectionNumber) {
                    1 -> {
                        mImageView.setImageResource(R.drawable.vlc_tutorial_1)
                        mDescriptionTextView.text = getString(R.string.tutorial_desc_vlc1)
                        mTitleTextView.setText(R.string.tutorial_middle_screens_title)
                    }
                    2 -> {
                        mImageView.setImageResource(R.drawable.vlc_tutorial_2)
                        mDescriptionTextView.text = getString(R.string.tutorial_desc_vlc2)
                        mTitleTextView.setText(R.string.tutorial_middle_screens_title)
                    }
                    3 -> {
                        mImageView.setImageResource(R.drawable.vlc_tutorial_3)
                        mDescriptionTextView.text = getString(R.string.tutorial_desc_vlc3)
                        mTitleTextView.setText(R.string.tutorial_middle_screens_title)
                    }
                    4 -> {
                        mImageView.setImageResource(R.drawable.tutorial_second_last_screen)
                        mDescriptionTextView.text = getString(R.string.tutorial_desc_vlc4)
                        mTitleTextView.setText(R.string.tutorial_second_last_screen_title)
                        mButton.visibility = View.VISIBLE
                        mButton.setText(R.string.tutorial_wave_exercise_button_text)
                        mButton.setOnClickListener { view -> startActivity(Intent(activity, WaveTutorialActivity::class.java)) }
                    }
                    5 -> {
                        mImageView.visibility = GONE
                        mDescriptionTextView.text = getString(R.string.tutorial_desc_vlc5)
                        mTitleTextView.setText(R.string.tutorial_end_screen_title)
                        initButtonFindServer()
                    }
                }
            }

            return rootView
        }

        companion object {

            /**
             * The fragment argument representing the section number for this
             * fragment.
             */
            private val ARG_SECTION_NUMBER = "section_number"

            /**
             * Returns a new instance of this fragment for the given section
             * number.
             */
            fun newInstance(sectionNumber: Int): TutorialFragment {
                val fragment = TutorialFragment()
                val args = Bundle()
                args.putInt(ARG_SECTION_NUMBER, sectionNumber)
                fragment.arguments = args
                return fragment
            }
        }
    }

    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    class TutorialPagerAdapter internal constructor(
            private val app: WavePlayersRemoteApplication,
            fm: FragmentManager
    ) : FragmentPagerAdapter(fm) {

        @Inject
        lateinit internal var mShPrefs: SharedPreferences

        init {
            (app as WavePlayersRemoteApplication).component.inject(this)
        }

        override fun getItem(position: Int): Fragment {
            // getItem is called to instantiate the fragment for the given page.
            // Return a TutorialFragment (defined as a static inner class below).
            return TutorialFragment.newInstance(position)
        }

        override fun getCount(): Int {
            // Show 3 total pages.
            val isPrefPlayerMpc = mShPrefs.getString(app.getString(R.string.pref_player_key),
                    app.getString(R.string.pref_player_default)) == app.getString(R.string.pref_player_mpc_hc)
            val isPrefPlayerVlc = mShPrefs.getString(app.getString(R.string.pref_player_key),
                    app.getString(R.string.pref_player_default)) == app.getString(R.string.pref_player_vlc)
            return if (isPrefPlayerMpc) {
                5
            } else if (isPrefPlayerVlc) {
                6
            } else {
                6
            }
        }
    }
}
