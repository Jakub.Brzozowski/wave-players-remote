package com.jakubbrzozowski.waveplayersremote.dagger

import com.jakubbrzozowski.waveplayersremote.PhoneStateReceiver
import com.jakubbrzozowski.waveplayersremote.StopServiceReceiver
import com.jakubbrzozowski.waveplayersremote.WaveControlService
import com.jakubbrzozowski.waveplayersremote.WavePlayersRemoteApplication
import com.jakubbrzozowski.waveplayersremote.ui.CommonModel
import com.jakubbrzozowski.waveplayersremote.ui.filebrowser.FileBrowserFragment
import com.jakubbrzozowski.waveplayersremote.ui.main.MainActivity
import com.jakubbrzozowski.waveplayersremote.ui.main.MainModel
import com.jakubbrzozowski.waveplayersremote.ui.remote.RemoteFragment
import com.jakubbrzozowski.waveplayersremote.ui.servermanager.ServerManagerFragment
import com.jakubbrzozowski.waveplayersremote.ui.servermanager.ServerManagerRecyclerViewAdapter
import com.jakubbrzozowski.waveplayersremote.ui.settings.SettingsActivity
import com.jakubbrzozowski.waveplayersremote.ui.settings.SettingsFragment
import com.jakubbrzozowski.waveplayersremote.ui.tutorial.TutorialActivity
import com.jakubbrzozowski.waveplayersremote.ui.tutorial.WaveTutorialActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(StorageModule::class))
interface StorageComponent {
    fun inject(mainActivity: MainActivity)
    fun inject(remoteFragment: RemoteFragment)
    fun inject(serverManagerFragment: ServerManagerFragment)
    fun inject(fileBrowserFragment: FileBrowserFragment)
    fun inject(waveControlService: WaveControlService)
    fun inject(serverManagerRecyclerViewAdapter: ServerManagerRecyclerViewAdapter)
    fun inject(phoneStateReceiver: PhoneStateReceiver)
    fun inject(settingsActivity: SettingsActivity)
    fun inject(settingsFragment: SettingsFragment)
    fun inject(stopServiceReceiver: StopServiceReceiver)
    fun inject(tutorialActivity: TutorialActivity)
    fun inject(tutorialFragment: TutorialActivity.TutorialFragment)
    fun inject(tutorialPagerAdapter: TutorialActivity.TutorialPagerAdapter)
    fun inject(application: WavePlayersRemoteApplication)
    fun inject(waveTutorialActivity: WaveTutorialActivity)
    fun inject(commonModel: CommonModel)
    fun inject(mainModel: MainModel)
}
