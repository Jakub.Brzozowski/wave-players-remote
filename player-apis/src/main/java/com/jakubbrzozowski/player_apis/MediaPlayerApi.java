package com.jakubbrzozowski.player_apis;


import org.json.JSONException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Client for the web interface exposed by MPC-HC.
 */
public abstract class MediaPlayerApi {
    public static final int READ_TIMEOUT = 3000;
    public static final int CONNECT_TIMEOUT = 3000;
    public static final String SCHEME = "http";
    public static final String VARIABLES_FILE_NAME = "file";
    public static final String VARIABLES_FILE_PATH = "filepath";
    public static final String VARIABLES_STATE = "state";
    public static final String VARIABLES_STATE_STRING = "statestring";
    public static final String VARIABLES_POSITION = "position";
    public static final String VARIABLES_POSITION_STRING = "positionstring";
    public static final String VARIABLES_DURATION = "duration";
    public static final String VARIABLES_DURATION_STRING = "durationstring";
    public static final String VARIABLES_VOLUME_LEVEL = "volumelevel";
    public static final String VARIABLES_MUTED = "muted";
    public static final String VARIABLES_PLAYBACK_RATE = "playbackrate";
    public static final String VARIABLE_IDS[] = new String[]{
            VARIABLES_STATE, VARIABLES_STATE_STRING, VARIABLES_POSITION,
            VARIABLES_POSITION_STRING, VARIABLES_DURATION, VARIABLES_DURATION_STRING,
            VARIABLES_VOLUME_LEVEL, VARIABLES_MUTED, VARIABLES_PLAYBACK_RATE, VARIABLES_FILE_PATH
    };
    protected String baseURI;
    protected String commandEndPoint;
    protected String infoEndPoint;
    protected String variableEndPoint;
    protected String password;
    protected String hostAddress;
    protected int hostPort;
    protected String pathSegments;
    protected OkHttpClient okHttpClient;
    protected String mBrowserRootUriPath = "";

    /**
     * Construct a new instance of the media player.
     *
     * @param hostname The hostname or IP address of the machine that MPC-HC is running on
     * @param port     The port that MPC-HC is listening on
     */
    public MediaPlayerApi(String hostname, int port, String pass) {
        hostPort = port;
        hostAddress = hostname;
        password = pass;
        baseURI = "http://" + hostname + ":" + Integer.toString(port);
    }

    /**
     * Get a list of {@link FileInfo} objects for the system root.
     * This is a listing of all attached drives, e.g. A:\, C:\, D:\.
     *
     * @return The file listing for the system root
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public List<FileInfo> browse() throws IOException, JSONException {
        return browse(mBrowserRootUriPath);
    }

    /**
     * Get a list of {@link FileInfo} objects for the given path.
     *
     * @param file The FileInfo object from the FileTable
     * @return A list of {@link FileInfo} objects for the given path
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public abstract List<FileInfo> browse(FileInfo file) throws IOException, JSONException;

    /**
     * Get a list of {@link FileInfo} objects for the given path.
     *
     * @param href The relative path to the file, as returned by
     *             {@link FileInfo#getHref()}
     * @return A list of {@link FileInfo} objects for the given path
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    protected abstract List<FileInfo> browse(String href) throws IOException, JSONException;

    /**
     * Open the file pointed to by the href with MPC-HC.
     *
     * @param file The {@link FileInfo} to open
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public abstract void openFile(FileInfo file) throws IOException;

    /**
     * Send the command to MPC-HC.
     *
     * @param command The command to execute
     * @param args    Any additional key value pairs to add to the form
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public abstract void execute(final WMCommand command,
                                 KeyValuePair... args) throws IOException;

    /**
     * Get the info string from MPC-HC.
     *
     * @return The info string
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public abstract String getInfo() throws IOException;

    /**
     * Get a map of all web interface exposed MPC-HC variables.
     *
     * @return The variables
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public Map<String, String> getVariables() throws IOException, JSONException {
        Response response = get(variableEndPoint);

        final String html = response.body().string();
        Document document = Jsoup.parse(html);
        Map<String, String> variables = new HashMap<String, String>();
        for (String variableID : VARIABLE_IDS) {
            String value = document.getElementById(variableID).text();
            variables.put(variableID, value);
        }

        return variables;
    }

    /**
     * Perform an HTTP GET request for a URL.
     *
     * @param url The URL to GET
     * @return The response
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    protected Response get(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = okHttpClient.newCall(request).execute();
        if (!response.isSuccessful()) {
            throw new IOException("Unexpected code: " + response);
        }

        return response;
    }

    /**
     * Toggle the mute state of the player.
     *
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public void toggleMute() throws IOException {
        execute(WMCommand.VOLUME_MUTE);
    }


    //
    // Convenience methods
    //

    /**
     * Returns true if the player is muted.
     *
     * @return True if the player is muted
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public boolean isMuted() throws IOException, JSONException {
        return getVariables().get(VARIABLES_MUTED).equals("1");
    }

    /**
     * Set mute on the player.
     *
     * @param mute If {@code true}, mute the player. If {@code false}, unmute.
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public void setMute(boolean mute) throws IOException, JSONException {
        boolean isMuted = isMuted();
        if ((isMuted && !mute) || (!isMuted && mute)) {
            toggleMute();
        }
    }

    /**
     * Returns true if the player is playing.
     *
     * @return True if the player is playing
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public abstract boolean isPlaying() throws IOException, JSONException;

    /**
     * Returns true if the player is paused.
     *
     * @return True if the player is paused
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public abstract boolean isPaused() throws IOException, JSONException;

    /**
     * Returns true if the player is stopped.
     *
     * @return True if the player is stopped
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public abstract boolean isStopped() throws IOException, JSONException;

    /**
     * Returns true if no file is open.
     *
     * @return True if no file is open
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public abstract boolean isClosed() throws IOException, JSONException;

    /**
     * Play the current video.
     *
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public void play() throws IOException {
        execute(WMCommand.PLAY);
    }

    /**
     * Pause the current video.
     *
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public void pause() throws IOException {
        execute(WMCommand.PAUSE);
    }

    /**
     * Toggle between play and pause.
     *
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public void togglePlayPause() throws IOException {
        execute(WMCommand.PLAY_PAUSE);
    }

    /**
     * Stop the current video.
     *
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public void stop() throws IOException {
        execute(WMCommand.STOP);
    }

    /**
     * Close the currently playing file. This does <b>not</b> end the process.
     *
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public void close() throws IOException {
        execute(WMCommand.CLOSE);
    }

    /**
     * Get the current volume level. Note that this can be non-zero, but
     * {@link #isMuted()} may still return true. Muting is independent of
     * the current volume level.
     *
     * @return The current volume level
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public int getVolume() throws IOException, JSONException {
        return Integer.valueOf(getVariables().get(VARIABLES_VOLUME_LEVEL));
    }

    /**
     * Set the volume (0 to 100). Values outside of this range are clamped.
     *
     * @param volume The volume to set
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public abstract void setVolume(int volume) throws IOException;

    /**
     * Seek to the given time code. If the give time is larger than the duration
     * of the video, seek to the end.
     *
     * @param where Where to seek
     * @throws IOException       If the HTTP call receives an unexpected response code
     * @throws TimeCodeException This should never happen
     */
    public abstract void seek(TimeCode where) throws IOException, TimeCodeException, JSONException;

    /**
     * Seek to the start of the video.
     *
     * @throws IOException       If the HTTP call receives an unexpected response code
     * @throws TimeCodeException This should never happen
     */
    public void seekToStart() throws IOException, TimeCodeException, JSONException {
        seek(TimeCode.START);
    }

    /**
     * Seek from the current position. A positive value will seek forward, and a
     * negative value will seek backward.
     *
     * @param seconds How far and in what direction to seek
     * @throws IOException       If the HTTP call receives an unexpected response code
     * @throws TimeCodeException This should never happen
     */
    public void jump(int seconds) throws IOException, TimeCodeException, JSONException {
        TimeCode destination;
        if (seconds >= 0) {
            destination = TimeCode.plus(getPosition(), new TimeCode(seconds));
        } else {
            try {
                destination = TimeCode.minus(getPosition(), new TimeCode(Math.abs(seconds)));
            } catch (TimeCodeException e) {
                // Invalid attempt to seek to before the video started (negative time code)
                destination = TimeCode.START;
                seek(destination);
                throw e;
            }
        }
        seek(destination);
    }

    /**
     * Toggle fullscreen.
     *
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public void toggleFullscreen() throws IOException {
        execute(WMCommand.FULLSCREEN);
    }

    public abstract void toggleRepeatMode() throws IOException;

    public void nextAudioTrack() throws IOException {
        execute(WMCommand.NEXT_AUDIO, new KeyValuePair("val", "audio-track"));
    }

    public abstract void nextSubtitles() throws IOException;

    public void nextFile() throws IOException {
        execute(WMCommand.NEXT_FILE);
    }

    public void previousFile() throws IOException {
        execute(WMCommand.PREVIOUS_FILE);
    }

    /**
     * Get the current position in the video.
     *
     * @return The current position
     * @throws IOException       If the HTTP call receives an unexpected response code
     * @throws TimeCodeException This should never happen
     */
    public TimeCode getPosition() throws IOException, TimeCodeException, JSONException {
        return new TimeCode(getVariables().get(VARIABLES_POSITION_STRING));
    }

    /**
     * Get the duration of the current video.
     *
     * @return The duration of the current video
     * @throws IOException       If the HTTP call receives an unexpected response code
     * @throws TimeCodeException This should never happen
     */
    public TimeCode getDuration() throws IOException, TimeCodeException, JSONException {
        return new TimeCode(getVariables().get(VARIABLES_DURATION_STRING));
    }

    /**
     * Get the version string (MPC-HC vX.X.X.X).
     *
     * @return The version string
     * @throws IOException If the HTTP call receives an unexpected response code
     */
    public abstract String getVersion() throws IOException;

    /**
     * A pair of {@link String}s.
     */
    public static class KeyValuePair {
        private final String key;
        private final String value;

        /**
         * Create a new instance.
         *
         * @param key   The key
         * @param value The value
         */
        public KeyValuePair(String key, String value) {
            this.key = key;
            this.value = value;
        }

        /**
         * Returns the key.
         *
         * @return the key
         */
        public String getKey() {
            return key;
        }

        /**
         * Returns the value.
         *
         * @return the value
         */
        public String getValue() {
            return value;
        }
    }
}
