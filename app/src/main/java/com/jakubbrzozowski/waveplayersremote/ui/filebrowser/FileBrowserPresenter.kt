package com.jakubbrzozowski.waveplayersremote.ui.filebrowser


import com.jakubbrzozowski.player_apis.FileInfo
import com.jakubbrzozowski.waveplayersremote.eventbus.InitFileBrowserEvent
import com.jakubbrzozowski.waveplayersremote.eventbus.MainToolbarTitleChangeEvent
import com.jakubbrzozowski.waveplayersremote.ui.CommonModel
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.json.JSONException
import timber.log.Timber
import java.io.IOException
import java.io.UnsupportedEncodingException
import javax.inject.Inject
import javax.inject.Named

class FileBrowserPresenter @Inject
internal constructor(private val mCommonModel: CommonModel, fileBrowserModel: FileBrowserModel,
                     @param:Named("mainThread") private val mMainScheduler: Scheduler) : FileBrowserContract.Presenter {
    private var mView: FileBrowserContract.View? = null
    private val mBrowserModel: FileBrowserContract.Model

    init {
        mBrowserModel = fileBrowserModel
    }

    override fun attachView(view: FileBrowserContract.View) {
        EventBus.getDefault().unregister(this)
        EventBus.getDefault().register(this)
        mView = view
        mCommonModel.setFileBrowserPresenter(this)
    }

    override fun detachView() {
        EventBus.getDefault().unregister(this)
        mCommonModel.setFileBrowserPresenter(null)
        mView = null
    }

    @Subscribe
    fun onEvent(event: InitFileBrowserEvent) {
        initFileInfoList()
    }

    override fun initFileInfoList() {
        val fileInfoList = mCommonModel.fileInfoList
        if (fileInfoList == null || fileInfoList.isEmpty()) {
            var directoryCache = mCommonModel.getDirectoryCache()
            if (directoryCache != null) {
                openFileInfo(directoryCache)
            } else {
                openRoot()
            }
        } else {
            setFileInfoData(fileInfoList)
            setToolbarTitle()
        }
    }

    override fun openRoot() {
        try {
            subscribeBrowseObservable(null)
        } catch (ex: Exception) {
            when (ex) {
                is NullPointerException,
                is KotlinNullPointerException -> {
                    Timber.d(ex)
                }
                else -> throw ex
            }
        }
    }

    override fun openFileInfo(fileInfo: FileInfo?) {
        if (fileInfo?.isDirectory == true) {
            subscribeBrowseObservable(fileInfo)
            var toolbarTitle = fileInfo.href.replace('/', '\\')
            if (toolbarTitle.startsWith("\\browser.html?path=")) {
                toolbarTitle = toolbarTitle.replace("\\browser.html?path=", "")
                try {
                    toolbarTitle = java.net.URLDecoder.decode(toolbarTitle, "UTF-8")
                } catch (e: UnsupportedEncodingException) {
                    Timber.d(e)
                }

            }
            EventBus.getDefault().post(MainToolbarTitleChangeEvent(toolbarTitle, this.javaClass))
        } else {
            mBrowserModel.openFile(mCommonModel, fileInfo)
        }
    }

    private fun getBrowseObservable(info: FileInfo?): Observable<MutableList<FileInfo>> {
        return Observable.fromCallable cal@{
            var fileInfos: MutableList<FileInfo> = ArrayList()
            try {
                if (info == null) {
                    fileInfos = mCommonModel.mediaPlayerApi?.browse() ?: ArrayList()
                    return@cal fileInfos
                }
                fileInfos = mCommonModel.mediaPlayerApi?.browse(info) ?: ArrayList()
            } catch (e: JSONException) {
                Timber.d(e.message)
            } catch (e: IOException) {
                Timber.d(e.message)
            }

            fileInfos
        }
    }

    private fun subscribeBrowseObservable(fileInfo: FileInfo?) {
        // Run on a background thread
        getBrowseObservable(fileInfo).subscribeOn(Schedulers.io())
                // Be notified on the main thread
                .observeOn(mMainScheduler)
                .subscribeWith(object : DisposableObserver<MutableList<FileInfo>>() {
                    override fun onNext(fileInfos: MutableList<FileInfo>) {
                        Timber.d("Browse observable onNext called")
                        onBrowseResult(fileInfos)
                        fileInfo?.let { mCommonModel.setDirectoryCache(it) }
                    }

                    override fun onError(e: Throwable) {
                        Timber.d(e)
                    }

                    override fun onComplete() {
                        Timber.d("Browse observable onComplete called")
                    }
                })
    }

    private fun onBrowseResult(fileInfos: MutableList<FileInfo>) {
        mCommonModel.fileInfoList = fileInfos
        for (i in fileInfos.indices.reversed()) {
            val info = fileInfos[i]
            if (!info.isDirectory && !info.isMediaOrPlaylist) {
                mCommonModel.fileInfoList!!.removeAt(i)
            }
        }
        setFileInfoData(fileInfos)
        mCommonModel.fileInfoList = fileInfos
        setToolbarTitle()
    }

    private fun setFileInfoData(fileInfoList: MutableList<FileInfo>) {
        mCommonModel.fileInfoList = fileInfoList
        mView?.displayFileInfoList(fileInfoList)
    }

    private fun setToolbarTitle() {
        if (!mView!!.isFragmentAdded) {
            return
        }
        val fileInfoList = mCommonModel.fileInfoList
        if (fileInfoList != null && !fileInfoList.isEmpty()) {
            if (fileInfoList.size > 1) {
                val firstFile = fileInfoList[1]
                var href = ""
                try {
                    href = java.net.URLDecoder.decode(firstFile.href, "UTF-8")
                } catch (e: UnsupportedEncodingException) {
                    Timber.d(e)
                }

                if (href.length > 3) {
                    href = href.replace(firstFile.fileName, "")
                } else {
                    EventBus.getDefault().post(MainToolbarTitleChangeEvent(
                            mBrowserModel.browserTitle, this.javaClass))
                    return
                }

                var filePath = href.substring(
                        href.indexOf('=') + 1)
                filePath = filePath.replace('/', '\\').replace("\\\\", "\\")
                EventBus.getDefault().post(MainToolbarTitleChangeEvent(filePath, this.javaClass))
                Timber.d(firstFile.fileName + "\n" + firstFile.href)
            }
        } else {
            EventBus.getDefault().post(MainToolbarTitleChangeEvent(
                    mBrowserModel.browserTitle, this.javaClass))
        }
    }

}
