package com.jakubbrzozowski.waveplayersremote.ui.filebrowser

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.jakubbrzozowski.player_apis.FileInfo
import com.jakubbrzozowski.waveplayersremote.R
import com.jakubbrzozowski.waveplayersremote.ui.filebrowser.FileBrowserRecyclerViewAdapter.OnBrowserItemClickedListener

/**
 * [RecyclerView.Adapter] that can display a [FileInfo] and makes a call to the
 * specified [OnBrowserItemClickedListener].
 */
class FileBrowserRecyclerViewAdapter(private val mValues: List<FileInfo>?, private val mListener: OnBrowserItemClickedListener?) : RecyclerView.Adapter<FileBrowserRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.filebrowser_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.mItem = mValues!![position]
        holder.mNameTextView.text = mValues[position].fileName
        if (holder.mItem?.isDirectory == false) {
            if (holder.mItem?.isMediaOrPlaylist == true) {
                holder.mIconImageView.setImageResource(R.drawable.ic_movie_white_48px)
            } else {
                holder.mIconImageView.setImageResource(R.drawable.ic_file_white_48px)
            }
        } else {
            holder.mIconImageView.setImageResource(R.drawable.ic_folder_white_48px)
        }
        holder.mView.setOnClickListener { v ->
            mListener?.onBrowserItemClicked(holder.mItem)
        }
    }

    override fun getItemCount(): Int {
        return mValues?.size ?: 0
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnBrowserItemClickedListener {
        fun onBrowserItemClicked(info: FileInfo?)
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mNameTextView: TextView = mView.findViewById(R.id.browser_item_name_textView)
        var mIconImageView: ImageView = mView.findViewById(R.id.browser_item_icon_imageView)
        var mButton: Button = mView.findViewById(R.id.browse_item_button)
        var mItem: FileInfo? = null

        override fun toString(): String {
            return super.toString() + " '" + mNameTextView.text + "'"
        }
    }
}
