package com.jakubbrzozowski.waveplayersremote.ui.tutorial

import android.content.Context
import android.content.SharedPreferences
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.os.Handler
import android.os.Vibrator
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.View
import android.view.View.GONE
import android.widget.Button
import android.widget.ImageView
import android.widget.RadioGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.jakubbrzozowski.waveplayersremote.R
import com.jakubbrzozowski.waveplayersremote.WavePlayersRemoteApplication
import com.jakubbrzozowski.waveplayersremote.utils.ThemeUtils
import timber.log.Timber
import javax.inject.Inject

class WaveTutorialActivity : AppCompatActivity(), SensorEventListener, SharedPreferences.OnSharedPreferenceChangeListener {

    @BindView(R.id.tutorial_RadioGroup)
    internal lateinit var mRadioGroup: RadioGroup
    @BindView(R.id.tutorial_screen_imageView)
    internal lateinit var mImageView: ImageView
    @BindView(R.id.tutorial_description_textView)
    internal lateinit var mDescriptionTextView: TextView
    @BindView(R.id.tutorial_title_textView)
    internal lateinit var mTitleTextView: TextView
    @BindView(R.id.tutorial_button)
    internal lateinit var mButton: Button
    @BindView(R.id.tutorial_fragment_root_constraintLayout)
    internal lateinit var mConstraintLayout: ConstraintLayout
    @BindView(R.id.tutorial_toolbar)
    internal lateinit var mToolbar: Toolbar
    @Inject
    lateinit internal var mShPrefs: SharedPreferences
    private var mWaitTime: Int? = null
    private var mSensorManager: SensorManager? = null
    private var mProximity: Sensor? = null

    private fun initializeSharedPreferences() {
        val stringDelay = mShPrefs.getString(getString(R.string.pref_filter_delay_key),
                getString(R.string.pref_filter_delay_default))
        mWaitTime = Integer.parseInt(stringDelay)

        mShPrefs.registerOnSharedPreferenceChangeListener(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ThemeUtils.onActivityCreateSetTheme(this)
        setContentView(R.layout.activity_wave_tutorial)

        (applicationContext as WavePlayersRemoteApplication).component.inject(this)

        ButterKnife.bind(this)

        mToolbar.title = (getString(R.string.app_name) + " "
                + getString(R.string.wave_tutorial_activity_title_end))
        setSupportActionBar(mToolbar)
        mSensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        mProximity = mSensorManager!!.getDefaultSensor(Sensor.TYPE_PROXIMITY)
        mSensorManager!!.registerListener(this, mProximity, SensorManager.SENSOR_DELAY_FASTEST)
        initializeSharedPreferences()

        val set = ConstraintSet()
        set.clone(mConstraintLayout)
        val metrics = resources.displayMetrics
        val density = metrics.densityDpi
        // Resize to 300dp
        set.constrainHeight(R.id.tutorial_screen_imageView, 300)
        set.constrainWidth(R.id.tutorial_screen_imageView, 300)
        // Apply the changes
        set.applyTo(mConstraintLayout)

        mImageView.scaleType = ImageView.ScaleType.FIT_CENTER
        mImageView.setImageResource(R.drawable.ic_wave_hand_off)

        mTitleTextView.setText(R.string.wave_tutorial_title)
        mTitleTextView.visibility = View.VISIBLE
        mRadioGroup.visibility = GONE
        mDescriptionTextView.text = getString(R.string.wave_tutorial_description)
        mButton.visibility = GONE
    }

    public override fun onDestroy() {
        mSensorManager!!.unregisterListener(this, mProximity)
        mShPrefs.unregisterOnSharedPreferenceChangeListener(this)
        super.onDestroy()
    }

    override fun onSensorChanged(sensorEvent: SensorEvent) {
        val `val` = sensorEvent.values[0]
        if (`val` < mProximity!!.maximumRange) {
            val vibrateOnWave = mShPrefs.getBoolean(
                    getString(R.string.pref_vibrate_on_wave_key),
                    resources.getBoolean(R.bool.pref_vibrate_on_wave_default))
            val vibrationStrength = Integer.parseInt(mShPrefs.getString(
                    getString(R.string.pref_vibration_strength_key),
                    getString(R.string.pref_vibration_strength_default)))

            val vibTime = 25 * vibrationStrength
            val vibTimeLonger = 2 * vibTime
            val vibrator = getSystemService(Context.VIBRATOR_SERVICE) as? Vibrator
            mImageView.setImageResource(R.drawable.ic_wave_hand)
            if (vibrateOnWave && vibrator != null) {
                vibrator.vibrate(vibTime.toLong())
            }
            try {
                Thread.sleep(vibTimeLonger.toLong())
                if (vibrateOnWave && vibrator != null) {
                    vibrator.vibrate(vibTimeLonger.toLong())
                }
                Thread.sleep(vibTimeLonger.toLong())
                val r = { mImageView.setImageResource(R.drawable.ic_wave_hand_off) }
                val handler = Handler()
                handler.postDelayed(r, 500)
            } catch (e: InterruptedException) {
                Timber.d(e)
            }

        }
    }

    override fun onAccuracyChanged(sensor: Sensor, i: Int) {

    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        if (key == getString(R.string.pref_filter_delay_key)) {
            val filterDelay = sharedPreferences?.getString(
                    getString(R.string.pref_filter_delay_key),
                    getString(R.string.pref_filter_delay_default)) ?: "1000"
            mWaitTime = Integer.parseInt(filterDelay)
        }
    }
}
