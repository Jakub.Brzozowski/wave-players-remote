package com.jakubbrzozowski.player_apis.vlc;

import com.jakubbrzozowski.player_apis.WMCommand;

import java.util.EnumMap;
import java.util.Map;

public enum VlcCommand {
    SET_VOLUME("volume"),
    SEEK("seek"),
    PLAY_PAUSE("pl_pause"),
    PLAY("pl_forceresume"),
    PAUSE("pl_forcepause"),
    STOP("pl_stop"),
    NEXT_FILE("pl_next"),
    PREVIOUS_FILE("pl_previous"),
    NEXT_AUDIO("key"),
    NEXT_SUBTITLE("key"),
    OPEN_FILE("in_play"),
    TOGGLE_REPEAT("key"),
    FULLSCREEN("fullscreen"),
    EMPTY_PLAYLIST("pl_empty"),
    ;

    public static final Map<WMCommand, VlcCommand> wmCommandToVlcCommand;
    static {
        wmCommandToVlcCommand = new EnumMap<WMCommand, VlcCommand>(
                WMCommand.class);
        wmCommandToVlcCommand.put(WMCommand.SET_VOLUME,VlcCommand.SET_VOLUME);
        wmCommandToVlcCommand.put(WMCommand.SEEK,VlcCommand.SEEK);
        wmCommandToVlcCommand.put(WMCommand.PLAY_PAUSE, VlcCommand.PLAY_PAUSE);
        wmCommandToVlcCommand.put(WMCommand.PLAY,VlcCommand.PLAY);
        wmCommandToVlcCommand.put(WMCommand.PAUSE,VlcCommand.PAUSE);
        wmCommandToVlcCommand.put(WMCommand.STOP,VlcCommand.STOP);
        wmCommandToVlcCommand.put(WMCommand.NEXT_FILE,VlcCommand.NEXT_FILE);
        wmCommandToVlcCommand.put(WMCommand.PREVIOUS_FILE,VlcCommand.PREVIOUS_FILE);
        wmCommandToVlcCommand.put(WMCommand.NEXT_AUDIO,VlcCommand.NEXT_AUDIO);
        wmCommandToVlcCommand.put(WMCommand.NEXT_SUBTITLE,VlcCommand.NEXT_SUBTITLE);
        wmCommandToVlcCommand.put(WMCommand.OPEN_FILE,VlcCommand.OPEN_FILE);
        wmCommandToVlcCommand.put(WMCommand.TOGGLE_REPEAT,VlcCommand.TOGGLE_REPEAT);
        wmCommandToVlcCommand.put(WMCommand.FULLSCREEN,VlcCommand.FULLSCREEN);
        wmCommandToVlcCommand.put(WMCommand.EMPTY_PLAYLIST,VlcCommand.EMPTY_PLAYLIST);
    }

    private String mValue;

    VlcCommand(String value) {
        mValue = value;
    }

    public String getValue() {
        return mValue;
    }
}
