package com.jakubbrzozowski.waveplayersremote

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.telephony.TelephonyManager
import com.jakubbrzozowski.player_apis.MediaPlayerApi
import com.jakubbrzozowski.player_apis.WMCommand
import com.jakubbrzozowski.player_apis.mpc.hc.MediaPlayerClassicHomeCinema
import com.jakubbrzozowski.player_apis.vlc.VlcHttpClient
import com.jakubbrzozowski.waveplayersremote.utils.PlayerAsyncTask
import timber.log.Timber
import javax.inject.Inject

class PhoneStateReceiver : BroadcastReceiver() {

    @Inject
    lateinit internal var mShPrefs: SharedPreferences

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action != null && intent.action != TelephonyManager.ACTION_PHONE_STATE_CHANGED) {
            return
        }
        val appContext = context.applicationContext
        if (!(appContext is WavePlayersRemoteApplication)) {
            Timber.e("appContext is not instance of WavePlayersRemoteApplication")
            return
        }
        appContext.component.inject(this)

        val state = intent.getStringExtra(TelephonyManager.EXTRA_STATE)

        if (state == TelephonyManager.EXTRA_STATE_RINGING) {
            val pauseOnRinging = mShPrefs.getBoolean(
                    context.getString(R.string.pref_pause_on_ringing_key),
                    context.resources.getBoolean(R.bool.pref_pause_on_ringing_default))
            if (!pauseOnRinging) {
                return
            }

            val address = mShPrefs.getString(context.getString(R.string.pref_server_address_key),
                    context.getString(R.string.pref_server_address_default))
            val port = Integer.valueOf(mShPrefs.getString(
                    context.getString(R.string.pref_server_port_key),
                    context.getString(R.string.pref_server_port_default)))!!
            val password = mShPrefs.getString(context.getString(R.string.pref_server_pass_key),
                    context.getString(R.string.pref_server_pass_default))
            val player = mShPrefs.getString(context.getString(R.string.pref_player_key),
                    context.getString(R.string.pref_player_default))
            val mediaPlayerApi: MediaPlayerApi
            if (player == context.getString(R.string.pref_player_mpc_hc)) {
                mediaPlayerApi = MediaPlayerClassicHomeCinema(address, port, password)
            } else if (player == context.getString(R.string.pref_player_vlc)) {
                mediaPlayerApi = VlcHttpClient(address, port, password)
            } else
                mediaPlayerApi = MediaPlayerClassicHomeCinema(address, port, password)

            PlayerAsyncTask(mediaPlayerApi).execute(WMCommand.PAUSE)
        }
    }
}
