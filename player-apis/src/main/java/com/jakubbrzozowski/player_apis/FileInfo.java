package com.jakubbrzozowski.player_apis;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Contains information about a file that MPC-HC exposes.
 */
public class FileInfo {
    public static final String PLAYLISTS_STRING = "asx|xspf|m3u|m3u8|mpcpl|pls";
    public static final Pattern PLAYLISTS_REGEX = Pattern.compile("\\.(" + PLAYLISTS_STRING + ")$");
    public static final Pattern MEDIA_AND_PLAYLISTS_EXTENSIONS_REGEX = Pattern.compile("\\." +
            "(3g2|3gp|3gp2|3gpp|amv|asf|avi|bik|bup|divx|dv|dvr|dvr-ms|evo|f4v|flc|fli|flic|flv" +
            "|h264|hdmov|idx|ifo|ivf|m1v|m2p|m2t|m2ts|m2v|m4e|m4v|mkv|mod|mov|mp2v|mp4|mp4v|mpa|mpe" +
            "|mpeg|mpeg1|mpeg4|mpg|mpm|mpv|mpv2|mpv4|mts|mxf|nsv|ogm|ogv|pva|qt|rec|rm|rms|rmvb|roq" +
            "|rp|rpm|rt|rv|smi|smil|smk|swf|swt|tp|tps|trp|ts|vob|vp6|vro|webm|wm|wmv|wtv|xvid|aac" +
            "|ac3|aif|aifc|aiff|alac|amr|aob|ape|apl|au|caf|cda|dsm|dss|dts|fla|flac|it|koz|m1a|m2a" +
            "|m4a|m4b|m4p|m4r|mid|midi|mka|mp2|mp3|mpc|mpga|mtm|ofr|ofs|oga|ogg|oma|ra|ram|rmi|rmj" +
            "|rmm|rmx|s3m|snd|spx|tak|tta|wav|wax|wma|wv|xm|" + PLAYLISTS_STRING + ")$");
    private boolean isDirectory;
    private String name;
    private String href;
    private String type;
    private String size;
    private String date;

    /**
     * Create a new FileInfo object.
     *
     * @param name        The file name
     * @param href        The hyperlink that can either be used to access a list of
     *                    {@code FileInfo} objects for this directory (if this is
     *                    a directory), or to play this file with MPC-HC
     * @param type        The file type
     * @param size        The file size
     * @param date        The last modified date of the file
     * @param isDirectory True if this file is a directory
     * @see MediaPlayerApi#openFile(FileInfo)
     */
    public FileInfo(String name, String href, String type, String size,
                    String date, boolean isDirectory) {
        this.name = name;
        this.href = href;
        this.type = type;
        this.size = size;
        this.date = date;
        this.isDirectory = isDirectory;
    }

    public static List<FileInfo> fromJson(String responseString) throws JSONException {
        List<FileInfo> files = new ArrayList<>();
        JSONObject rootJson = new JSONObject(responseString);
        JSONArray elementJsonArray = rootJson.getJSONArray("element");
        for (int i = 0; i < elementJsonArray.length(); i++) {
            JSONObject jsonObject = elementJsonArray.getJSONObject(i);
            String type = jsonObject.getString("type");
            String uri = jsonObject.getString("path");
            uri = uri.replace('\\', '/');
            String name = jsonObject.getString("name");
            files.add(new FileInfo(
                    name,
                    uri,
                    type,
                    jsonObject.getString("size"),
                    jsonObject.getString("modification_time"),
                    type.equals("dir")));
        }

        return files;
    }

    /**
     * Parse a table element into a list of {@link FileInfo} objects.
     *
     * @param element A jsoup table element
     * @return A list of {@link FileInfo} objects
     */
    public static List<FileInfo> fromHTMLTableElement(Element element) {
        List<FileInfo> files = new ArrayList<>();

        for (Element row : element.getElementsByTag("tr")) {
            if (row.getElementsByTag("th").isEmpty()) {
                String name, href, type, size, date;
                boolean isDirectory = false;
                Elements columns = row.getElementsByTag("td");

                for (Element e : columns) {
                    if (e.className().equals("dirname")) {
                        isDirectory = true;
                        break;
                    }
                }

                if (isDirectory) {
                    name = columns.get(0).getElementsByTag("a").first().text();
                    href = columns.get(0).getElementsByTag("a").first().attr("href");
                    type = columns.get(1).text();
                    size = columns.get(2).text();
                    date = columns.get(3).text();
                } else {
                    name = columns.get(0).getElementsByTag("a").first().text();
                    href = columns.get(0).getElementsByTag("a").first().attr("href");
                    type = columns.get(1).getElementsByTag("span").first().text();
                    size = columns.get(2).getElementsByTag("span").first().text();
                    date = columns.get(3).getElementsByTag("span").first().text();
                }

                files.add(new FileInfo(name, href, type, size, date, isDirectory));
            }
        }

        return files;
    }

    public boolean isMediaOrPlaylist() {
        return FileInfo.MEDIA_AND_PLAYLISTS_EXTENSIONS_REGEX.matcher(name).find();
    }

    public boolean isPlaylist() {
        return FileInfo.PLAYLISTS_REGEX.matcher(name).find();
    }

    /**
     * Returns {@code true} if the file is a directory.
     *
     * @return {@code true} if the file is a directory.
     */
    public boolean isDirectory() {
        return isDirectory;
    }

    /**
     * Returns the name of the file.
     *
     * @return the name of the file
     */
    public String getFileName() {
        return name;
    }

    /**
     * Returns the relative hyperlink to the file.
     *
     * @return the relative hyperlink to the file
     */
    public String getHref() {
        return href;
    }

    /**
     * Returns the file type.
     *
     * @return the file type
     */
    public String getFileType() {
        return type;
    }

    /**
     * Returns the size of the file.
     *
     * @return the size of the file
     */
    public String getFileSize() {
        return size;
    }

    /**
     * Returns the timestamp of when this file was last modified.
     *
     * @return the timestamp of when this file was last modified
     */
    public String getLastModified() {
        return date;
    }
}
