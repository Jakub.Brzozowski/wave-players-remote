package com.jakubbrzozowski.waveplayersremote.main;

import com.jakubbrzozowski.waveplayersremote.eventbus.MainToolbarTitleChangeEvent;
import com.jakubbrzozowski.waveplayersremote.ui.CommonModel;
import com.jakubbrzozowski.waveplayersremote.ui.filebrowser.FileBrowserPresenter;
import com.jakubbrzozowski.waveplayersremote.ui.main.MainContract;
import com.jakubbrzozowski.waveplayersremote.ui.main.MainPresenter;
import com.jakubbrzozowski.waveplayersremote.ui.remote.RemotePresenter;
import com.jakubbrzozowski.waveplayersremote.ui.servermanager.ServerManagerPresenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class MainPresenterTest {

    @Mock
    private MainContract.Model mMainModel;

    @Mock
    private CommonModel mCommonModel;

    @Mock
    private MainContract.View mMainView;


    private MainPresenter mMainPresenter;

    @Before
    public void setupTasksPresenter() {
        MockitoAnnotations.initMocks(this);

        mMainPresenter = new MainPresenter(mCommonModel, mMainModel);
        mMainPresenter.attachView(mMainView);
    }

    @Test
    public void should_NotSetToolbarTitle_When_TitleFromFileBrowserAndBrowserNotVisible() throws
            Exception {
        Mockito.when(mMainView.isFileBrowserDrawerOpen()).thenReturn(false);

        mMainPresenter.onEvent(
                new MainToolbarTitleChangeEvent("title", FileBrowserPresenter.class));

        verify(mMainView, times(0)).setToolbarTitle(anyString());
    }

    @Test
    public void should_SetToolbarTitle_When_TitleFromFileBrowserAndBrowserVisible() throws
            Exception {
        Mockito.when(mMainView.isFileBrowserDrawerOpen()).thenReturn(true);

        mMainPresenter.onEvent(
                new MainToolbarTitleChangeEvent("title", FileBrowserPresenter.class));

        verify(mMainView, times(1)).setToolbarTitle(anyString());
    }

    @Test
    public void should_NotSetToolbarTitle_When_TitleFromRemoteAndBrowserVisible() throws
            Exception {
        Mockito.when(mMainView.isFileBrowserDrawerOpen()).thenReturn(true);

        mMainPresenter.onEvent(
                new MainToolbarTitleChangeEvent("title", RemotePresenter.class));

        verify(mMainView, times(0)).setToolbarTitle(anyString());
    }

    @Test
    public void should_SetToolbarTitle_When_TitleFromRemoteAndBrowserNotVisible() throws
            Exception {
        Mockito.when(mMainView.isFileBrowserDrawerOpen()).thenReturn(false);

        mMainPresenter.onEvent(
                new MainToolbarTitleChangeEvent("title", RemotePresenter.class));

        verify(mMainView, times(1)).setToolbarTitle(anyString());
    }

    @Test
    public void should_SetToolbarTitle_When_TitleFromServerManagerAndBrowserVisible() throws
            Exception {
        Mockito.when(mMainView.isFileBrowserDrawerOpen()).thenReturn(true);

        mMainPresenter.onEvent(
                new MainToolbarTitleChangeEvent("title", ServerManagerPresenter.class));

        verify(mMainView, times(1)).setToolbarTitle(anyString());
    }

    @Test
    public void should_SetToolbarTitle_When_TitleFromServerManagerAndBrowserNotVisible() throws
            Exception {
        Mockito.when(mMainView.isFileBrowserDrawerOpen()).thenReturn(false);

        mMainPresenter.onEvent(
                new MainToolbarTitleChangeEvent("title", ServerManagerPresenter.class));

        verify(mMainView, times(1)).setToolbarTitle(anyString());
    }
}