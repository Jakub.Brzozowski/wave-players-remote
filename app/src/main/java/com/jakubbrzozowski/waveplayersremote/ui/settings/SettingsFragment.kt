package com.jakubbrzozowski.waveplayersremote.ui.settings

import android.Manifest
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Vibrator
import androidx.core.content.ContextCompat
import androidx.preference.CheckBoxPreference
import androidx.preference.EditTextPreference
import androidx.preference.Preference.OnPreferenceChangeListener
import androidx.preference.PreferenceGroup
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.jakubbrzozowski.waveplayersremote.R
import com.jakubbrzozowski.waveplayersremote.WavePlayersRemoteApplication
import com.jakubbrzozowski.waveplayersremote.data.ServerUtils
import com.jakubbrzozowski.waveplayersremote.utils.DialogUtils
import com.takisoft.fix.support.v7.preference.PreferenceFragmentCompatDividers
import javax.inject.Inject

class SettingsFragment : PreferenceFragmentCompatDividers(), OnPreferenceChangeListener, SharedPreferences.OnSharedPreferenceChangeListener {

    @Inject
    lateinit internal var mShPrefs: SharedPreferences
    lateinit internal var mPauseOnRingingPref: CheckBoxPreference

    override fun onCreatePreferencesFix(savedInstanceState: Bundle?, rootKey: String?) {
        (activity?.application as WavePlayersRemoteApplication).component.inject(this)
        addPreferencesFromResource(R.xml.preferences)
        mShPrefs.registerOnSharedPreferenceChangeListener(this)

        findPreference(getString(R.string.pref_server_address_key)).onPreferenceChangeListener = this
        findPreference(getString(R.string.pref_server_port_key)).onPreferenceChangeListener = this

        mPauseOnRingingPref = findPreference(getString(R.string
                .pref_pause_on_ringing_key)) as CheckBoxPreference
        mPauseOnRingingPref.onPreferenceChangeListener = this

        val vibrationStrengthPref: EditTextPreference
        val filterDelayPref: EditTextPreference
        vibrationStrengthPref = findPreference(
                getString(R.string.pref_vibration_strength_key)) as EditTextPreference
        vibrationStrengthPref.dialogMessage = getString(R.string.pref_vibration_strength_dialog_message)
        vibrationStrengthPref.onPreferenceChangeListener = this
        filterDelayPref = findPreference(getString(R.string.pref_filter_delay_key)) as EditTextPreference
        filterDelayPref.dialogMessage = getString(R.string.pref_filter_delay_dialog_message)
        filterDelayPref.onPreferenceChangeListener = this
    }

    private fun checkPreferenceAvailability() {
        val waveToPlayPause: CheckBoxPreference
        val vibrateOnWave: CheckBoxPreference
        val vibrationStrengthPref: EditTextPreference
        val filterDelayPref: EditTextPreference
        waveToPlayPause = findPreference(getString(R.string.pref_wave_service_auto_startup_key)) as CheckBoxPreference
        vibrateOnWave = findPreference(getString(R.string.pref_vibrate_on_wave_key)) as CheckBoxPreference
        vibrationStrengthPref = findPreference(
                getString(R.string.pref_vibration_strength_key)) as EditTextPreference
        vibrationStrengthPref.dialogMessage = getString(R.string.pref_vibration_strength_dialog_message)
        vibrationStrengthPref.onPreferenceChangeListener = this
        filterDelayPref = findPreference(getString(R.string.pref_filter_delay_key)) as EditTextPreference
        filterDelayPref.dialogMessage = getString(R.string.pref_filter_delay_dialog_message)
        filterDelayPref.onPreferenceChangeListener = this
        val pm = requireContext().packageManager
        val hasTelephony = pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY)
        val hasProximity = pm.hasSystemFeature(PackageManager.FEATURE_SENSOR_PROXIMITY)
        val vibrator = requireContext().getSystemService(Context.VIBRATOR_SERVICE) as? Vibrator
        var hasVibrator = false
        if (vibrator != null) {
            hasVibrator = vibrator.hasVibrator()
        }

        if (hasProximity) {
            waveToPlayPause.isEnabled = true
        } else {
            waveToPlayPause.summary = getString(R.string.pref_no_poximity_summary)
            waveToPlayPause.isEnabled = false
        }

        if (hasTelephony) {
            mPauseOnRingingPref.isEnabled = true
        } else {
            mPauseOnRingingPref.summary = getString(R.string.pref_no_telephony_summary)
            mPauseOnRingingPref.isEnabled = false
        }

        if (hasProximity && hasVibrator) {
            filterDelayPref.isEnabled = true
            vibrateOnWave.isEnabled = true
            vibrationStrengthPref.isEnabled = true
        } else {
            val summary: String
            if (hasProximity && !hasVibrator) {
                summary = getString(R.string.preferences_vibrate_not_supported)
            } else if (!hasProximity && hasVibrator) {
                summary = getString(R.string.pref_no_poximity_summary)
            } else {
                summary = getString(R.string.preferences_vibrate_not_supported) + "\n" +
                        getString(R.string.pref_no_poximity_summary)
            }
            filterDelayPref.summary = summary
            vibrateOnWave.summary = summary
            vibrationStrengthPref.summary = summary
            filterDelayPref.isEnabled = false
            vibrateOnWave.isEnabled = false
            vibrationStrengthPref.isEnabled = false
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        setDividerPreferences(PreferenceFragmentCompatDividers.DIVIDER_DEFAULT)
        val drawable = requireContext().resources.getDrawable(R.drawable.line_divider)
        setDivider(ColorDrawable(requireContext().resources.getColor(R.color.colorAccent)))
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //        setDividerPreferences(DIVIDER_DEFAULT);
        //        Drawable drawable = getContext().getResources().getDrawable(R.drawable.line_divider);
        //        setDivider(drawable);
        initSummaries(preferenceScreen)
    }

    private fun initSummaries(prefGroup: PreferenceGroup?) {
        for (i in 0 until (prefGroup?.preferenceCount ?: return)) {
            val preference = prefGroup.getPreference(i)
            if (preference is PreferenceGroup) {
                initSummaries(preference)
            } else if (preference is EditTextPreference) {
                preference.setSummary(preference.text)
            } else if (preference is CheckBoxPreference) {
                val key = preference.getKey()
                if (key == getString(R.string.pref_wave_service_auto_startup_key)) {
                    preference.setSummary(if (preference.isChecked)
                        getString(R.string.pref_wave_service_auto_startup_summary_on)
                    else
                        getString(R.string.pref_wave_service_auto_startup_summary_off))
                } else if (key == getString(R.string.pref_vibrate_on_wave_key)) {
                    preference.setSummary(if (preference.isChecked)
                        getString(R.string.pref_vibrate_on_wave_summary_on)
                    else
                        getString(R.string.pref_vibrate_on_wave_summary_off))
                } else if (key == getString(R.string.pref_pause_on_ringing_key)) {
                    preference.setSummary(if (preference.isChecked)
                        getString(R.string.pref_pause_on_ringing_summary_on)
                    else
                        getString(R.string.pref_pause_on_ringing_summary_off))
                }
            }
        }
        checkPreferenceAvailability()
    }

    override fun onPreferenceChange(preference: androidx.preference.Preference, newValue: Any): Boolean {
        val key = preference.key
        if (key == getString(R.string.pref_pause_on_ringing_key)) {
            if (newValue as Boolean) {
                if (ContextCompat.checkSelfPermission(requireContext(),
                        Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    activity?.let { DialogUtils.getRingingPermissionDialog(it).show() }
                }
            }
        }

        if (key == getString(R.string.pref_server_address_key)) {
            if (!ServerUtils.isIpAddress(newValue as String)) {
                Toast.makeText(context, getString(R.string.err_msg_invalid_address),
                        Toast.LENGTH_SHORT).show()
                return false
            }
        }

        if (key == getString(R.string.pref_server_port_key)) {
            if (!ServerUtils.isPortValid(newValue as String)) {
                Toast.makeText(context, getString(R.string.err_msg_invalid_port),
                        Toast.LENGTH_SHORT).show()
                return false
            }
        }

        if (key == getString(R.string.pref_filter_delay_key)) {
            val value = newValue as String
            val delayInt: Int
            try {
                delayInt = Integer.parseInt(value)
                if (delayInt < 0) {
                    Toast.makeText(context, "Value can't be < 0", Toast.LENGTH_SHORT).show()
                    return false
                } else if (delayInt > 5000) {
                    Toast.makeText(context, "Value can't be > 5000", Toast.LENGTH_SHORT)
                            .show()
                    return false
                }
            } catch (e: NumberFormatException) {
                Toast.makeText(context, "Value is not a valid integer", Toast
                        .LENGTH_SHORT).show()
                return false
            }

            return true
        }

        if (key == getString(R.string.pref_vibration_strength_key)) {
            val value = newValue as String
            val strengthInt: Int
            try {
                strengthInt = Integer.parseInt(value)
                if (strengthInt < 1) {
                    Toast.makeText(context, "Value can't be < 1", Toast.LENGTH_SHORT).show()
                    return false
                } else if (strengthInt > 5) {
                    Toast.makeText(context, "Value can't be > 5", Toast.LENGTH_SHORT).show()
                    return false
                }
            } catch (e: NumberFormatException) {
                Toast.makeText(context, "Value is not a valid integer", Toast
                        .LENGTH_SHORT).show()
                return false
            }

            return true
        }
        return true
    }

    override fun onDestroy() {
        mShPrefs.unregisterOnSharedPreferenceChangeListener(this)
        super.onDestroy()
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        if (key == getString(R.string.pref_pause_on_ringing_key)) {
            mPauseOnRingingPref.isChecked = context?.resources?.getBoolean(R.bool.pref_pause_on_ringing_default)
                ?.let { sharedPreferences?.getBoolean(key, it) } == true
        }

        initSummaries(preferenceScreen)
    }
}
