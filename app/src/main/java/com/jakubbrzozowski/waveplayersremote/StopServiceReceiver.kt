package com.jakubbrzozowski.waveplayersremote

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences

import com.jakubbrzozowski.waveplayersremote.ui.main.MainActivity

import javax.inject.Inject

class StopServiceReceiver(private val mMainActivity: MainActivity) : BroadcastReceiver() {

    @Inject
    lateinit internal var mShPref: SharedPreferences

    init {
        (mMainActivity.applicationContext as WavePlayersRemoteApplication).component.inject(this)
    }

    override fun onReceive(context: Context, intent: Intent) {
        // an Intent broadcast.
        context.sendBroadcast(Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS))
        mMainActivity.presenter.stopWaveService()
        mShPref.edit().putBoolean(mMainActivity.getString(
                R.string.pref_wave_service_auto_startup_key), false).apply()
    }
}
