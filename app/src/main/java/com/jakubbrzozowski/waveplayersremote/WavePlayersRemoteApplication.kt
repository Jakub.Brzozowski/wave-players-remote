package com.jakubbrzozowski.waveplayersremote

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Vibrator
import androidx.appcompat.app.AppCompatDelegate
import com.jakubbrzozowski.waveplayersremote.dagger.DaggerStorageComponent
import com.jakubbrzozowski.waveplayersremote.dagger.StorageComponent
import com.jakubbrzozowski.waveplayersremote.dagger.StorageModule
import io.reactivex.exceptions.UndeliverableException
import io.reactivex.plugins.RxJavaPlugins
import io.realm.Realm
import io.realm.RealmConfiguration
import timber.log.Timber
import java.io.IOException
import javax.inject.Inject

class WavePlayersRemoteApplication : Application() {

    @Inject
    lateinit internal var mShPrefs: SharedPreferences

    lateinit var component: StorageComponent
        internal set

    override fun onCreate() {
        super.onCreate()
        component = DaggerStorageComponent.builder()
                .storageModule(StorageModule(this))
                .build()

        component.inject(this)

        // Initialize prefs according to available hardware
        val pm = packageManager
        val hasTelephony = pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY)
        val hasProximity = pm.hasSystemFeature(PackageManager.FEATURE_SENSOR_PROXIMITY)
        val vibrator = getSystemService(Context.VIBRATOR_SERVICE) as? Vibrator
        var hasVibrator = false

        if (vibrator != null) {
            hasVibrator = vibrator.hasVibrator()
        }

        if (!hasProximity) {
            mShPrefs.edit().putBoolean(getString(R.string.pref_wave_service_auto_startup_key),
                    false).apply()
        }
        if (!hasProximity || !hasVibrator) {
            mShPrefs.edit().putBoolean(getString(R.string.pref_vibrate_on_wave_key), false).apply()
        }
        if (!hasTelephony) {
            mShPrefs.edit().putBoolean(getString(R.string.pref_pause_on_ringing_key), false).apply()
        }

        Realm.init(this)
        val config = RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build()
        Realm.setDefaultConfiguration(config)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        setRxJavaPlugins()
    }

    private fun setRxJavaPlugins() {
        RxJavaPlugins.setErrorHandler handler@ { e ->
            if (e is UndeliverableException) {
                Timber.d(e)
            }
            if (e is IOException) {
                // fine, irrelevant network problem or API that throws on cancellation
                Timber.d(e)
                return@handler
            }
            if (e is InterruptedException) {
                // fine, some blocking code was interrupted by a dispose call
                Timber.d(e)
                return@handler
            }
            if (e is NullPointerException || e is IllegalArgumentException) {
                // that's likely a bug in the application
                Thread.currentThread().uncaughtExceptionHandler
                        .uncaughtException(Thread.currentThread(), e)
                return@handler
            }
            if (e is IllegalStateException) {
                // that's a bug in RxJava or in a custom operator
                Thread.currentThread().uncaughtExceptionHandler
                        .uncaughtException(Thread.currentThread(), e)
                return@handler
            }
            Timber.w("Undeliverable exception received, not sure what to do", e)
        }
    }
}
