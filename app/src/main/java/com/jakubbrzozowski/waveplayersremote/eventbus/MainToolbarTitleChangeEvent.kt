package com.jakubbrzozowski.waveplayersremote.eventbus


class MainToolbarTitleChangeEvent(val toolbarTitle: String, val sourceClass: Class<*>)
