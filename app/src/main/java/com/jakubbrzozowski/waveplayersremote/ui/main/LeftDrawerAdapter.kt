package com.jakubbrzozowski.waveplayersremote.ui.main

import android.content.Context
import android.content.res.TypedArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView

import com.jakubbrzozowski.waveplayersremote.R

class LeftDrawerAdapter(private val mContext: Context, private val mLabelsArray: Array<String>, private val mImageIdsTypedArray: TypedArray) : BaseAdapter() {

    override fun getCount(): Int {
        return mLabelsArray.size
    }

    override fun getItem(arg0: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val inflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val row: View
        row = inflater.inflate(R.layout.left_drawer_list_item, parent, false)
        val title: TextView
        val imageView: ImageView
        imageView = row.findViewById(R.id.list_item_imageView)
        title = row.findViewById(R.id.list_item_textView)
        title.text = mLabelsArray[position]
        imageView.setImageResource(mImageIdsTypedArray.getResourceId(position, 0))

        return row
    }
}