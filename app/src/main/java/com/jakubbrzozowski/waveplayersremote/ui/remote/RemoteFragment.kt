package com.jakubbrzozowski.waveplayersremote.ui.remote

import android.os.Bundle
import android.os.Handler
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.OnLongClick
import com.jakubbrzozowski.waveplayersremote.R
import com.jakubbrzozowski.waveplayersremote.WavePlayersRemoteApplication
import com.jakubbrzozowski.waveplayersremote.ui.PressableImageView
import com.jakubbrzozowski.waveplayersremote.ui.servermanager.ServerManagerFragment
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import timber.log.Timber
import javax.inject.Inject

class RemoteFragment : Fragment(), RemoteContract.View {

    @BindView(R.id.remote_background_image_view)
    internal lateinit var mRemoteBackgroundImageView: ImageView
    @BindView(R.id.current_position_textView)
    internal lateinit var mCurrentPositionTextView: TextView
    @BindView(R.id.end_position_textView)
    internal lateinit var mEndPositionTextView: TextView
    @BindView(R.id.progress_seekBar)
    internal lateinit var mProgressSeekBar: SeekBar
    @BindView(R.id.volume_seekBar)
    internal lateinit var mVolumeSeekBar: SeekBar
    @BindView(R.id.toggle_play_imageView)
    internal lateinit var mTogglePlayImageView: ImageView
    @BindView(R.id.jump_forward_imageView)
    internal lateinit var mJumpForwardImageView: PressableImageView
    @BindView(R.id.jump_backward_imageView)
    internal lateinit var mJumpBackwardImageView: PressableImageView
    @BindView(R.id.wave_hand_imageView)
    internal lateinit var mWaveHandImageView: ImageView
    @Inject
    lateinit internal var mPresenter: RemoteContract.Presenter
    internal var mSnackbar: Snackbar? = null

    override val backgroundImageViewWith: Int
        get() = mRemoteBackgroundImageView.width

    override val isSnackBarNull: Boolean
        get() = mSnackbar == null

    /**
     * Call this only if isSnackBarNull returns false.
     */
    override val isSnackBarShown: Boolean
        get() = mSnackbar!!.isShown

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        (context!!.applicationContext as WavePlayersRemoteApplication).component.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_remote, container, false)
        mPresenter.attachView(this)
        ButterKnife.bind(this, view)
        setOnTouchListeners()
        setOnSeekBarChangeListeners()
        return view
    }

    override fun initSnackBar() {
        val view = view
        if (view != null) {
            mSnackbar = Snackbar.make(view, R.string.no_connection_snackbar_message,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.no_connection_snackbar_action_server_manager
                    ) { view1 -> mPresenter.snackBarChangeServerAction(ServerManagerFragment::class.java, R.id.drawer_middle_layout) }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mPresenter.setDefaultToolbarTitle()
    }

    override fun onDestroyView() {
        mPresenter.detachView()
        super.onDestroyView()
    }

    private fun setOnTouchListeners() {
        mJumpForwardImageView.setOnTouchListener lis@{ v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    v.isPressed = true
                    mPresenter.startJumpingForward()
                    v.performClick()
                    return@lis true
                }
                MotionEvent.ACTION_CANCEL, MotionEvent.ACTION_UP -> {
                    v.isPressed = false
                    mPresenter.stopJumpingForward()
                    return@lis true
                }
            }
            false
        }
        mJumpBackwardImageView.setOnTouchListener lis@{ v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    v.isPressed = true
                    mPresenter.startJumpingBackward()
                    v.performClick()
                    return@lis true
                }
                MotionEvent.ACTION_CANCEL, MotionEvent.ACTION_UP -> {
                    v.isPressed = false
                    mPresenter.stopJumpingBackward()
                    return@lis true
                }
            }
            false
        }
    }

    private fun setOnSeekBarChangeListeners() {
        mProgressSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                mPresenter.onProgressSeekBarChange(progress, fromUser)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
                mPresenter.stopUpdatingInfo()
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {

                val handler = Handler()
                val r = {
                    mPresenter.stopUpdatingInfo()
                    mPresenter.startInfoUpdate()
                }
                handler.postDelayed(r, 500)
                mPresenter.updateSnapshot()
            }
        })
        mVolumeSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                mPresenter.onVolumeSeekBarChange(progress, fromUser)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {

            }
        })
    }

    override fun onStart() {
        Timber.d("onStart")
        super.onStart()
        mPresenter.invalidateOptionsMenu()
        mPresenter.startInfoUpdate()
        mPresenter.initSnackBar()
        mPresenter.initWaveHandIcon()
        mPresenter.startSnapshotUpdate()
        mPresenter.unlockRightDrawer()
    }

    override fun onStop() {
        Timber.d("onStop")
        mPresenter.stopSnapshotUpdate()
        mPresenter.dismissSnackBar()
        mSnackbar = null
        mPresenter.stopUpdatingInfo()
        super.onStop()
    }

    @OnClick(R.id.toggle_play_imageView)
    fun togglePlay() {
        mPresenter.togglePlay()
    }

    @OnClick(R.id.audio_track_imageView)
    fun nextAudioTrack() {
        mPresenter.nextAudioTrack()
    }

    @OnClick(R.id.subtitles_imageView)
    fun nextSubtitles() {
        mPresenter.nextSubtitles()
    }

    @OnClick(R.id.fullscreen_imageView)
    fun toggleFullscreen() {
        mPresenter.toggleFullscreen()
    }

    @OnClick(R.id.repeat_imageView)
    fun toggleRepeatMode() {
        mPresenter.toggleRepeatMode()
    }

    @OnClick(R.id.skip_next_imageView)
    fun nextFile() {
        mPresenter.nextFile()
    }

    @OnClick(R.id.skip_previous_imageView)
    fun previousFile() {
        mPresenter.previousFile()
    }

    @OnClick(R.id.wave_hand_imageView)
    fun onWaveHandSwitchClick() {
        mPresenter.waveHandToggle()
    }

    override fun setVlcBackground() {
        mRemoteBackgroundImageView.setImageResource(R.drawable.cone_soppera10_large)
    }

    override fun loadMpcBackground(address: String, port: String) {
        Picasso.with(context).load("http://$address:$port/snapshot.jpg")
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .noFade()
                .centerCrop()
                .placeholder(mRemoteBackgroundImageView.drawable)
                .resize(mRemoteBackgroundImageView.measuredWidth,
                        mRemoteBackgroundImageView.measuredHeight)
                .into(mRemoteBackgroundImageView)
    }

    @OnLongClick(R.id.skip_next_imageView, R.id.subtitles_imageView, R.id.toggle_play_imageView, R.id.audio_track_imageView, R.id.skip_previous_imageView, R.id.fullscreen_imageView, R.id.wave_hand_imageView, R.id.repeat_imageView)
    fun onControlLongClick(view: View): Boolean {
        Toast.makeText(context, view.contentDescription, Toast.LENGTH_SHORT).show()
        return true
    }

    override fun setCurrentPositionText(text: String) {
        mCurrentPositionTextView.text = text
    }

    override fun setEndPositionText(text: String) {
        mEndPositionTextView.text = text
    }

    override fun setProgressSeekBarProgress(progress: Int) {
        mProgressSeekBar.progress = progress
    }

    override fun setVolumesSeekBarProgress(progress: Int) {
        mVolumeSeekBar.progress = progress
    }

    override fun displayPauseIcon() {
        mTogglePlayImageView.setImageResource(android.R.drawable.ic_media_pause)
    }

    override fun displayPlayIcon() {
        mTogglePlayImageView.setImageResource(android.R.drawable.ic_media_play)
    }

    override fun setWaveHandIconOff() {
        mWaveHandImageView.setImageResource(R.drawable.ic_wave_hand_off)
    }

    override fun setWaveHandIconOn() {
        mWaveHandImageView.setImageResource(R.drawable.ic_wave_hand)
    }

    /**
     * Call this only if isSnackBarNull returns false.
     */
    override fun showSnackBar() {
        mSnackbar!!.show()
    }

    /**
     * Call this only if isSnackBarNull returns false.
     */
    override fun dismissSnackBar() {
        mSnackbar!!.dismiss()
    }
}// Required empty public constructor
