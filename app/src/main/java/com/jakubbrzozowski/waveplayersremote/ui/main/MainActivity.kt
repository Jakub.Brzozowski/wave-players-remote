package com.jakubbrzozowski.waveplayersremote.ui.main

import android.Manifest
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.*
import butterknife.ButterKnife
import com.afollestad.materialdialogs.MaterialDialog
import com.jakubbrzozowski.waveplayersremote.R
import com.jakubbrzozowski.waveplayersremote.StopServiceReceiver
import com.jakubbrzozowski.waveplayersremote.WavePlayersRemoteApplication
import com.jakubbrzozowski.waveplayersremote.ui.filebrowser.FileBrowserFragment
import com.jakubbrzozowski.waveplayersremote.ui.remote.RemoteFragment
import com.jakubbrzozowski.waveplayersremote.ui.servermanager.ServerManagerFragment
import com.jakubbrzozowski.waveplayersremote.ui.settings.SettingsActivity
import com.jakubbrzozowski.waveplayersremote.ui.tutorial.TutorialActivity
import com.jakubbrzozowski.waveplayersremote.utils.DialogUtils
import com.jakubbrzozowski.waveplayersremote.utils.ThemeUtils
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber
import java.lang.reflect.InvocationTargetException
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainContract.View {

    @Inject
    lateinit var presenter: MainContract.Presenter
        internal set
    internal var mToolbar: Toolbar? = null
    private var mStopServiceReceiver: StopServiceReceiver? = null
    private var mDrawerToggle: ActionBarDrawerToggle? = null
    private var mRingPermDialog: MaterialDialog? = null

    override val isFileBrowserDrawerOpen: Boolean
        get() = main_drawer_layout.isDrawerOpen(GravityCompat.END)

    override fun startFirstRunTutorial() {
        val intent = Intent(this, SettingsActivity::class.java)
        intent.action = SettingsActivity.ACTION_CLOSE
        startActivity(intent)
        startActivity(Intent(this, TutorialActivity::class.java))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as WavePlayersRemoteApplication).component.inject(this)
        ThemeUtils.onActivityCreateSetTheme(this)
        setContentView(R.layout.activity_main)
        mToolbar = findViewById(R.id.main_toolbar)
        setSupportActionBar(mToolbar)

        ButterKnife.bind(this)
        if (supportFragmentManager.fragments.isEmpty()) {
            loadFragment(RemoteFragment::class.java, R.id.drawer_middle_layout)
            loadFragment(FileBrowserFragment::class.java, R.id.right_drawer_frame_layout)
        }
        val intent = intent
        if (intent != null && intent.action != null && intent.action == ACTION_SERVER_MANAGER) {
            loadFragment(ServerManagerFragment::class.java, R.id.drawer_middle_layout)
        }

        // Set the adapter for the list view
        left_drawer.adapter = LeftDrawerAdapter(this,
                resources.getStringArray(R.array.left_drawer_labels),
                resources.obtainTypedArray(R.array.left_drawer_icons))

        left_drawer.setOnItemClickListener { adapterView, view, position, id ->
            when (position) {
                0 -> {
                    loadFragment(RemoteFragment::class.java, R.id.drawer_middle_layout)
                    loadFragment(FileBrowserFragment::class.java, R.id.right_drawer_frame_layout)
                }
                1 -> loadFragment(ServerManagerFragment::class.java, R.id.drawer_middle_layout)
                2 -> startActivity(Intent(this@MainActivity, SettingsActivity::class.java))
                3 -> startActivity(Intent(this@MainActivity, TutorialActivity::class.java))
                4 -> DialogUtils.getLegalNoticeDialog(this@MainActivity).show()
            }
            main_drawer_layout.closeDrawer(GravityCompat.START)
        }

        val stopServiceFilter = IntentFilter("action_stop_service")
        mStopServiceReceiver = StopServiceReceiver(this)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            registerReceiver(mStopServiceReceiver, stopServiceFilter, RECEIVER_EXPORTED)
        } else {
            registerReceiver(mStopServiceReceiver, stopServiceFilter)
        }

        setUpDrawerToggle()

    }

    override fun onStart() {
        super.onStart()
        presenter.attachView(this)
        presenter.firstRunCheck()
        presenter.initializeService()
        presenter.initKeepScreenOn()
    }

    override fun onStop() {
        super.onStop()
        presenter.detachView()
    }

    private fun setUpDrawerToggle() {
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setHomeButtonEnabled(true)
        }

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the navigation drawer and the action bar app icon.
        mDrawerToggle = object : ActionBarDrawerToggle(
                this, /* host Activity */
                main_drawer_layout, /* DrawerLayout object */
                R.string.navigation_drawer_open, /* "open drawer" description for accessibility */
                R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
        ) {
            override fun onDrawerClosed(drawerView: View) {
                invalidateOptionsMenu() // calls onPrepareOptionsMenu()
                if (drawerView.id == R.id.right_drawer_frame_layout) {
                    presenter.updateProgress()
                }
            }

            override fun onDrawerOpened(drawerView: View) {
                invalidateOptionsMenu() // calls onPrepareOptionsMenu()
                if (drawerView.id == R.id.right_drawer_frame_layout) {
                    presenter.initFileBrowser()
                }
            }
        }

        // Defer code dependent on restoration of previous instance state.
        // NB: required for the drawer indicator to show up!
        main_drawer_layout.post { mDrawerToggle!!.syncState() }

        main_drawer_layout.addDrawerListener(mDrawerToggle!!)
    }

    override fun loadFragment(tFragment: Class<*>, layoutId: Int) {
        val fragmentClassName = tFragment.name

        val manager = supportFragmentManager
        val fragmentPopped = manager.popBackStackImmediate(fragmentClassName, 0)
        var fragment: Fragment? = supportFragmentManager.findFragmentByTag(fragmentClassName)
        if (!fragmentPopped) { //fragment not in back stack, create it.
            if (fragment == null || !fragment.isVisible) {
                try {
                    fragment = tFragment.newInstance() as Fragment
                    val ft = manager.beginTransaction()
                    ft.replace(layoutId, fragment, fragmentClassName)
                    ft.addToBackStack(fragmentClassName)
                    ft.commit()
                } catch (e: IllegalAccessException) {
                    Timber.d(e)
                } catch (e: InstantiationException) {
                    Timber.d(e)
                }

            }
        }
    }

    private fun checkPermissions() {
        if (presenter.isPauseOnRingingPref && ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            if (mRingPermDialog == null) {
                mRingPermDialog = DialogUtils.getRingingPermissionDialog(this)
            }
            if (!mRingPermDialog!!.isShowing) {
                mRingPermDialog!!.show()
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSIONS_REQUEST_READ_PHONE_STATE -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    presenter.setPauseOnRinging(false)
                }
                return
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        val fragment = supportFragmentManager.findFragmentByTag(ServerManagerFragment::class.java.name)
        if (fragment == null || !fragment.isVisible) {
            menuInflater.inflate(R.menu.menu_main, menu)
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_right_nav -> {
                if (main_drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    main_drawer_layout.closeDrawer(GravityCompat.START)
                }
                if (main_drawer_layout.isDrawerOpen(GravityCompat.END)) {
                    main_drawer_layout.closeDrawer(GravityCompat.END)
                } else {
                    main_drawer_layout.openDrawer(GravityCompat.END)
                }
                return true
            }
            android.R.id.home -> {
                if (main_drawer_layout.isDrawerOpen(GravityCompat.END)) {
                    main_drawer_layout.closeDrawer(GravityCompat.END)
                } else if (main_drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    main_drawer_layout.closeDrawer(GravityCompat.START)
                } else {
                    main_drawer_layout.openDrawer(GravityCompat.START)
                }

                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        presenter.initializeService()
        checkPermissions()

        val clazz = ContextThemeWrapper::class.java
        try {
            val themeResId: Int
            val method = clazz.getMethod("getThemeResId")
            method.isAccessible = true
            themeResId = method.invoke(this) as Int
            presenter.checkTheme(themeResId)
        } catch (e: NoSuchMethodException) {
            Timber.d("Failed to get theme resource ID", e)
        } catch (e: IllegalAccessException) {
            Timber.d("Failed to get theme resource ID", e)
        } catch (e: IllegalArgumentException) {
            Timber.d("Failed to get theme resource ID", e)
        } catch (e: InvocationTargetException) {
            Timber.d("Failed to get theme resource ID", e)
        }

    }

    override fun onBackPressed() {
        if (main_drawer_layout.isDrawerOpen(GravityCompat.START)) {
            main_drawer_layout.closeDrawer(GravityCompat.START)
            return
        } else if (main_drawer_layout.isDrawerOpen(GravityCompat.END)) {
            main_drawer_layout.closeDrawer(GravityCompat.END)
            return
        } else if (supportFragmentManager.backStackEntryCount > 2) {
            supportFragmentManager.popBackStack()
            return
        }
        moveTaskToBack(true)
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(PERSIST_TOOLBAR_TITLE_KEY, mToolbar?.title.toString())
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        mToolbar?.title = savedInstanceState.getString(PERSIST_TOOLBAR_TITLE_KEY)
        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onDestroy() {
        presenter.stopWaveService()
        if (mRingPermDialog != null) {
            mRingPermDialog!!.dismiss()
        }
        unregisterReceiver(mStopServiceReceiver)
        super.onDestroy()
    }

    override fun setToolbarTitle(title: String) {
        mToolbar?.title = title
    }

    override fun lockRightDrawer() {
        main_drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, GravityCompat.END)
    }

    override fun unlockRightDrawer() {
        main_drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, GravityCompat.END)
    }

    override fun setKeepScreenOn() {
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    override fun setDontKeepScreenOn() {
        window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    override fun goToRemote() {
        supportFragmentManager.popBackStack()
    }

    companion object {

        val PERSIST_TOOLBAR_TITLE_KEY = "toolbar_title"
        val ACTION_SERVER_MANAGER = "action_server_manager"
        val PERMISSIONS_REQUEST_READ_PHONE_STATE = 4536
    }
}