package com.jakubbrzozowski.waveplayersremote.ui.filebrowser

import com.jakubbrzozowski.player_apis.FileInfo
import com.jakubbrzozowski.waveplayersremote.ui.CommonModel

interface FileBrowserContract {

    interface Model {
        val browserTitle: String

        fun openFile(mCommonModel: CommonModel, fileInfo: FileInfo?)

    }

    interface View {

        val isFragmentAdded: Boolean
        fun displayFileInfoList(fileInfo: List<FileInfo>)

    }

    interface Presenter {
        fun attachView(view: View)

        fun detachView()

        fun openFileInfo(fileInfo: FileInfo?)

        fun openRoot()

        fun initFileInfoList()

    }
}
