package com.jakubbrzozowski.waveplayersremote.ui.main


import com.jakubbrzozowski.waveplayersremote.eventbus.*
import com.jakubbrzozowski.waveplayersremote.ui.CommonModel
import com.jakubbrzozowski.waveplayersremote.ui.filebrowser.FileBrowserContract
import com.jakubbrzozowski.waveplayersremote.ui.remote.RemoteContract
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import javax.inject.Inject

class MainPresenter @Inject
internal constructor(private val mCommonModel: CommonModel, mainModel: MainContract.Model) :
        MainContract.Presenter {

    private var mView: MainContract.View? = null
    private val mMainModel: MainContract.Model = mainModel

    override val isPauseOnRingingPref: Boolean
        get() = mCommonModel.isPauseOnRinging

    override fun attachView(view: MainContract.View) {
        EventBus.getDefault().register(this)
        mView = view
        mCommonModel.setMainPresenter(this)
    }

    override fun detachView() {
        EventBus.getDefault().unregister(this)
        mView = null
        mCommonModel.setMainPresenter(null)
    }

    override fun updateProgress() {
        EventBus.getDefault().post(ProgressUpdateEvent())
    }

    @Subscribe
    fun onEvent(event: LoadFragmentEvent) {
        if (mView != null) {
            mView!!.loadFragment(event.fragmentClass, event.layoutId)
        }
    }

    @Subscribe
    fun onEvent(event: InvalidateOptionsMenuEvent) {
        if (mView != null) {
            mView!!.invalidateOptionsMenu()
        }
    }

    @Subscribe
    fun onEvent(event: UnlockRightDrawerEvent) {
        if (mView != null) {
            mView!!.unlockRightDrawer()
        }
    }

    @Subscribe
    fun onEvent(event: LockRightDrawerEvent) {
        if (mView != null) {
            mView!!.lockRightDrawer()
        }
    }

    @Subscribe
    fun onEvent(event: GoToRemoteEvent) {
        mView!!.goToRemote()
    }

    @Subscribe
    fun onEvent(event: MainToolbarTitleChangeEvent) {
        val toolbarTitle = event.toolbarTitle
        val clazz = event.sourceClass
        var implementsRemoteContractPresenter = false
        var implementsFileBrowserPresenter = false
        val interfaces = clazz.interfaces
        for (anInterface in interfaces) {
            if (anInterface.name == RemoteContract.Presenter::class.java.name) {
                implementsRemoteContractPresenter = true
            }
            if (anInterface.name == FileBrowserContract.Presenter::class.java.name) {
                implementsFileBrowserPresenter = true
            }
        }
        if (implementsRemoteContractPresenter) {
            if (!mView!!.isFileBrowserDrawerOpen) {
                mView!!.setToolbarTitle(toolbarTitle)
            }
        } else if (implementsFileBrowserPresenter) {
            if (mView!!.isFileBrowserDrawerOpen) {
                mView!!.setToolbarTitle(toolbarTitle)
            }
        } else {
            mView!!.setToolbarTitle(toolbarTitle)
        }
    }

    override fun stopWaveService() {
        mMainModel.stopWaveService()
    }

    override fun startWaveService() {
        mMainModel.startWaveService()
    }

    override fun initializeService() {
        if (mCommonModel.isAutoServiceStartupFlag) {
            startWaveService()
        } else {
            stopWaveService()
        }
    }

    override fun initKeepScreenOn() {
        setKeepScreenOn(mCommonModel.isKeepScreenOnFlag)
    }

    override fun initFileBrowser() {
        EventBus.getDefault().post(InitFileBrowserEvent())
    }

    override fun setPauseOnRinging(pauseOnRinging: Boolean) {
        mCommonModel.isPauseOnRinging = pauseOnRinging
    }

    override fun checkTheme(themeResId: Int) {
        val isThemeChanged =
                themeResId == CommonModel.VLC_APP_THEME_NO_ACTION_BAR && mCommonModel.isPrefPlayerMpc ||
                        themeResId == CommonModel.APP_THEME_NO_ACTION_BAR && mCommonModel.isPrefPlayerVlc
        if (isThemeChanged) {
            mView!!.recreate()
        }
    }

    override fun setKeepScreenOn(keepScreenOn: Boolean) {
        if (keepScreenOn) {
            mView!!.setKeepScreenOn()
        } else {
            mView!!.setDontKeepScreenOn()
        }
    }

    override fun firstRunCheck() {
        if (mMainModel.isFirstRun) {
            mView!!.startFirstRunTutorial()
        }
    }
}
