package com.jakubbrzozowski.waveplayersremote.ui.servermanager


import com.jakubbrzozowski.waveplayersremote.data.Server
import com.jakubbrzozowski.waveplayersremote.data.ServerUtils
import com.jakubbrzozowski.waveplayersremote.eventbus.GoToRemoteEvent
import com.jakubbrzozowski.waveplayersremote.eventbus.InvalidateOptionsMenuEvent
import com.jakubbrzozowski.waveplayersremote.eventbus.LockRightDrawerEvent
import com.jakubbrzozowski.waveplayersremote.eventbus.MainToolbarTitleChangeEvent
import com.jakubbrzozowski.waveplayersremote.ui.CommonModel
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import io.realm.OrderedCollectionChangeSet
import io.realm.OrderedRealmCollection
import io.realm.OrderedRealmCollectionChangeListener
import io.realm.RealmResults
import io.realm.internal.util.Pair
import org.greenrobot.eventbus.EventBus
import timber.log.Timber
import java.net.Inet4Address
import java.net.InetAddress
import java.net.NetworkInterface
import java.net.SocketException
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class ServerManagerPresenter @Inject
internal constructor(private val mCommonModel: CommonModel, serverManagerModel:
ServerManagerContract.Model, @param:Named("mainThread") private val mMainScheduler: Scheduler) :
        ServerManagerContract.Presenter,
        OrderedRealmCollectionChangeListener<RealmResults<Server>> {
    private var mView: ServerManagerContract.View? = null
    private val mServerManagerModel: ServerManagerContract.Model = serverManagerModel
    private var mCheckServersDisposable: DisposableObserver<*>? = null
    private var mScanDisposable: DisposableObserver<*>? = null
    private var mAdapter: ServerManagerContract.Adapter? = null

    override val data: OrderedRealmCollection<Server>
        get() = mServerManagerModel.servers

    private val scanType: Int
        get() = if (mCommonModel.isPrefPlayerVlc) {
            VLC_SCAN
        } else {
            MPC_SCAN
        }

    private val localIpAddress: ByteArray?
        get() {
            try {
                val en = NetworkInterface.getNetworkInterfaces()
                while (en.hasMoreElements()) {
                    val intf = en.nextElement() as NetworkInterface
                    val enumIpAddr = intf.inetAddresses
                    while (enumIpAddr.hasMoreElements()) {
                        val inetAddress = enumIpAddr.nextElement() as InetAddress
                        if (!inetAddress.isLoopbackAddress && inetAddress is Inet4Address) {
                            return inetAddress.getAddress()
                        }
                    }
                }
            } catch (ex: SocketException) {
                Timber.d("Socket exception in GetIP Address of Utilities" + ex.toString())
            }

            return null
        }

    override fun attachView(view: ServerManagerContract.View, adapter: ServerManagerContract
    .Adapter?) {
        mServerManagerModel.servers.addChangeListener(this)
        mView = view
        mAdapter = adapter
        mServerManagerModel.openDatabaseConnection()
    }

    override fun detachView() {
        mView = null
        mAdapter = null
        mServerManagerModel.servers.removeChangeListener(this)
        if (mCheckServersDisposable != null) {
            mCheckServersDisposable!!.dispose()
        }
        mServerManagerModel.closeDatabaseConnection()
    }

    override fun lockRightDrawer() {
        EventBus.getDefault().post(LockRightDrawerEvent())
    }

    override fun invalidateOptionsMenu() {
        EventBus.getDefault().post(InvalidateOptionsMenuEvent())
    }

    override fun initServerList() {
        val servers = mServerManagerModel.servers
        if (servers.isEmpty()) {
            mView!!.setRefreshingMessage()
            scanForServers()
        }
        mServerManagerModel.setServersNotResponding(servers)
    }

    override fun checkServers() {
        val servers = mServerManagerModel.servers
        mCheckServersDisposable = Observable.fromIterable(servers)
                .subscribeOn(mMainScheduler)
                .map { server ->
                    mServerManagerModel.buildCheckServerUrlString(server,
                            server.address, server.port, scanType)
                }
                .observeOn(Schedulers.io())
                .map { serverStringPair ->
                    mServerManagerModel.checkServer(serverStringPair.first,
                            serverStringPair.second, scanType)
                }
                .observeOn(mMainScheduler)
                .repeatWhen { objectObservable ->
                    objectObservable.delay(CHECK_SERVERS_INTERVAL.toLong(),
                            TimeUnit.MILLISECONDS)
                }
                .retryWhen { throwables ->
                    throwables.delay(CHECK_SERVERS_INTERVAL.toLong(),
                            TimeUnit.MILLISECONDS)
                }
                .subscribeWith(object : DisposableObserver<Pair<Server, Boolean>>() {
                    override fun onNext(serverBooleanPair: Pair<Server, Boolean>) {
                        mServerManagerModel.setResponding(serverBooleanPair.first,
                                serverBooleanPair.second)
                    }

                    override fun onError(e: Throwable) {

                    }

                    override fun onComplete() {

                    }
                })
    }

    override fun setToolbarTitle() {
        EventBus.getDefault().post(MainToolbarTitleChangeEvent(
                mServerManagerModel.defaultToolbarTitle, this.javaClass))
    }

    /**
     * @return true when:
     * <br></br>- server successfully created,
     * <br></br> false when:
     * <br></br>- server is a duplicate of an existing one
     * <br></br>- if ip address is invalid
     * <br></br>- port is invalid
     */
    override fun createServer(newAddress: String, newPort: String, newName: String,
                              newPassword: String): Boolean {
        var creationSuccessful = true
        val newServer = Server(newAddress, newPort, newName, newPassword)
        if (ServerUtils.isServerDuplicate(newAddress, newPort, null)) {
            mView!!.setServerDuplicationError()
            creationSuccessful = false
        }
        if (!ServerUtils.isIpAddress(newAddress)) {
            mView!!.setAddressInvalidError()
            creationSuccessful = false
        }
        if (!ServerUtils.isPortValid(newPort)) {
            mView!!.setPortInvalidError()
            creationSuccessful = false
        }
        if (creationSuccessful) {
            mServerManagerModel.saveNewServer(newServer)
        }
        return creationSuccessful
    }

    override fun onItemSwiped(position: Int) {
        val realmServer = data[position]
        mServerManagerModel.remove(realmServer)
        mAdapter!!.setSnackbarText(mServerManagerModel.deleteServerSnackbarText)
        mAdapter!!.notifyItemRemoved(position)
        mAdapter!!.showSnackbar()
    }

    override fun undoDeleteServer() {
        mServerManagerModel.undoDeleteServer()
        mAdapter!!.notifyDataSetChanged()
    }

    override fun isPreferredServer(position: Int): Boolean {
        val server = data[position]
        return (server.address == mCommonModel.serverAddress
                && server.port == mCommonModel.port.toString()
                && server.password == mCommonModel.password)
    }

    override fun setPreferredServer(newAddress: String, newPort: String, newPassword: String) {
        mServerManagerModel.setPreferredServer(newAddress, newPort, newPassword)
    }

    override fun editServer(currentName: String, currentAddress: String, currentPort: String,
                            currentPassword: String, newName: String, newAddress: String, newPort: String,
                            newPassword: String): Boolean {
        val newServer = Server(newAddress, newPort, newName, newPassword)
        // Check if anything changed in the parameters
        val existingServer = Server(currentAddress, currentPort, currentName,
                currentPassword)
        var editingSuccessful = true
        // If new server is exactly the same as existing one, do nothing
        if (newServer != existingServer) {
            if (ServerUtils.isServerDuplicate(newAddress, newPort, existingServer)) {
                mAdapter!!.setServerDuplicationError()
                editingSuccessful = false
            }
            if (!ServerUtils.isIpAddress(newAddress)) {
                mAdapter!!.setAddressInvalidError()
                editingSuccessful = false
            }
            if (!ServerUtils.isPortValid(newPort)) {
                mAdapter!!.setPortInvalidError()
                editingSuccessful = false
            }
            if (editingSuccessful) {
                mServerManagerModel.editServer(currentAddress, currentPort, newServer)
                mServerManagerModel.checkAndUpdatePreferences(currentAddress, currentPort,
                        currentPassword, newAddress, newPort, newPassword)
                mAdapter!!.notifyDataSetChanged()
            }
        }
        return editingSuccessful
    }

    override fun prefillPort() {
        if (mCommonModel.isPrefPlayerVlc) {
            mView!!.prefillPortVlc()
        } else if (mCommonModel.isPrefPlayerMpc) {
            mView!!.prefillPortMpc()
        }
    }

    override fun goToRemote() {
        EventBus.getDefault().post(GoToRemoteEvent())
    }

    override fun scanForServers() {
        val ip = localIpAddress
        val port = mCommonModel.defaultPort
        val scanType = scanType
        if (mScanDisposable != null) {
            mScanDisposable!!.dispose()
        }
        val vals = Observable.range(FIRST_IP_FOURTH_BYTE, LAST_IP_FOURTH_BYTE)
        mScanDisposable = vals.flatMap { `val` ->
            Observable.just(`val`)
                    .subscribeOn(Schedulers.computation())
                    .observeOn(Schedulers.io())
                    .map { i -> mServerManagerModel.scanServer(ip, i, port, scanType) }
        }.observeOn(mMainScheduler)
                .subscribeWith(object : DisposableObserver<Int>() {
                    override fun onNext(integer: Int) {

                    }

                    override fun onError(e: Throwable) {

                    }

                    override fun onComplete() {
                        if (mView != null) {
                            mView!!.setEmptyListMessage()
                            mView!!.clearRefreshAnimation()
                        }
                    }
                })
    }

    override fun onChange(servers: RealmResults<Server>, changeSet: OrderedCollectionChangeSet) {
        if (servers.isEmpty()) {
            mView!!.setServerListEmptyLayout()
        } else {
            mView!!.setServerListNotEmptyLayout()
        }
    }

    companion object {

        internal val MPC_SCAN = 0
        internal val VLC_SCAN = 1
        private val FIRST_IP_FOURTH_BYTE = 1
        private val LAST_IP_FOURTH_BYTE = 254
        private val CHECK_SERVERS_INTERVAL = 1000
    }
}
