package com.jakubbrzozowski.waveplayersremote.data

import io.realm.RealmObject
import io.realm.annotations.Index

open class Server : RealmObject {
    @Index
    var address: String = ""
    var port: String = ""
    var name: String = ""
    var password = "1234"
    var isResponding = false

    constructor() {}

    constructor(address: String, port: String, name: String, password: String) {
        this.address = address
        this.port = port
        this.name = name
        this.password = password
    }

    override fun equals(other: Any?): Boolean {
        if (other is Server) {
            val server = other as Server?
            return (address == server!!.address
                    && name == server.name
                    && port == server.port
                    && password == server.password)
        }
        return super.equals(other)
    }

    companion object {
        val ADDRESS = "address"
        val PORT = "port"
        val NAME = "name"
        val PASSWORD = "password"
    }
}
